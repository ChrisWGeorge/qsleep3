# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSLEEP
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                              -------------------
        begin                : 2014-07-18
        copyright            : (C) 2014 by Chris George
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt5.QtCore import * # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
import os.path

from .QSLEEPTopology import QSLEEPTopology
from .QSLEEPUtils import QSLEEPUtils
from .sleepdelin import Delineation
from .TauDEMUtils import TauDEMUtils

class GlobalVars:
    """Data used across across the plugin, and some utilities on it."""
    def __init__(self, iface, isBatch):
        """Initialise class variables."""
        # set TauDEM executables directory and mpiexec path
        # In Windows values currently stored in registry, in HKEY_CURRENT_USER\Software\QGIS\QGIS2
        # Read values from settings if present, else set to defaults
        # This allows them to be set elsewhere, in particular by Parameters module
        settings = QSettings()
        ## QGIS interface
        self.iface = iface
        ## Directory of TauDEM executables
        self.TauDEMDir = TauDEMUtils.findTauDEMDir(settings, isBatch)
        ## Path of mpiexec
        self.mpiexecPath = TauDEMUtils.findMPIExecPath(settings)
        ## Index of slope group in Layers panel
        self.slopeGroupIndex = -1
        ## Index of landuse group in Layers panel
        self.landuseGroupIndex = -1
        ## Index of soil group in Layers panel
        self.soilGroupIndex = -1
        ## Index of watershed group in Layers panel
        self.watershedGroupIndex = -1
        ## Index of results group in Layers panel
        self.resultsGroupIndex = -1
        ## Flag showing if using existing watershed
        self.existingWshed = False
        ## Flag showing if using grid model
        self.useGridModel = False
        ## Directory containing QSLEEP plugin
        self.plugin_dir = ''
        ## Path of DEM grid
        self.demFile = ''
        ## path of filled DEM
        self.felFile = ''
        ## Path of stream burn-in shapefile
        self.burnFile = ''
        ## Path of DEM after burning-in
        self.burnedDemFile = ''
        ## Path of D8 flow direction grid
        self.pFile = ''
        ## Path of basins grid
        self.basinFile = ''
        ## Path of outlets shapefile
        self.outletFile = ''
        ## Path of outlets shapefile for extra reservoirs and point sources
        self.extraOutletFile = ''
        ## Path of stream reaches shapefile
        self.streamFile = ''
        ## path of stream raster
        self.srcStreamFile = ''
        ## Path of watershed shapefile
        self.wshedFile = ''
        ## Path of slope grid
        self.slopeFile = ''
        ## valley depths file
        self.valleyDepthsFile = ''
        ## hillslopes file
        self.hillslopesFile = ''
        ## Area of DEM cell in square metres
        self.cellArea = 0
        ## Topology object
        self.topo = QSLEEPTopology(isBatch)
        projFile = QgsProject.instance().fileName()
        projPath = QFileInfo(projFile).canonicalFilePath()
        # avoid / on Windows because of SWAT Editor
        if os.name == 'nt':
            projPath = projPath.replace('/', '\\')
        pdir, base = os.path.split(projPath)
        ## Project name
        self.projName = os.path.splitext(base)[0]
        ## Project directory
        self.projDir = QSLEEPUtils.join(pdir, self.projName)
        ## Source directory
        self.sourceDir = ''
        ## SLEEP directory
        self.sleepDir = ''
        ## Soil directory
        self.soilDir = ''
        ## flood directory
        self.floodDir = ''
        ## NIR band directory
        self.NIRDir = ''
        ## Red band directory
        self.RedDir = ''
        ## Shapes directory
        self.shapesDir = ''
        self.createSubDirectories()
        ## Path of FullHRUs shapefile
        self.fullHRUsFile = QSLEEPUtils.join(self.shapesDir, 'hru1.shp')
        ## Path of ActHRUs shapefile
        self.actHRUsFile = QSLEEPUtils.join(self.shapesDir, 'hru2.shp')
        ## csv file containing soil attributes
        self.soilDataCSV = ''
        ## csv file with entries for automatic generation of rasters
        self.automateCSV = ''
        ## Flag to show if running in batch mode
        self.isBatch = isBatch
        ## Path of project database
#        self.db = DBUtils(self.projDir, self.projName, self.dbProjTemplate, self.dbRefTemplate, self.isBatch)
        ## multiplier to turn elevations to metres
        self.verticalFactor = 1
        ## vertical units
        self.verticalUnits = Delineation._METRES
        # positions of sub windows
        ## Position of delineation form
        self.delineatePos = QPoint(0, 100)
        ## Position of HRUs form
        self.hrusPos = QPoint(0, 100)
        ## Position of parameters form
        self.parametersPos = QPoint(50, 100)
        ## Position of select subbasins form
        self.selectSubsPos = QPoint(50, 100)
        ## Position of select reservoirs form
        self.selectResPos = QPoint(50, 100)
        ## Position of about form
        self.aboutPos = QPoint(50, 100)
        ## Position of elevation bands form
        self.elevationBandsPos = QPoint(50, 100)
        ## Position of split landuses form
        self.splitPos = QPoint(50, 100)
        ## Position of select landuses form
        self.selectLuPos = QPoint(50, 100)
        ## Position of exempt landuses form
        self.exemptPos = QPoint(50, 100)
        ## Position of outlets form
        self.outletsPos = QPoint(50, 100)
        ## Position of select outlets file form
        self.selectOutletFilePos = QPoint(50, 100)
        ## Position of select outlets form
        self.selectOutletPos = QPoint(50, 100)
        ## Position of visualise form
        self.visualisePos = QPoint(0, 100)
        ## rasters generated
        # map from raster id (also used as attribute name in soil data shapefile) to 
        # pair of file path and layer
        self.rasters = dict()
        ## facets
        self.facets = ''
        ## Width of DEM cell in metres
        self.sizeX = 0
        ## Height of DEM cell in metres
        self.sizeY = 0
        ## string of (xmin, xmax, ymin, ymax) values for GRASS region parameter
        self.extentString = ''
        ## map for soil layer number to depth in cm
        self.layers = dict()
        ## flag to show if running QGIS 2.6 or 2.18
        self.qgisIs2_6 = Qgis.QGIS_VERSION.startswith('2.6')
        ## shapefile containing the soil points data
        self.soilDataFile = ''
        ## means for each raw pridictor
        self.means = dict()
        ## standard deviations for each raw predictor
        self.stds = dict()
        ## number of facet classes selected
        self.facetClassCount = 1
        
    def createSubDirectories(self):
        """Create subdirectories under project file's directory."""
        if not os.path.exists(self.projDir):
            os.makedirs(self.projDir)
        self.sourceDir = QSLEEPUtils.join(self.projDir, 'Source')
        if not os.path.exists(self.sourceDir):
            os.makedirs(self.sourceDir)
        self.sleepDir = QSLEEPUtils.join(self.projDir, 'SLEEP')
        if not os.path.exists(self.sleepDir):
            os.makedirs(self.sleepDir)
        self.soilDir = QSLEEPUtils.join(self.sourceDir, 'soil')
        if not os.path.exists(self.soilDir):
            os.makedirs(self.soilDir)
        self.floodDir = QSLEEPUtils.join(self.sourceDir, 'flood')
        if not os.path.exists(self.floodDir):
            os.makedirs(self.floodDir)
        self.NIRDir = QSLEEPUtils.join(self.sourceDir, 'NIR')
        if not os.path.exists(self.NIRDir):
            os.makedirs(self.NIRDir)
        self.RedDir = QSLEEPUtils.join(self.sourceDir, 'Red')
        if not os.path.exists(self.RedDir):
            os.makedirs(self.RedDir)
        watershedDir = QSLEEPUtils.join(self.projDir, 'Watershed')
        if not os.path.exists(watershedDir):
            os.makedirs(watershedDir)
        self.shapesDir = QSLEEPUtils.join(watershedDir, 'Shapes')
        if not os.path.exists(self.shapesDir):
            os.makedirs(self.shapesDir)
            
    def setVerticalFactor(self):
        """Set vertical conversion factor according to vertical units."""
        if self.verticalUnits == Delineation._METRES:
            self.verticalFactor = 1
        elif self.verticalUnits == Delineation._FEET:
            self.verticalFactor = Delineation._FEETTOMETRES
        elif self.verticalUnits == Delineation._CM:
            self.verticalFactor = Delineation._CMTOMETRES
        elif self.verticalUnits == Delineation._MM:
            self.verticalFactor = Delineation._MMTOMETRES
        elif self.verticalUnits == Delineation._INCHES:
            self.verticalFactor = Delineation._INCHESTOMETRES
        elif self.verticalUnits == Delineation._YARDS:
            self.verticalFactor = Delineation._YARDSTOMETRES
     
