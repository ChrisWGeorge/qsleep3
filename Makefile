#/***************************************************************************
# QSleep
#
# Soil-Landscape Estimation and Evaluation Program
#                             -------------------
#        begin                : 2016-05-21
#        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
#        email                : cgeorge@mcmaster.ca
# ***************************************************************************/
#
#/***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************/
# On Windows this makefile needs to be made using mingw32-make

PLUGINNAME = QSLEEP3

# another windows setting
DOXYGEN = 'C:\Program Files\doxygen\bin\doxygen.exe'

QGISDIR = AppData/Roaming/QGIS/QGIS3/profiles/default

# CONFIGURATION
PLUGIN_UPLOAD = $(CURDIR)/plugin_upload.py


# Makefile for a PyQGIS plugin

# translation
SOURCES = $(PY_FILES)
#TRANSLATIONS = i18n/qsleep_en.ts
TRANSLATIONS =

# global

PY_FILES = soildata.py soildatadialog.py regressiondialog.py selectcsv.py selectcsvdialog.py \
	selectvarsdialog.py outletsdialog.py parameters.py \
	parametersdialog.py sleepdelin.py sleepdelindialog.py writeusersoil.py qsleep.py \
	qsleepdialog.py sleepglobals.py QSLEEPTopology.py QSLEEPUtils.py TauDEMUtils.py __init__.py raster.py \
	landscape.py floodplain.py about.py aboutdialog.py

EXTRAS = SLEEP32.png

UI_FILES = ui_soildata.ui ui_regression.ui ui_selectcsv.ui ui_selectvars.ui ui_outlets.ui ui_parameters.ui ui_sleepdelin.ui ui_qsleep.ui ui_about.ui

QML_FILES = outlets.qml stream.qml

RESOURCE_FILES = resources_rc.py

METADATA = metadata.txt

HELP = help/build/html

EXTRAPACKAGES = statsmodels sklearn pandas patsy joblib 

default: deploy

compile: $(UI_FILES) $(RESOURCE_FILES)

# used as single target for changing python files without rebuilding documentation
# See note on deploy below
recompile: compile
	mkdir -p "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
	cp -vuf $(PY_FILES) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"

%_rc.py : %.qrc
	pyrcc5 -o $*_rc.py  $<

%.py : %.ui
	pyuic5 -o $@ $<

%.qm : %.ts
	lrelease $<

# The deploy  target only works on unix like operating system where
# the Python plugin directory is located at:
# $HOME/$(QGISDIR)/python/plugins
deploy: recompile # doc transcompile
	cp -vuf $(UI_FILES) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
	cp -vuf $(QML_FILES) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
	cp -vuf $(RESOURCE_FILES) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
	cp -vu $(EXTRAS) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
	cp -vuf $(METADATA) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)/metadata.txt"
	cp -vufr i18n "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
	cp -vufr $(EXTRAPACKAGES) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"
#	cp -vufr $(HELP) "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)/help"

# The dclean target removes compiled python files from plugin directory
# also delets any .svn entry
dclean:
	find "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)" -iname "*.pyc" -delete
	find "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)" -iname ".svn" -prune -exec rm -Rf {} \;

# The derase deletes deployed plugin
derase:
	rm -Rf "$(HOME)/$(QGISDIR)/python/plugins/$(PLUGINNAME)"

# The zip target deploys the plugin and creates a zip file with the deployed
# content. You can then upload the zip file on http://plugins.qgis.org
zip: deploy dclean
	rm -f $(PLUGINNAME).zip
	cd "$(HOME)/$(QGISDIR)/python/plugins"; zip -9r $(CURDIR)/$(PLUGINNAME).zip $(PLUGINNAME)

# Create a zip package of the plugin named $(PLUGINNAME).zip.
# This requires use of git (your plugin development directory must be a
# git repository).
# To use, pass a valid commit or tag as follows:
#   make package VERSION=Version_0.3.2
package: compile
		rm -f $(PLUGINNAME).zip
		git archive --prefix=$(PLUGINNAME)/ -o $(PLUGINNAME).zip $(VERSION)
		echo "Created package: $(PLUGINNAME).zip"

upload: zip
	$(PLUGIN_UPLOAD) $(PLUGINNAME).zip

# transup
# update .ts translation files
transup:
	pylupdate4 Makefile

# transcompile
# compile translation files into .qm binary format
transcompile: $(TRANSLATIONS:.ts=.qm)

# transclean
# deletes all .qm files
transclean:
	rm -f i18n/*.qm

clean:
	rm $(UI_FILES) $(RESOURCE_FILES)

# build documentation with sphinx
doc:
#	cd help; make html
# use doxygen
	$(DOXYGEN)
