# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                              -------------------
        begin                : 2014-07-18
        copyright            : (C) 2014 by Chris George
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt5.QtCore import * # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
from qgis.gui import * # @UnusedWildImport
import os.path
import glob
import shutil
import subprocess
import processing  # @UnresolvedImport
from osgeo import gdal, ogr

# Import the code for the dialog
from .sleepdelindialog import SleepDelinDialog
from .TauDEMUtils import TauDEMUtils
from .QSLEEPUtils import QSLEEPUtils, fileWriter, FileTypes
from .QSLEEPTopology import QSLEEPTopology
from .outletsdialog import OutletsDialog
from .landscape import Landscape

class Delineation(QObject):
    
    """Do watershed delineation."""
    
    _SQKM = 'sq. km'
    _HECTARES = 'hectares'
    _SQMETRES = 'sq. metres'
    _SQMILES = 'sq. miles'
    _ACRES = 'acres'
    _SQFEET = 'sq. feet'
    _METRES = 'metres'
    _FEET = 'feet'
    _CM = 'centimetres'
    _MM = 'millimetres'
    _INCHES = 'inches'
    _YARDS = 'yards'
    _DEGREES = 'degrees'
    _UNKNOWN = 'unknown'
    _FEETTOMETRES = 0.3048
    _CMTOMETRES = 0.01
    _MMTOMETRES = 0.001
    _INCHESTOMETRES = 0.0254
    _YARDSTOMETRES = 0.91441
    
    def __init__(self, iface, gv, isDelineated):
        """Initialise class variables."""
        QObject.__init__(self)
        self._iface = iface
        self._gv = gv
        self._dlg = SleepDelinDialog()
        self._dlg.setWindowFlags(self._dlg.windowFlags() & ~Qt.WindowContextHelpButtonHint & Qt.WindowMinimizeButtonHint)
        self._dlg.move(self._gv.delineatePos)
        self._odlg = OutletsDialog()
        self._odlg.setWindowFlags(self._odlg.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self._odlg.move(self._gv.outletsPos)
        ## Qgs vector layer for drawing inlet/outlet points
        self.drawOutletLayer = None
        ## depends on DEM height and width and also on choice of area units
        self.areaOfCell = 0
        ## Width of DEM as number of cells
        self.demWidth = 0
        ## Height of DEM cell as number of cells
        self.demHeight = 0
        ## coordinate reference system (derived from DEM)
        self.crs = None
        ## flag to prevent infinite recursion between number of cells and area
        self.changing = False
        ## flag to show basic delineation is done, so removing subbasins, 
        # adding reservoirs and point sources may be done
        self.isDelineated = isDelineated
        ## flag to show delineation completed successfully or not
        self.delineationFinishedOK = True
        ## flag to show finishDelineation has been run
        self.finishHasRun = False
        ## mapTool used for drawing outlets etc
        self.mapTool = None
#         ## watershed outlet point
#         self.outlet = None
        ## streams raster
        self.str = ''
        ## basins raster
        self.basins = ''
        ## snapped outlet
        self.snapFile = ''
        
    def init(self):
        """Set connections to controls; read project delineation data."""
        settings = QSettings()
        try:
            self._dlg.numProcesses.setValue(int(settings.value('/QSWAT/NumProcesses')))
        except Exception:
            self._dlg.numProcesses.setValue(8)
        self._dlg.selectDemButton.clicked.connect(self.btnSetDEM)
        self._dlg.checkBurn.stateChanged.connect(self.changeBurn)
        self._dlg.burnButton.clicked.connect(self.btnSetBurn)
        self._dlg.selectOutletsButton.clicked.connect(self.btnSetOutlets)
        self._dlg.delinRunButton1.clicked.connect(self.runTauDEM1)
        self._dlg.delinRunButton2.clicked.connect(self.runTauDEM2)
        self._dlg.tabWidget.currentChanged.connect(self.changeExisting)
        self._dlg.useOutlets.stateChanged.connect(self.changeUseOutlets)
        self._dlg.drawOutletsButton.clicked.connect(self.drawOutlets)
        self._dlg.taudemHelpButton.clicked.connect(TauDEMUtils.taudemHelp)
        self._dlg.OKButton.clicked.connect(self.finishDelineation)
        self._dlg.cancelButton.clicked.connect(self.doClose)
        self._dlg.numCells.setValidator(QIntValidator())
        self._dlg.numCells.textChanged.connect(self.setArea)
        self._dlg.area.textChanged.connect(self.setNumCells)
        self._dlg.area.setValidator(QDoubleValidator())
        self._dlg.areaUnitsBox.addItem(Delineation._SQKM)
        self._dlg.areaUnitsBox.addItem(Delineation._HECTARES)
        self._dlg.areaUnitsBox.addItem(Delineation._SQMETRES)
        self._dlg.areaUnitsBox.addItem(Delineation._SQMILES)
        self._dlg.areaUnitsBox.addItem(Delineation._ACRES)
        self._dlg.areaUnitsBox.addItem(Delineation._SQFEET)
        self._dlg.areaUnitsBox.activated.connect(self.changeAreaOfCell)
        self._dlg.horizontalCombo.addItem(Delineation._METRES)
        self._dlg.horizontalCombo.addItem(Delineation._FEET)
        self._dlg.horizontalCombo.addItem(Delineation._DEGREES)
        self._dlg.horizontalCombo.addItem(Delineation._UNKNOWN)
        self._dlg.verticalCombo.addItem(Delineation._METRES)
        self._dlg.verticalCombo.addItem(Delineation._FEET)
        self._dlg.verticalCombo.addItem(Delineation._CM)
        self._dlg.verticalCombo.addItem(Delineation._MM)
        self._dlg.verticalCombo.addItem(Delineation._INCHES)
        self._dlg.verticalCombo.addItem(Delineation._YARDS)
        # set vertical unit default to metres
        self._dlg.verticalCombo.setCurrentIndex(self._dlg.verticalCombo.findText(Delineation._METRES))
        self._dlg.verticalCombo.activated.connect(self.setVerticalUnits)
        self._odlg.resumeButton.clicked.connect(self.resumeDrawing)
        self.readProj()
        self.checkMPI()
        # allow for cancellation without being considered an error
        self.delineationFinishedOK = True
        # Prevent annoying "error 4 .shp not recognised" messages.
        # These should become exceptions but instead just disappear.
        # Safer in any case to raise exceptions if something goes wrong.
        gdal.UseExceptions()
        ogr.UseExceptions()
        
    def run(self):
        """Do delineation; check done and save topology data.  Return 1 if delineation done and no errors, 2 if not delineated and nothing done, else 0."""
        self.init()
        self._dlg.show()
        result = self._dlg.exec_()  # @UnusedVariable
        self._gv.delineatePos = self._dlg.pos()
        if self.delineationFinishedOK:
            if self.finishHasRun:
#                self._gv.writeMasterProgress(1,0) 
                return 1
            else:
                # nothing done
                return 2
#        self._gv.writeMasterProgress(0,0)
        return 0
    
    def checkMPI(self):
        """
        Try to make sure there is just one msmpi.dll, either on the path or in the TauDEM directory.
        
        TauDEM executables are built on the assumption that MPI is available.
        But they can run without MPI if msmpi.dll is placed in their directory.
        MPI will fail if there is an msmpi.dll on the path and one in the TauDEM directory 
        (unless they happen to be the same version).
        QSWAT supplies msmpi_dll in the TauDEM directory that can be renamed to provide msmpi.dll 
        if necessary.
        This function is called every time delineation is started so that if the user installs MPI
        or uninstalls it the appropriate steps are taken.
        """
        dll = 'msmpi.dll'
        dummy = 'msmpi_dll'
        dllPath = QSLEEPUtils.join(self._gv.TauDEMDir, dll)
        dummyPath = QSLEEPUtils.join(self._gv.TauDEMDir, dummy)
        # tried various methods here.  
        #'where msmpi.dll' succeeds if it was there and is moved or renamed - cached perhaps?
        # isfile fails similarly
        #'where mpiexec' always fails because when run interactively the path does not include the MPI directory
        # so just check for existence of mpiexec.exe and assume user will not leave msmpi.dll around
        # if MPI is installed and then uninstalled
        if self._gv.mpiexecPath is not None and os.path.isfile(self._gv.mpiexecPath):
            QSLEEPUtils.loginfo('mpiexec found')
            # MPI is on the path; rename the local dll if necessary
            if os.path.exists(dllPath):
                if os.path.exists(dummyPath):
                    os.remove(dllPath)
                    QSLEEPUtils.loginfo('dll removed')
                else:
                    os.rename(dllPath, dummyPath)
                    QSLEEPUtils.loginfo('dll renamed')
        else:
            QSLEEPUtils.loginfo('mpiexec not found')
            # we don't have MPI on the path; rename the local dummy if necessary
            if os.path.exists(dllPath):
                return
            elif os.path.exists(dummyPath):
                os.rename(dummyPath, dllPath)
                QSLEEPUtils.loginfo('dummy renamed')
            else:
                QSLEEPUtils.error('Cannot find executable mpiexec in the system or {0} in {1}: TauDEM functions will not run.  Install MPI or reinstall QSWAT.'.format(dll, self._gv.TauDEMDir), self._gv.isBatch)
            
    def finishDelineation(self):
        """
        Finish delineation.
        
        Checks stream reaches and watersheds defined, sets DEM attributes, 
        checks delineation is complete, calculates flow distances,
        runs topology setup.  Sets delineationFinishedOK to true if all completed successfully.
        """
        self.delineationFinishedOK = False
        self.finishHasRun = True
        if 'fil' not in self._gv.rasters:
            QSLEEPUtils.error('Pit filled DEM not found: have you created the watershed?', self._gv.isBatch)
            return
        demLayer = self._gv.rasters['fil'][1]
        if not self.setDimensions(demLayer):
            return
        self.saveProj()
        self.delineationFinishedOK = True
        self._dlg.done(1)
    
    def checkDEMProcessed(self):
        """Return true if wshedFile is newer than DEM) 
        """
        return QSLEEPUtils.isUpToDate(self._gv.demFile, self._gv.wshedFile)
        
    def btnSetDEM(self):
        """Open and load DEM; set default threshold."""
        root = QgsProject.instance().layerTreeRoot()
        (demFile, demMapLayer) = QSLEEPUtils.openAndLoadFile(root, FileTypes._DEM, self._dlg.selectDem,
                                                            self._gv.sourceDir, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        if demFile and demMapLayer:
            self._gv.demFile = demFile
            self.setDefaultNumCells(demMapLayer)
            # warn if large DEM
            numCells = self.demWidth * self.demHeight
            if numCells > 4E6:
                millions = int(numCells / 1E6)
                self._gv.iface.messageBar().pushMessage('Large DEM',
                                                 'This DEM has over {0!s} million cells and could take some time to process.  Be patient!'.format(millions),
                                                 level=Qgis.Warning, duration=20)
            self.addHillshade(demFile, root, demMapLayer)
            
    def addHillshade(self, demFile, root, demMapLayer):
        """ Create hillshade layer and load."""
        hillshadeFile = QSLEEPUtils.join(self._gv.sourceDir, 'hillshade.tif')
        if not QSLEEPUtils.isUpToDate(demFile, hillshadeFile):
            # run gdaldem to generate hillshade.tif
            settings = QSettings()
            path = str(settings.value('/GdalTools/gdalPath', ''))
            gdaldem = QSLEEPUtils.join(path, 'gdaldem.exe')
            QSLEEPUtils.removeLayerAndFiles(hillshadeFile, root)
            command = gdaldem + ' hillshade -compute_edges -z 5 "' + demFile + '" "' + hillshadeFile +'"'
            proc = subprocess.run(command,
                                    shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT,
                                    text=True)
            QSLEEPUtils.loginfo('Creating hillshade ...')
            QSLEEPUtils.loginfo(command)
            for line in  proc.stdout.split('\n'):
                QSLEEPUtils.loginfo(line)
            if not os.path.exists(hillshadeFile):
                QSLEEPUtils.information('Failed to create hillshade file {0}'.format(hillshadeFile), self._gv.isBatch)
                return
            QSLEEPUtils.copyPrj(demFile, hillshadeFile)
        # add hillshade above DEM
        # demMapLayer allowed to be None for batch running
        if demMapLayer is not None:
            demLayer = root.findLayer(demMapLayer.id())
            hillMapLayer = QSLEEPUtils.getLayerByFilename(root.findLayers(), hillshadeFile, FileTypes._HILLSHADE, 
                                                         self._gv, demLayer, QSLEEPUtils._WATERSHED_GROUP_NAME)[0]
            if hillMapLayer is None:
                QSLEEPUtils.information('Failed to load hillshade file {0}'.format(hillshadeFile), self._gv.isBatch)
                return
            # compress legend entry
            hillTreeLayer = root.findLayer(hillMapLayer.id())
            hillTreeLayer.setExpanded(False)
            hillMapLayer.renderer().setOpacity(0.4)
            hillMapLayer.triggerRepaint()
            
    def btnSetBurn(self):
        """Open and load stream network to burn in."""
        root = QgsProject.instance().layerTreeRoot()
        (burnFile, burnLayer) = QSLEEPUtils.openAndLoadFile(root, FileTypes._BURN, self._dlg.selectBurn, self._gv.sourceDir, 
                                                            self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        if burnFile and burnLayer:
            fileType = QgsWkbTypes.geometryType(burnLayer.dataProvider().wkbType())
            if fileType != QgsWkbTypes.LineGeometry:
                QSLEEPUtils.error('Burn in file {0} is not a line shapefile'.format(burnFile), self._gv.isBatch)
            else:
                self._gv.burnFile = burnFile
        
    def btnSetOutlets(self):
        """Open and load outlet shapefile."""
        root = QgsProject.instance().layerTreeRoot()
        assert self._dlg.tabWidget.currentIndex() == 0
        box = self._dlg.selectOutlets
        (outletFile, outletLayer) = QSLEEPUtils.openAndLoadFile(root, FileTypes._OUTLETS, box, self._gv.shapesDir, 
                                                                self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        if outletFile and outletLayer:
            fileType = QgsWkbTypes.geometryType(outletLayer.dataProvider().wkbType())
            if fileType != QgsWkbTypes.PointGeometry:
                QSLEEPUtils.error('Inlets/outlets file {0} is not a point shapefile'.format(outletFile), self._gv.isBatch) 
            else:
                self._gv.outletFile = outletFile
                
    def runTauDEM1(self):
        """Run Taudem to create stream reach network."""
        self.runTauDEM(None, False)
       
    def runTauDEM2(self):
        """Run TauDEM to create watershed shapefile."""
        # first remove any existing shapesDir inlets/outlets file as will
        # probably be inconsistent with new subbasins
        root = QgsProject.instance().layerTreeRoot()
        QSLEEPUtils.removeLayerByLegend(QSLEEPUtils._EXTRALEGEND, root.findLayers())
        self._gv.extraOutletFile = ''
        if not self._dlg.useOutlets.isChecked():
            self.runTauDEM(None, True)
        else:
            outletFile = self._dlg.selectOutlets.text()
            if outletFile == '' or not os.path.exists(outletFile):
                QSLEEPUtils.error('Please select an outlet file', self._gv.isBatch)
                return
            self.runTauDEM(outletFile, True)
        
    def changeExisting(self):
        """Change between using existing and delineating watershed."""
        tab = self._dlg.tabWidget.currentIndex()
        if tab > 1: # DEM properties or TauDEM output
            return # no change
    
    def runTauDEM(self, outletFile, makeWshed):
        """Run TauDEM."""
        self.delineationFinishedOK = False
        demFile = self._dlg.selectDem.text()
        if demFile == '' or not os.path.exists(demFile):
            QSLEEPUtils.error('Please select a DEM file', self._gv.isBatch)
            return
        self.isDelineated = False
        # find dem layer (or load it)
        root = QgsProject.instance().layerTreeRoot()
        demLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), self._gv.demFile, FileTypes._DEM, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        if not demLayer:
            QSLEEPUtils.error('Cannot load DEM {0}'.format(self._gv.demFile), self._gv.isBatch)
            return
        if not self.setDefaultNumCells(demLayer):
            return
        (base, suffix) = os.path.splitext(self._gv.demFile)
        # burn in if required
        if self._dlg.checkBurn.isChecked():
            burnFile = self._dlg.selectBurn.text()
            if burnFile == '':
                QSLEEPUtils.error('Please select a burn in stream network shapefile', self._gv.isBatch)
                return
            if not os.path.exists(burnFile):
                QSLEEPUtils.error('Cannot find burn in file {0}'.format(burnFile), self._gv.isBatch)
                return
            #burnMethod = 0
            #burnEpsilon = 50
            burnedFile = os.path.splitext(self._gv.demFile)[0] + '_burned.tif'
            if not QSLEEPUtils.isUpToDate(demFile, burnFile) or not QSLEEPUtils.isUpToDate(burnFile, burnedFile):
                # just in case
                QSLEEPUtils.removeLayerAndFiles(burnedFile, root)
                self.progress('Burning streams ...')
                #burnRasterFile = self.streamToRaster(demLayer, burnFile)
                #processing.runalg('saga:burnstreamnetworkintodem', demFile, burnRasterFile, burnMethod, burnEpsilon, burnedFile)
                QSLEEPTopology.burnStream(burnFile, demFile, burnedFile, self._gv.verticalFactor, self._gv.isBatch)
                if not os.path.exists(burnedFile):
                    return
            self._gv.burnedFile = burnedFile
            delineationDem = burnedFile
        else:
            self._gv.burnedFile = ''
            delineationDem = demFile
        numProcesses = self._dlg.numProcesses.value()
        mpiexecPath = self._gv.mpiexecPath
        if numProcesses > 0 and (mpiexecPath == '' or not os.path.exists(mpiexecPath)):
            QSLEEPUtils.information('Cannot find MPI program {0} so running TauDEM with just one process'.format(mpiexecPath), self._gv.isBatch)
            numProcesses = 0
            self._dlg.numProcesses.setValue(0)
        QSettings().setValue('/QSWAT/NumProcesses', str(numProcesses))
        if self._dlg.showTaudem.isChecked():
            self._dlg.tabWidget.setCurrentIndex(3)
        self._dlg.setCursor(Qt.WaitCursor)
        self._dlg.taudemOutput.clear()
        # delet old flood files if any
        self.deleteFloodFiles(root)
        felFile = base + 'fel' + suffix
        QSLEEPUtils.removeLayer(felFile, root)
        self.progress('PitFill ...')
        ok = TauDEMUtils.runPitFill(delineationDem, felFile, numProcesses, self._dlg.taudemOutput)   
        if not ok:
            self.cleanUp(2)
            return
#         # run r.watershed to generate streams, drainage and accumulation rasters
#         threshold = int(self._dlg.numCells.text())
#         maxSurfaceFlowLength = 0 # ignored - only for USLE
#         convergenceFactor = 5 # standard default
#         memory = 300 # standard default (and only applies with -m option)
#         CELLSize = self._gv.sizeX # TODO assume square
#         positiveAccumulations = True
#         D8FlowDirections = False
#         fac = QSLEEPUtils.join(self._gv.sleepDir, 'fac.tif')
#         QSLEEPUtils.removeLayerAndFiles(fac, li)
#         fdr = QSLEEPUtils.join(self._gv.sleepDir, 'fdr.tif')
#         QSLEEPUtils.removeLayerAndFiles(fdr, li)
#         self.streamRast = QSLEEPUtils.join(self._gv.sleepDir, 'streamRast.tif')
#         QSLEEPUtils.removeLayerAndFiles(self.streamRast, li)
#         if self._gv.qgisIs2_6:
#             command = '"grass:r.watershed", {0}, None, None, None, None, {1}, {2}, {3}, {4}, {5}, False, False, {6}, {7}, {8}, {9}, {10}, None, {11}, None, None, None, None'. \
#             format(felFile, threshold, maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, positiveAccumulations, self._gv.extentString, CELLSize, fac, fdr, self.streamRast)
#             result = processing.runalg("grass:r.watershed", felFile, None, None, None, None, threshold,
#                                        maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, True, False, positiveAccumulations, self._gv.extentString,
#                                        CELLSize, fac, fdr, None, self.streamRast, None, None, None, None)
#         else:
#             command = '"grass7:r.watershed", {0}, None, None, None, None, {1}, {2}, {3}, {4}, {5}, False, False, {6}, False, {7}, {8}, {9}, {10}, None, {11}, None, None, None, None'. \
#             format(felFile, threshold, maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, positiveAccumulations, self._gv.extentString, CELLSize, fac, fdr, self.streamRast)
#             result = processing.runalg("grass7:r.watershed", felFile, None, None, None, None, threshold,
#                                        maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, True, False, positiveAccumulations, False, self._gv.extentString,
#                                        CELLSize, fac, fdr, None, self.streamRast, None, None, None, None)
#         QSLEEPUtils.loginfo(command)
#         if not result:
#             QSLEEPUtils.error(u'Processing command failed: {0}'.format(command), self._gv.isBatch)
#             self.cleanUp(-1)
#             return
#         if not 'accumulation' in result:
#             QSLEEPUtils.error(u'Processing command produced dictionary with keys: {0}'.format(result.keys()), self._gv.isBatch)
#         QSLEEPUtils.loginfo(repr(result))
#         if not os.path.exists(fac):
#             QSLEEPUtils.error(u"""Accumulation file {0} not created.  
#             Please check in QGIS that under Processing -> Options -> Providers
#             GRASS (QGIS 2.6) or GRASS 7 (QGIS 2.18) is enabled.""".format(fac))
#             self.cleanUp(-1)
#             return
        # use TauDEM functions for delineation
        self._gv.felFile = felFile
        sd8File = base + 'sd8' + suffix
        pFile = base + 'p' + suffix
        QSLEEPUtils.removeLayer(sd8File, root)
        QSLEEPUtils.removeLayer(pFile, root)
        self.progress('D8FlowDir ...')
        ok = TauDEMUtils.runD8FlowDir(felFile, sd8File, pFile, numProcesses, self._dlg.taudemOutput)   
        if not ok:
            self.cleanUp(3)
            return
        self._gv.pFile = pFile
        slpFile = base + 'slp' + suffix
        angFile = base + 'ang' + suffix
        QSLEEPUtils.removeLayer(slpFile, root)
        QSLEEPUtils.removeLayer(angFile, root)
        self.progress('DinfFlowDir ...')
        ok = TauDEMUtils.runDinfFlowDir(felFile, slpFile, angFile, numProcesses, self._dlg.taudemOutput)  
        if not ok:
            self.cleanUp(3)
            return
        ad8File = base + 'ad8' + suffix
        QSLEEPUtils.removeLayer(ad8File, root)
        self.progress('AreaD8 ...')
        ok = TauDEMUtils.runAreaD8(pFile, ad8File, None, None, numProcesses, self._dlg.taudemOutput, mustRun=False)   
        if not ok:
            self.cleanUp(3)
            return
        scaFile = base + 'sca' + suffix
        QSLEEPUtils.removeLayer(scaFile, root)
        self.progress('AreaDinf ...')
        ok = TauDEMUtils.runAreaDinf(angFile, scaFile, None, None, numProcesses, self._dlg.taudemOutput, mustRun=False)  
        if not ok:
            self.cleanUp(3)
            return
        gordFile = base + 'gord' + suffix
        plenFile = base + 'plen' + suffix
        tlenFile = base + 'tlen' + suffix
        QSLEEPUtils.removeLayer(gordFile, root)
        QSLEEPUtils.removeLayer(plenFile, root)
        QSLEEPUtils.removeLayer(tlenFile, root)
        self.progress('GridNet ...')
        ok = TauDEMUtils.runGridNet(pFile, plenFile, tlenFile, gordFile, None, numProcesses, self._dlg.taudemOutput, mustRun=False)  
        if not ok:
            self.cleanUp(3)
            return
        srcStreamFile = base + 'srcStream' + suffix
        QSLEEPUtils.removeLayer(srcStreamFile, root)
        self.progress('Threshold ...')
        streamThreshold = int(self._dlg.numCells.text())
        if self._gv.isBatch:
            QSLEEPUtils.information('Stream threshold: {0} cells'.format(streamThreshold), True)
        ok = TauDEMUtils.runThreshold(ad8File, srcStreamFile, str(streamThreshold), numProcesses, self._dlg.taudemOutput, mustRun=False) 
        if not ok:
            self.cleanUp(3)
            return
        self._gv.srcStreamFile = srcStreamFile
        self.str = srcStreamFile
        assert os.path.exists(ad8File), 'accumulation file {0} not created'.format(ad8File)
        assert os.path.exists(pFile), 'drainage direction file {0} not created'.format(pFile)
        assert os.path.exists(self.str), 'stream raster file {0} not created'.format(self.str)
        QSLEEPUtils.copyPrj(felFile, ad8File)
        QSLEEPUtils.copyPrj(felFile, pFile)
        QSLEEPUtils.copyPrj(felFile, self.str)
        ordStreamFile = base + 'ordStream' + suffix
        treeStreamFile = base + 'treeStream.dat'
        coordStreamFile = base + 'coordStream.dat'
        streamFile = os.path.join(self._gv.shapesDir,'stream.shp')
        wStreamFile = base + 'wStream' + suffix
        QSLEEPUtils.removeLayer(ordStreamFile, root)
        QSLEEPUtils.removeLayer(wStreamFile, root)
        QSLEEPUtils.removeLayer(streamFile, root)
        self.progress('StreamNet ...')
        ok = TauDEMUtils.runStreamNet(felFile, pFile, ad8File, srcStreamFile, None, ordStreamFile, treeStreamFile, coordStreamFile,
                                      streamFile, wStreamFile, False, numProcesses, self._dlg.taudemOutput, mustRun=False)
        if not ok:
            self.cleanUp(3)
            return
        root = QgsProject.instance().layerTreeRoot()
        hillshadeLayer = QSLEEPUtils.getLayerByLegend(QSLEEPUtils._HILLSHADELEGEND, root.findLayers())
        if hillshadeLayer:
            subLayer = hillshadeLayer
        else:
            subLayer = demLayer
        streamLayer, loaded = QSLEEPUtils.getLayerByFilename(root.findLayers(), streamFile, FileTypes._STREAMS, self._gv, subLayer, QSLEEPUtils._WATERSHED_GROUP_NAME)
        if not streamLayer or not loaded:
            self.cleanUp(-1)
            return
        if not makeWshed:
            # initial run to enable placing of outlets, so finishes with load of stream network
            self._dlg.taudemOutput.append('------------------- TauDEM finished -------------------\n')
            self.saveProj()
            self.cleanUp(-1)
            return
        if self._dlg.useOutlets.isChecked():
            outletLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), outletFile, FileTypes._OUTLETS, self._gv, streamLayer, QSLEEPUtils._WATERSHED_GROUP_NAME)
            if not outletLayer:
                self.cleanUp(-1)
                return
            outletBase = os.path.splitext(outletFile)[0]
            snapFile = outletBase + '_snap.shp'
            self.progress('SnapOutletsToStreams ...')
            ok = self.createSnapOutletFile(outletLayer, streamLayer, outletFile, snapFile, root)  
            if not ok:
                self.cleanUp(-1)
                return
            # make sure later TauDEM runs use snapped outlets  TODO: 
            # self._gv.outletFile = self.snapFile
#             self.progress(u'Move outlet to stream ...')
#             if outletLayer.featureCount() > 1:
#                 QSLEEPUtils.information(u'There is more than one point in your outlet layer.  Only the first will be used.', self._gv.isBatch)
#             fileType = outletLayer.dataProvider().geometryType()
#             for feature in outletLayer.getFeatures():
#                 geom = feature.geometry()
#                 if fileType == QGis.WKBPoint:
#                     startOutlet = geom.asPoint()
#                 else:
#                     startOutlet = geom.asMultiPoint()[0]
#                 break
#             maxMoves = 10
#             self.outlet = self.moveOutletToStream(fdr, self.streamRast, startOutlet, int(maxMoves))
#             if not self.outlet:
#                 self.cleanUp(-1)
#                 return
#             QSLEEPUtils.loginfo(u'Moved from ({0},{1}) to ({2},{3})'.format(startOutlet.x(), startOutlet.y(), self.outlet.x(), self.outlet.y()))
#             # run r.water.outlet to get basin mask
#             # WARNING r.water.outlet leaves handle on drainDIR: cannot be removed after this
#             self.progress(u'Masking catchment ...')
#             self._gv.cat = QSLEEPUtils.join(self._gv.sleepDir, 'cat.tif')
#             QSLEEPUtils.removeLayerAndFiles(self._gv.cat, li)
#             if self._gv.qgisIs2_6:
#                 command = '"grass:r.water.outlet", {0}, {1}, {2}, {3}, {4}, {5}'. \
#                 format(fdr, self.outlet.x(), self.outlet.y(), self._gv.extentString, CELLSize, self._gv.cat)
#                 result = processing.runalg("grass:r.water.outlet", fdr, self.outlet.x(), self.outlet.y(), self._gv.extentString, CELLSize, self._gv.cat)
#                 expect = 'basin'
#             else:
#                 # essential there is no space in coordString
#                 coordString = '{0},{1}'.format(self.outlet.x(), self.outlet.y())
#                 command = '"grass7:r.water.outlet", {0}, {1}, {2}, {3}, {4}'. \
#                 format(fdr, coordString, self._gv.extentString, CELLSize, self._gv.cat)
#                 result = processing.runalg("grass7:r.water.outlet", fdr, coordString, self._gv.extentString, CELLSize, self._gv.cat)
#                 expect = 'output'
#             QSLEEPUtils.loginfo(command)
#             if not result:
#                 QSLEEPUtils.error(u'Processing command failed: {0}'.format(command), self._gv.isBatch)
#                 self.cleanUp(-1)
#                 return
#             if not expect in result:
#                 QSLEEPUtils.error(u'Processing command produced dictionary with keys: {0}'.format(result.keys()), self._gv.isBatch)
            
            # repeat AreaD8, GridNet, Threshold and StreamNet with snapped outlet
            mustRun = True
            QSLEEPUtils.removeLayer(ad8File, root)
            self.progress('AreaD8 ...')
            ok = TauDEMUtils.runAreaD8(pFile, ad8File, self.snapFile, None, numProcesses, self._dlg.taudemOutput, mustRun=mustRun)   
            if not ok:
                self.cleanUp(3)
                return
            self.progress('GridNet ...')
            ok = TauDEMUtils.runGridNet(pFile, plenFile, tlenFile, gordFile, self.snapFile, numProcesses, self._dlg.taudemOutput, mustRun=mustRun)  
            if not ok:
                self.cleanUp(3)
                return
            QSLEEPUtils.removeLayer(srcStreamFile, root)
            self.progress('Threshold ...')
            ok = TauDEMUtils.runThreshold(ad8File, srcStreamFile, str(streamThreshold), numProcesses, self._dlg.taudemOutput, mustRun=mustRun) 
            if not ok:
                self.cleanUp(3)
                return
            QSLEEPUtils.removeLayer(ordStreamFile, root)
            QSLEEPUtils.removeLayer(wStreamFile, root)
            QSLEEPUtils.removeLayer(streamFile, root)
            self.progress('StreamNet ...')
            ok = TauDEMUtils.runStreamNet(felFile, pFile, ad8File, srcStreamFile, self.snapFile, ordStreamFile, treeStreamFile, coordStreamFile,
                                          streamFile, wStreamFile, False, numProcesses, self._dlg.taudemOutput, mustRun=mustRun)
            if not ok:
                self.cleanUp(3)
                return
            QSLEEPUtils.copyPrj(felFile, wStreamFile)
        # polygonize basin raster to shapefile for clipping
        wshedFile = QSLEEPUtils.join(self._gv.shapesDir, 'wshed.shp')
        QSLEEPUtils.rasterToPolygonShapefile(wStreamFile, wshedFile, 'PolygonId', self.crs, root, self._gv.isBatch)
        # make hillslopes and floodplain
        L = Landscape(self._gv, self._dlg.taudemOutput, numProcesses, self.progress)
        L.run(streamThreshold, streamThreshold, streamThreshold, wshedFile, False)
        # clip filled DEM
        fil = QSLEEPUtils.join(self._gv.sleepDir, 'fil.tif')
        QSLEEPUtils.removeLayerAndFiles(fil, root)
        command = 'gdalwarp --config GDALWARP_IGNORE_BAD_CUTLINE YES -dstnodata -32768 -q -cutline "{0}" -crop_to_cutline -of GTiff "{1}" "{2}"'.format(wshedFile, felFile, fil)
        # os.system(command)
        QSLEEPUtils.loginfo(command)
        with os.popen(command) as proc:
            res = proc.read()
            if res != '':
                QSLEEPUtils.loginfo(res)
        assert os.path.exists(fil)
        QSLEEPUtils.copyPrj(felFile, fil)
        # remove DEM and hillshade, replace with clipped dem
        layers = root.findLayers()
        demLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(FileTypes._DEM), layers)
        if demLayer:
            self._iface.setActiveLayer(demLayer.layer())
            clipLayer, loaded = QSLEEPUtils.getLayerByFilename(layers, fil, FileTypes._DEM, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
            if clipLayer and loaded:
                QSLEEPUtils.removeLayer(self._gv.demFile, root)
                # redo hillshade
                self.addHillshade(fil, root, clipLayer)
        # recalculate extent
        extent = clipLayer.extent()
        north = extent.yMaximum()
        south = extent.yMinimum()
        east = extent.xMaximum()
        west = extent.xMinimum()
#         # extentString must not contain spaces
#         self._gv.extentString = '{0},{1},{2},{3}'.format(west, east, south, north)
        # set extent of D8 slope and area to match that of fil and (later) aspect and curvature maps
        clipPFile = base + 'pclip' + suffix
        QSLEEPUtils.removeLayerAndFiles(clipPFile, root)
        command = 'gdalwarp -te {0} {1} {2} {3} "{4}" "{5}"'.format(west, south, east, north, pFile, clipPFile)
        os.system(command)
        assert os.path.exists(clipPFile)
        # gdalwarp can leave file untouched if it already existed, so we (effectively) touch it
        os.utime(clipPFile, None)
        QSLEEPUtils.copyPrj(felFile, clipPFile)
        clipAD8File = base + 'ad8clip' + suffix
        QSLEEPUtils.removeLayerAndFiles(clipAD8File, root)
        command = 'gdalwarp -te {0} {1} {2} {3} "{4}" "{5}"'.format(west, south, east, north, ad8File, clipAD8File)
        os.system(command)
        assert os.path.exists(clipAD8File)
        # gdalwarp can leave file untouched if it already existed, so we (effectively) touch it
        os.utime(clipAD8File, None)
        QSLEEPUtils.copyPrj(felFile, clipAD8File)
        # use D8
        fac = clipAD8File
        fdr = clipPFile
        facLayer = QgsRasterLayer(fac, 'fac')
        fdrLayer = QgsRasterLayer(fdr, 'fdr')
        self._gv.rasters['fac'] = fac, facLayer
        self._gv.rasters['fdr'] = fdr, fdrLayer
        filLayer = clipLayer if clipLayer else demLayer if demLayer else QgsRasterLayer(fil)
        filLayer.setName('fil')
        self._gv.rasters['fil'] = fil, filLayer
#         # run r.watershed to generate halfbasin and streams within basin
#         self.progress(u'Facets ...')
#         self.basins = QSLEEPUtils.join(self._gv.sleepDir, 'basins.tif')
#         QSLEEPUtils.removeLayerAndFiles(self.basins, li)
#         self._gv.facets = QSLEEPUtils.join(self._gv.sleepDir, 'facets.tif')
#         QSLEEPUtils.removeLayerAndFiles(self._gv.facets, li)
#         self.str = QSLEEPUtils.join(self._gv.sleepDir, 'str.tif')
#         QSLEEPUtils.removeLayerAndFiles(self.str, li)
#         if self._gv.qgisIs2_6:
#             command = '"grass:r.watershed", {0}, None, None, None, None, {1}, {2}, {3}, {4}, {5}, True, False, {6}, {7}, {8}, None, None, {9}, {10}, {11}, None, None, None'. \
#             format(fil, threshold, maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, positiveAccumulations, self._gv.extentString, CELLSize, self.basins, self.str, self._gv.facets)
#             QSLEEPUtils.loginfo(command)
#             result = processing.runalg("grass:r.watershed", fil, None, None, None, None, threshold,
#                                        maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, True, False, positiveAccumulations, self._gv.extentString,
#                                        CELLSize, None, None, self.basins, self.str, self._gv.facets, None, None, None)
#         else:
#             command = '"grass7:r.watershed", {0}, None, None, None, None, {1}, {2}, {3}, {4}, {5}, True, False, {6}, False, {7}, {8}, None, None, {9}, {10}, {11}, None, None, None'. \
#             format(fil, threshold, maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, positiveAccumulations, self._gv.extentString, CELLSize, self.basins, self.str, self._gv.facets)
#             QSLEEPUtils.loginfo(command)
#             result = processing.runalg("grass7:r.watershed", fil, None, None, None, None, threshold,
#                                        maxSurfaceFlowLength, convergenceFactor, memory, D8FlowDirections, True, False, positiveAccumulations, False, 
#                                        self._gv.extentString,
#                                        CELLSize, None, None, self.basins, self.str, self._gv.facets, None, None, None)
#         if not result:
#             QSLEEPUtils.error(u'Processing command failed: {0}'.format(command), self._gv.isBatch)
#             self.cleanUp(-1)
#             return
#         if not 'stream' in result:
#             QSLEEPUtils.error(u'Processing command produced dictionary with keys: {0}'.format(result.keys()), self._gv.isBatch)
        assert os.path.exists(self.str), 'basin streams file {0} not created'.format(self.str)
        assert os.path.exists(self._gv.facets), 'facets file {0} not created'.format(self._gv.facets)
        assert os.path.exists(wStreamFile), 'basins file {0} not created'.format(wStreamFile)
        strLayer = QgsRasterLayer(self.str, 'str')
        self._gv.rasters['str'] = self.str, strLayer
        QSLEEPUtils.copyPrj(fil, self.str)
        QSLEEPUtils.copyPrj(fil, self._gv.facets)
        QSLEEPUtils.copyPrj(fil, wStreamFile)
        self._dlg.taudemOutput.append('------------------- TauDEM finished -------------------\n')
        self._gv.pFile = fdr
        self._gv.basinFile = wStreamFile
        self.isDelineated = True
        self.saveProj()
        self.cleanUp(-1)
        self.progress('Watershed done')
    
    def setDefaultNumCells(self, demLayer):
        """Set threshold number of cells to default of 1% of number in grid, 
        unless already set.
        """
        if not self.setDimensions(demLayer):
            return False
        # set to default number of cells unless already set
        if self._dlg.numCells.text() == '':
            numCells = self.demWidth * self.demHeight
            defaultNumCells = int(numCells * 0.01)
            self._dlg.numCells.setText(str(defaultNumCells))
        else:
            # already have a setting: keep same area but change number of cells according to dem cell size
            self.setNumCells()
        return True
            
    def setDimensions(self, demLayer):
        """
        Set dimensions of DEM.
        
        Also sets DEM properties tab.
        
        """
        # can fail if demLayer is None or not projected
        try:
            if not self._gv.topo.setCrs(demLayer):
                return False
            units = demLayer.crs().mapUnits()
        except Exception as e:
            QSLEEPUtils.loginfo('Failure to read DEM units: {0}'.format(str(e)))
            return False
        demFile = QSLEEPUtils.layerFileInfo(demLayer).absoluteFilePath()
        demPrj = os.path.splitext(demFile)[0] + '.prj'
        demPrjTxt = demPrj + '.txt'
        if os.path.exists(demPrj) and not os.path.exists(demPrjTxt):
            command = 'gdalsrsinfo -p -o wkt "{0}" > "{1}"'.format(demPrj, demPrjTxt)
            os.system(command)
        if os.path.exists(demPrjTxt):
            with open(demPrjTxt) as f:
                text = f.read()
                self._dlg.textBrowser.setText(text)
        else:
            self._dlg.textBrowser.setText(demLayer.crs().toWkt()) # much poorer presentation
        try:
            epsg = demLayer.crs().authid()
            QSLEEPUtils.loginfo(epsg)
            rect = demLayer.extent()
            self._dlg.label.setText('Spatial reference: {0}'.format(epsg))
            # epsg has format 'EPSG:N' where N is the EPSG number
            startNum = epsg.find(':') + 1
            if self._gv.isBatch and startNum > 0:
                demDataFile = QSLEEPUtils.join(self._gv.projDir, 'dem_data.xml')
                if not os.path.exists(demDataFile):
                    f = fileWriter(demDataFile)
                    f.writeLine('<demdata>')
                    f.writeLine('<epsg>{0}</epsg>'.format(epsg[startNum:]))
                    f.writeLine('<minx>{0}</minx>'.format(rect.xMinimum()))
                    f.writeLine('<maxx>{0}</maxx>'.format(rect.xMaximum()))
                    f.writeLine('<miny>{0}</miny>'.format(rect.yMinimum()))
                    f.writeLine('<maxy>{0}</maxy>'.format(rect.yMaximum()))
                    f.writeLine('</demdata>')
                    f.close()
        except:
            # fail gracefully
            epsg = ''
        if units == QgsUnitTypes.DistanceMeters:
            factor = 1
            self._dlg.horizontalCombo.setCurrentIndex(self._dlg.horizontalCombo.findText(Delineation._METRES))
            self._dlg.horizontalCombo.setEnabled(False)
        elif units == QgsUnitTypes.DistanceFeet:
            factor = 0.3048
            self._dlg.horizontalCombo.setCurrentIndex(self._dlg.horizontalCombo.findText(Delineation._FEET))
            self._dlg.horizontalCombo.setEnabled(False)
        else:
            if units == QgsUnitTypes.AngleDegrees:
                string = 'degrees'
                self._dlg.horizontalCombo.setCurrentIndex(self._dlg.horizontalCombo.findText(Delineation._DEGREES))
                self._dlg.horizontalCombo.setEnabled(False)
            else:
                string = 'unknown'
                self._dlg.horizontalCombo.setCurrentIndex(self._dlg.horizontalCombo.findText(Delineation._DEGREES))
                self._dlg.horizontalCombo.setEnabled(True)
            QSLEEPUtils.information('WARNING: DEM does not seem to be projected: its units are ' + string, self._gv.isBatch)
            return False
        self.demWidth = demLayer.width()
        self.demHeight = demLayer.height()
        sizeX = demLayer.rasterUnitsPerPixelX()
        sizeY = demLayer.rasterUnitsPerPixelY()
        if sizeX != sizeY:
            QSLEEPUtils.information("""WARNING: DEM cells are not square: {0!s} x {1!s}.
You are likely to get a failure later.  Use Raster -> Projections -> Warp to create new DEM with same projection
and set the resolution, probably close to {2!s}
""".format(sizeX, sizeY, int((sizeX + sizeY)/2 + 0.5)), self._gv.isBatch)
        self._gv.sizeX = sizeX * factor
        self._gv.sizeY = sizeY * factor
        self._gv.cellArea = self._gv.sizeX * self._gv.sizeY * factor * factor
        self.crs = demLayer.crs()
        self._dlg.sizeEdit.setText('{:.4G} x {:.4G}'.format(self._gv.sizeX, self._gv.sizeY))
        self._dlg.sizeEdit.setReadOnly(True)
        self.setAreaOfCell()
        areaM2 = float(self._gv.sizeX * self._gv.sizeY) / 1E4
        self._dlg.areaEdit.setText('{:.4G}'.format(areaM2))
        self._dlg.areaEdit.setReadOnly(True)
        extent = demLayer.extent()
        north = extent.yMaximum()
        south = extent.yMinimum()
        east = extent.xMaximum()
        west = extent.xMinimum()
        topLeft = self._gv.topo.pointToLatLong(QgsPointXY(west, north))
        bottomRight = self._gv.topo.pointToLatLong(QgsPointXY(east, south))
        northll = topLeft.y()
        southll = bottomRight.y()
        eastll = bottomRight.x()
        westll = topLeft.x()
        # extentString must not contain spaces
        self._gv.extentString = '{0},{1},{2},{3}'.format(west, east, south, north)
        self._dlg.northEdit.setText(self.degreeString(northll))
        self._dlg.southEdit.setText(self.degreeString(southll))
        self._dlg.eastEdit.setText(self.degreeString(eastll))
        self._dlg.westEdit.setText(self.degreeString(westll))
        return True
    
    @staticmethod
    def degreeString(decDeg):
        """Generate string showing degrees as decmal and as degrees minuts seconds."""
        deg = int(decDeg)
        decMin = abs(decDeg - deg) * 60
        minn = int(decMin)
        sec = int((decMin - minn) * 60)
        return '{0:.2F}{1} ({2!s}{1} {3!s}\' {4!s}")'.format(decDeg, chr(176), deg, minn, sec)
            
    def setAreaOfCell(self):
        """Set area of 1 cell according to area units choice."""
        areaSqM = float(self._gv.sizeX * self._gv.sizeY)
        if self._dlg.areaUnitsBox.currentText() == Delineation._SQKM:
            self.areaOfCell = areaSqM / 1E6 
        elif self._dlg.areaUnitsBox.currentText() == Delineation._HECTARES:
            self.areaOfCell = areaSqM / 1E4
        elif self._dlg.areaUnitsBox.currentText() == Delineation._SQMETRES:
            self.areaOfCell = areaSqM
        elif self._dlg.areaUnitsBox.currentText() == Delineation._SQMILES:
            self.areaOfCell = areaSqM / 2589988.1
        elif self._dlg.areaUnitsBox.currentText() == Delineation._ACRES:
            self.areaOfCell = areaSqM / 4046.8564
        elif self._dlg.areaUnitsBox.currentText() == Delineation._SQFEET:
            self.areaOfCell = areaSqM * 10.763910
            
    def changeAreaOfCell(self):
        """Set area of cell and update area threshold display."""
        self.setAreaOfCell()
        self.setArea()
        
    def setVerticalUnits(self):
        """Sets vertical units from combo box; sets corresponding factor to apply to elevations."""
        self._gv.verticalUnits = self._dlg.verticalCombo.currentText()
        self._gv.setVerticalFactor()

    def setArea(self):
        """Update area threshold display."""
        if self.changing: return
        try:
            numCells = float(self._dlg.numCells.text())
        except Exception:
            # not currently parsable - ignore
            return
        area = numCells * self.areaOfCell
        self.changing = True
        self._dlg.area.setText('{0:.4G}'.format(area))
        self.changing = False
            
    def setNumCells(self):
        """Update number of cells threshold display."""
        if self.changing: return
        # prevent division by zero
        if self.areaOfCell == 0: return
        try:
            area = float(self._dlg.area.text())
        except Exception:
            # not currently parsable - ignore
            return
        numCells = int(area / self.areaOfCell)
        self.changing = True
        self._dlg.numCells.setText(str(numCells))
        self.changing = False
        
    def changeBurn(self):
        """Make burn option available or not according to check box state."""
        if self._dlg.checkBurn.isChecked():
            self._dlg.selectBurn.setEnabled(True)
            self._dlg.burnButton.setEnabled(True)
            if self._dlg.selectBurn.text() != '':
                self._gv.burnFile = self._dlg.selectBurn.text()
        else:
            self._dlg.selectBurn.setEnabled(False)
            self._dlg.burnButton.setEnabled(False)
            self._gv.burnFile = ''
        
    def changeUseOutlets(self):
        """Make outlets option available or not according to check box state."""
        if self._dlg.useOutlets.isChecked():
            self._dlg.outletsWidget.setEnabled(True)
            self._dlg.selectOutlets.setEnabled(True)
            self._dlg.selectOutletsButton.setEnabled(True)
            if self._dlg.selectOutlets.text() != '':
                self._gv.outletFile = self._dlg.selectOutlets.text()
        else:
            self._dlg.outletsWidget.setEnabled(False)
            self._dlg.selectOutlets.setEnabled(False)
            self._dlg.selectOutletsButton.setEnabled(False)
            self._gv.outletFile = ''
            
    def drawOutlets(self):
        """Allow user to create inlets/outlets in current shapefile 
        or a new one.
        """
        self._odlg.widget.setEnabled(True)
        canvas = self._iface.mapCanvas()
        self.mapTool = QgsMapToolEmitPoint(canvas)
        self.mapTool.canvasClicked.connect(self.getPoint)
        canvas.setMapTool(self.mapTool)
        # detect maptool change
        canvas.mapToolSet.connect(self.mapToolChanged)
        root = QgsProject.instance().layerTreeRoot()
        outletLayer = QSLEEPUtils.getLayerByFilenameOrLegend(root.findLayers(), self._gv.outletFile, FileTypes._OUTLETS, '', self._dlg)
        if outletLayer:  # we have a current outlet layer - give user a choice 
            msgBox = QMessageBox()
            msgBox.move(self._gv.selectOutletFilePos)
            msgBox.setWindowTitle('Select inlets/outlets file to draw on')
            text = """
            Select "Current" if you wish to draw new points in the 
            existing inlets/outlets layer, which is
            {0}.
            Select "New" if you wish to make a new inlets/outlets file.
            Select "Cancel" to abandon drawing.
            """.format(self._gv.outletFile)
            msgBox.setText(QSLEEPUtils.trans(text))
            currentButton = msgBox.addButton(QSLEEPUtils.trans('Current'), QMessageBox.ActionRole)
            newButton = msgBox.addButton(QSLEEPUtils.trans('New'), QMessageBox.ActionRole)  # @UnusedVariable
            msgBox.setStandardButtons(QMessageBox.Cancel)
            result = msgBox.exec_()
            self._gv.selectOutletFilePos = msgBox.pos()
            if result == QMessageBox.Cancel:
                return
            drawCurrent = msgBox.clickedButton() == currentButton
        else:
            drawCurrent = False
        if drawCurrent:
            if not self._iface.setActiveLayer(outletLayer):
                QSLEEPUtils.error('Could not make inlets/outlets layer active', self._gv.isBatch)
                return
            self.drawOutletLayer = outletLayer
            self.drawOutletLayer.startEditing()
        else:
            drawOutletFile = QSLEEPUtils.join(self._gv.shapesDir, 'drawoutlets.shp')
            # our outlet file may already be called drawoutlets.shp
            if QSLEEPUtils.samePath(drawOutletFile, self._gv.outletFile):
                drawOutletFile = QSLEEPUtils.join(self._gv.shapesDir, 'drawoutlets1.shp')
            if not self.createOutletFile(drawOutletFile, self._gv.demFile, False, root):
                return
            self.drawOutletLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), \
                                                                          drawOutletFile, FileTypes._OUTLETS, self._gv, 
                                                                          None, QSLEEPUtils._WATERSHED_GROUP_NAME)
            if self.drawOutletLayer is None:
                QSLEEPUtils.error('Unable to load shapefile {0}'.format(drawOutletFile), self._gv.isBatch)
                return
            if not self._iface.setActiveLayer(self.drawOutletLayer):
                QSLEEPUtils.error('Could not make drawing inlets/outlets layer active', self._gv.isBatch)
                return
            self.drawOutletLayer.startEditing()
        self._dlg.showMinimized()
        self._odlg.show()
        result = self._odlg.exec_()
        self._gv.outletsPos = self._odlg.pos()
        self._dlg.showNormal()
        canvas.setMapTool(None)
        if result == 1:
            # points added by drawing will have ids of -1, so fix them
            self.fixPointIds()
            if not drawCurrent:
                self._gv.outletFile = drawOutletFile
                self._dlg.selectOutlets.setText(drawOutletFile)
        else:
            if drawCurrent:
                self.drawOutletLayer.rollBack()
            else:
                # cancel - destroy drawn shapefile
                QSLEEPUtils.removeLayerAndFiles(drawOutletFile, root)
           
    def mapToolChanged(self, tool):
        """Disable choice of point to be added to show users they must resume adding,
        unless changing to self.mapTool."""
        self._odlg.widget.setEnabled(tool == self.mapTool)
                
    def resumeDrawing(self):
        """Reset canvas' mapTool."""
        self._odlg.widget.setEnabled(True)
        self._iface.setActiveLayer(self.drawOutletLayer)
        canvas = self._iface.mapCanvas()
        canvas.setMapTool(self.mapTool)
    
    def getPoint(self, point, _):
        """Add point to drawOutletLayer."""
        # can't use feature count as they can't be counted until adding is confirmed
        # so set to -1 and fix them later
        pid = -1
        idIndex = self._gv.topo.getIndex(self.drawOutletLayer, QSLEEPTopology._ID)
        inletIndex = self._gv.topo.getIndex(self.drawOutletLayer, QSLEEPTopology._INLET)
        resIndex = self._gv.topo.getIndex(self.drawOutletLayer, QSLEEPTopology._RES)
        ptsourceIndex = self._gv.topo.getIndex(self.drawOutletLayer, QSLEEPTopology._PTSOURCE)
        feature = QgsFeature()
        fields = self.drawOutletLayer.dataProvider().fields()
        feature.setFields(fields)
        feature.setAttribute(idIndex, pid)
        feature.setAttribute(inletIndex, 0)
        feature.setAttribute(resIndex, 0)
        feature.setAttribute(ptsourceIndex, 0)
        feature.setGeometry(QgsGeometry.fromPointXY(point))
        self.drawOutletLayer.addFeature(feature)
        self.drawOutletLayer.triggerRepaint()
        # clicking on map may have hidden the dialog, so make it top
        self._odlg.raise_()
        
    def fixPointIds(self):
        """Give suitable point ids to drawn points."""
        # need to commit first or appear to be no features
        self.drawOutletLayer.commitChanges()
        # then start editing again
        self.drawOutletLayer.startEditing()
        idIndex = self._gv.topo.getIndex(self.drawOutletLayer, QSLEEPTopology._ID)
        # find maximum existing feature id
        maxId = 0
        for feature in self.drawOutletLayer.getFeatures():
            maxId = max(maxId, feature.attributes()[idIndex])
        # replace negative feature ids
        for feature in self.drawOutletLayer.getFeatures():
            pid = feature.attributes()[idIndex]
            if pid < 0:
                maxId += 1
                self.drawOutletLayer.changeAttributeValue(feature.id(), idIndex, maxId)
        self.drawOutletLayer.commitChanges()
                
    def selectOutlets(self):
        """Allow user to select points in inlets/outlets layer."""
        root = QgsProject.instance().layerTreeRoot()
        selFromLayer = None
        layer = self._iface.activeLayer()
        if layer:
            if 'inlets/outlets' in layer.name():
                #if layer.name().startswith(QSLEEPUtils._SELECTEDLEGEND):
                #    QSLEEPUtils.error(u'You cannot select from a selected inlets/outlets layer', self._gv.isBatch)
                #    return
                selFromLayer = layer
        if not selFromLayer:
            selFromLayer = QSLEEPUtils.getLayerByFilenameOrLegend(root.findLayers(), self._gv.outletFile, FileTypes._OUTLETS, '', self._dlg)
            if not selFromLayer:
                QSLEEPUtils.error('Cannot find inlets/outlets layer.  Please choose the layer you want to select from in the layers panel.', self._gv.isBatch)
                return
        if not self._iface.setActiveLayer(selFromLayer):
            QSLEEPUtils.error('Could not make inlets/outlets layer active', self._gv.isBatch)
            return
        self._iface.actionSelectRectangle().trigger()
        msgBox = QMessageBox()
        msgBox.move(self._gv.selectOutletPos)
        msgBox.setWindowTitle('Select inlets/outlets')
        text = """
        Hold Ctrl and select the points by dragging the mouse 
        to make a small rectangle around them.
        Selected points will turn yellow, and a count is shown 
        at the bottom left of the main window.
        If you want to start again release Ctrl and click somewhere
        away from any points; then hold Ctrl and resume selection.
        You can pause in the selection to pan or zoom provided 
        you hold Ctrl again when you resume selection.
        When finished click "Save" to save your selection, 
        or "Cancel" to abandon the selection.
        """
        msgBox.setText(QSLEEPUtils.trans(text))
        msgBox.setStandardButtons(QMessageBox.Save | QMessageBox.Cancel)
        msgBox.setWindowModality(Qt.NonModal)
        self._dlg.showMinimized()
        msgBox.show()
        result = msgBox.exec_()
        self._gv.selectOutletPos = msgBox.pos()
        self._dlg.showNormal()
        if result != QMessageBox.Save:
            selFromLayer.removeSelection()
            return
        selectedIds = selFromLayer.selectedFeaturesIds()
        # QSLEEPUtils.information(u'Selected feature ids: {0!s}'.format(selectedIds), self._gv.isBatch)
        selFromLayer.removeSelection()
        # make a copy of selected layer's file, then remove non-selected features from it
        info = QSLEEPUtils.layerFileInfo(selFromLayer)
        baseName = info.baseName()
        path = info.absolutePath()
        pattern = QSLEEPUtils.join(path, baseName) + '.*'
        for f in glob.iglob(pattern):
            base, suffix = os.path.splitext(f)
            target = base + '_sel' + suffix
            shutil.copyfile(f, target)
            if suffix == '.shp':
                self._gv.outletFile = target
        assert os.path.exists(self._gv.outletFile) and self._gv.outletFile.endswith('_sel.shp')
        # make old outlet layer invisible
        root = QgsProject.instance().layerTreeRoot()
        QSLEEPUtils.setLayerVisibility(selFromLayer, False, root)
        # remove any existing selected layer
        QSLEEPUtils.removeLayerByLegend(QSLEEPUtils._SELECTEDLEGEND, root.findLayers())
        # load new outletFile
        selOutletLayer, loaded = QSLEEPUtils.getLayerByFilename(root.findLayers(), self._gv.outletFile, FileTypes._OUTLETS, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        if not selOutletLayer or not loaded:
            QSLEEPUtils.error('Could not load selected inlets/outlets shapefile {0}'.format(self._gv.outletFile), self._gv.isBatch)
            return
        # remove non-selected features
        featuresToDelete = []
        for feature in selOutletLayer.getFeatures():
            fid = feature.id()
            if not fid in selectedIds:
                featuresToDelete.append(fid)
        # QSLEEPUtils.information('Non-selected feature ids: {0!s}'.format(featuresToDelete), self._gv.isBatch)
        selOutletLayer.dataProvider().deleteFeatures(featuresToDelete)
        selOutletLayer.triggerRepaint()
        self._dlg.selectOutlets.setText(self._gv.outletFile)
        self._dlg.selectOutletsInteractiveLabel.setText('{0!s} selected'.format(len(selectedIds)))
        
    def cleanUp(self, tabIndex):
        """Set cursor to Arrow, clear progress label, clear message bar, 
        and change tab index if not negative.
        """
        if tabIndex >= 0:
            self._dlg.tabWidget.setCurrentIndex(tabIndex)
        self._dlg.setCursor(Qt.ArrowCursor)
        self.progress('')
        return
     
        
    def createBasinFile(self, wshedFile, demLayer):
        """Create basin file from watershed shapefile."""
        demPath = QSLEEPUtils.layerFileInfo(demLayer).canonicalFilePath()
        wFile = os.path.splitext(demPath)[0] + 'w.tif'
        shapeBase = os.path.splitext(wshedFile)[0]
        # if basename of wFile is used rasterize fails
        baseName = os.path.basename(shapeBase)
        QSLEEPUtils.removeLayerAndFiles(wFile, self._iface.legendInterface())
        assert not os.path.exists(wFile)
        command = 'gdal_rasterize -a {0} -tr {1!s} {2!s} -a_nodata -9999 -l "{3}" "{4}" "{5}"'.format(QSLEEPTopology._POLYGONID, self._gv.sizeX, self._gv.sizeY, baseName, wshedFile, wFile)
        QSLEEPUtils.loginfo(command)
        os.system(command)
        assert os.path.exists(wFile)
        QSLEEPUtils.copyPrj(wshedFile, wFile)
        return wFile      
    
    # no longer used.  Note setDimensions would need to be called first as it uses gv.sizeX and sizeY
#     def streamToRaster(self, demLayer, streamFile):
#         """Use rasterize to generate a raster for the streams, with a fixed value of 1 along the streams."""
#         demPath = QSLEEPUtils.layerFileInfo(demLayer).absolutePath()
#         rasterFile = QSLEEPUtils.join(os.path.splitext(demPath)[0], 'streams.tif')
#         QSLEEPUtils.removeLayerAndFiles(rasterFile, self._iface.legendInterface())
#         assert not os.path.exists(rasterFile)
#         extent = demLayer.extent()
#         xMin = extent.xMinimum()
#         xMax = extent.xMaximum()
#         yMin = extent.yMinimum()
#         yMax = extent.yMaximum()
#         command = 'gdal_rasterize -burn 1 -a_nodata -9999 -te {0!s} {1!s} {2!s} {3!s} -tr {4!s} {5!s} -ot Int32 "{6}" "{7}"'.format(xMin, yMin, xMax, yMax, self._gv.sizeX, self._gv.sizeY, streamFile, rasterFile)
#         QSLEEPUtils.information(command, self._gv.isBatch)
#         os.system(command)
#         assert os.path.exists(rasterFile)
#         QSLEEPUtils.copyPrj(streamFile, rasterFile)
#         return rasterFile
        

    
    def createOutletFile(self, filePath, sourcePath, subWanted, root):
        """Create filePath with fields needed for outlets file, 
        copying .prj from sourcePath, and adding Subbasin field if wanted.
        """
        if QSLEEPUtils.shapefileExists(filePath):
            # safer to empty it than to try to create a new one
            # TODO: check it has expected fields
            ft = FileTypes._OUTLETS
            outletLayer = QSLEEPUtils.getLayerByFilename(root.findLayers(), filePath, ft, None, None, None)[0]
            if outletLayer is None:
                outletLayer = QgsVectorLayer(filePath, FileTypes.legend(ft), 'ogr')
            return QSLEEPUtils.removeAllFeatures(outletLayer)
        else:
            QSLEEPUtils.removeLayerAndFiles(filePath, root)
            fields = QgsFields()
            fields.append(QgsField(QSLEEPTopology._ID, QVariant.Int))
            fields.append(QgsField(QSLEEPTopology._INLET, QVariant.Int))
            fields.append(QgsField(QSLEEPTopology._RES, QVariant.Int))
            fields.append(QgsField(QSLEEPTopology._PTSOURCE, QVariant.Int))
            if subWanted:
                fields.append(QgsField(QSLEEPTopology._SUBBASIN, QVariant.Int))
            writer = QgsVectorFileWriter(filePath, 'CP1250', fields, QgsWkbTypes.Point, QgsCoordinateReferenceSystem(), 'ESRI Shapefile')
            if writer.hasError() != QgsVectorFileWriter.NoError:
                QSLEEPUtils.error('Cannot create outlets shapefile {0}: {1}'.format(filePath, writer.errorMessage()), self._gv.isBatch)
                return False
            writer.flushBuffer()
            QSLEEPUtils.copyPrj(sourcePath, filePath)
            return True
    
    def getOutletIds(self, field):
        """Get list of ID values from inlets/outlets layer 
        for which field has value 1.
        """
        result = set()
        if self._gv.outletFile == '':
            return result
        outletLayer = QSLEEPUtils.getLayerByFilenameOrLegend(self._iface.legendInterface().layers(), self._gv.outletFile, FileTypes._OUTLETS, '', self._dlg)
        if not outletLayer:
            QSLEEPUtils.error('Cannot find inlets/outlets layer', self._gv.isBatch)
            return result
        idIndex = self._gv.topo.getIndex(outletLayer, QSLEEPTopology._ID)
        fieldIndex = self._gv.topo.getIndex(outletLayer, field)
        for f in outletLayer.getFeatures():
            attrs = f.attributes()
            if attrs[fieldIndex] == 1:
                result.add(attrs[idIndex])
        return result
        
    def deleteFloodFiles(self, root):
        """Delete any files (and their layers if any) matching *flood*.* in Flood directory."""
        pattern = QSLEEPUtils.join(self._gv.floodDir, '*flood*.*')
        for f in glob.iglob(pattern):
            QSLEEPUtils.removeLayerAndFiles(f, root)
        
    def createSnapOutletFile(self, outletLayer, streamLayer, outletFile, snapFile, root):
        """Create inlets/outlets file with points snapped to stream reaches."""
        if outletLayer.featureCount() == 0:
            QSLEEPUtils.error('The outlet layer {0} has no points'.format(outletLayer.name()), self._gv.isBatch)
            return False
        try:
            snapThreshold = 300  # int(self._dlg.snapThreshold.text())
        except Exception:
            QSLEEPUtils.error('Cannot parse snap threshold {0} as integer.'.format(self._dlg.snapThreshold.text()), self._gv.isBatch)
            return False
        if not self.createOutletFile(snapFile, outletFile, False, root):
            return False
        if self._gv.isBatch:
            QSLEEPUtils.information('Snap threshold: {0!s} metres'.format(snapThreshold), self._gv.isBatch)
        snapLayer = QgsVectorLayer(snapFile, 'Snapped inlets/outlets ({0})'.format(QFileInfo(snapFile).baseName()), 'ogr')
        idIndex = self._gv.topo.getIndex(outletLayer, QSLEEPTopology._ID)
        inletIndex = self._gv.topo.getIndex(outletLayer, QSLEEPTopology._INLET)
        resIndex = self._gv.topo.getIndex(outletLayer, QSLEEPTopology._RES)
        ptsourceIndex = self._gv.topo.getIndex(outletLayer, QSLEEPTopology._PTSOURCE)
        idSnapIndex = self._gv.topo.getIndex(snapLayer, QSLEEPTopology._ID)
        inletSnapIndex = self._gv.topo.getIndex(snapLayer, QSLEEPTopology._INLET)
        resSnapIndex = self._gv.topo.getIndex(snapLayer, QSLEEPTopology._RES)
        ptsourceSnapIndex = self._gv.topo.getIndex(snapLayer, QSLEEPTopology._PTSOURCE)
        fields = snapLayer.dataProvider().fields()
        count = 0
        errorCount = 0
        for feature in outletLayer.getFeatures():
            point = feature.geometry().asPoint()
            point1 = QSLEEPTopology.snapPointToReach(streamLayer, point, snapThreshold, self._gv.isBatch)
            if point1 is None: 
                errorCount += 1
                continue
            attrs = feature.attributes()
            pid = attrs[idIndex]
            inlet = attrs[inletIndex]
            res = attrs[resIndex]
            ptsource = attrs[ptsourceIndex]
            # QSLEEPUtils.information('Snap point at ({0:.2F}, {1:.2F})'.format(point1.x(), point1.y()), self._gv.isBatch)
            feature1 = QgsFeature()
            feature1.setFields(fields)
            feature1.setAttribute(idSnapIndex, pid)
            feature1.setAttribute(inletSnapIndex, inlet)
            feature1.setAttribute(resSnapIndex, res)
            feature1.setAttribute(ptsourceSnapIndex, ptsource)
            feature1.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(point1.x(), point1.y())))
            snapLayer.dataProvider().addFeatures([feature1])
            count += 1
        failMessage = '' if errorCount == 0 else ': {0!s} failed'.format(errorCount)
#         self._dlg.snappedLabel.setText('{0!s} snapped{1}'.format(count, failMessage))
        if self._gv.isBatch:
            QSLEEPUtils.information('{0!s} snapped{1}'.format(count, failMessage), True)
        if count == 0:
            QSLEEPUtils.error('Could not snap any points to stream reaches', self._gv.isBatch)
            return False
        # shows we have created a snap file
        self.snapFile = snapFile
#         self.snapErrors = (errorCount > 0)
        return True
    
#     def moveOutletToStream(self, fdr, streamRast, start, maxMoves):
#         """Return point on streamRast (positive value) by moving point at most maxMoves times from start along D8 directions."""
#         directions = gdal.Open(fdr, gdal.GA_ReadOnly)
#         directionsBand = directions.GetRasterBand(1)
#         transform = directions.GetGeoTransform()
#         streamLayer = QgsRasterLayer(streamRast, 'Streams')
#         nodata = streamLayer.dataProvider().srcNoDataValue(1)
#         QSLEEPUtils.loginfo(u'Stream raster nodata is {0}'.format(nodata))
#         x = start.x()
#         y = start.y()
#         col, row = QSLEEPTopology.projToCell(x, y, transform)
#         count = 0
#         while count < maxMoves:
#             streamValue = QSLEEPTopology.valueAtPoint(QgsPoint(x, y), streamLayer)
#             QSLEEPUtils.loginfo(u'Stream raster data at ({0},{1}) is {2}'.format(x, y, streamValue))
#             if streamValue and streamValue != nodata:
#                 directionsBand = None
#                 directions = None
#                 return QgsPoint(x, y)
#             if col < 0 or col >= directionsBand.XSize or row < 0 or row >=directionsBand.YSize:
#                 QSLEEPUtils.error(u'Your outlet point is outside the watershed.  Please move it inwards.', self._gv.isBatch)
#                 directionsBand = None
#                 directions = None
#                 return None
#             arr = directionsBand.ReadAsArray(col, row, 1, 1)
#             if arr is None:
#                 QSLEEPUtils.error(u'Your outlet point is outside the watershed.  Please move it inwards.', self._gv.isBatch)
#                 directionsBand = None
#                 directions = None
#                 return None
#             direction = arr[0,0]
#             if direction == 1: # E
#                 col += 1
#             elif direction == 2: # NE
#                 col += 1
#                 row -= 1
#             elif direction == 3: # N
#                 row -= 1
#             elif direction == 4: # NW
#                 col -= 1
#                 row -= 1
#             elif direction == 5: # W
#                 col -= 1
#             elif direction == 6: # SW
#                 col -= 1
#                 row += 1
#             elif direction == 7: # S
#                 row += 1
#             elif direction == 8: # SE
#                 col += 1
#                 row += 1
#             else:
#                 break
#             x, y = QSLEEPTopology.cellToProj(col, row, transform)
#             count += 1
#         QSLEEPUtils.error(u'Failed to move outlet to stream in {0} steps'.format(maxMoves), self._gv.isBatch)
#         directionsBand = None
#         directions = None
#         return None
        
    def progress(self, msg):
        """Update progress label with message; emit message for display in testing."""
        QSLEEPUtils.progress(msg, self._dlg.progressLabel)
        if msg != '':
            self.progress_signal.emit(msg)
            
    ## signal for updating progress label
    progress_signal = pyqtSignal(str)
    
    def doClose(self):
        """Close form."""
        self._dlg.done(0)

    def readProj(self):
        """Read delineation data from project file."""
        proj = QgsProject.instance()
        title = proj.title()
        root = proj.layerTreeRoot()
        self._dlg.tabWidget.setCurrentIndex(0)
        demFile, found = proj.readEntry(title, 'delin/DEM', '')
        demLayer = None
        if found and demFile != '':
            demFile = QSLEEPUtils.join(self._gv.projDir, demFile)
            demLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), demFile, FileTypes._DEM, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        else:
            treeLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(FileTypes._DEM), root.findLayers())
            if treeLayer:
                layer = treeLayer.layer()
                possFile = QSLEEPUtils.layerFileInfo(layer).absoluteFilePath()
                if QSLEEPUtils.question('Use {0} as {1} file?'.format(possFile, FileTypes.legend(FileTypes._DEM)), self._dlg, True) == QMessageBox.Yes:
                    demLayer = layer
                    demFile = possFile
        if demLayer:
            self._gv.demFile = demFile
            self._dlg.selectDem.setText(self._gv.demFile)
            self.setDefaultNumCells(demLayer)
        else:
            self._gv.demFile = '' 
        verticalUnits, found = proj.readEntry(title, 'delin/verticalUnits', Delineation._METRES)
        if found:
            self._gv.verticalUnits = verticalUnits
            self._gv.setVerticalFactor()
        threshold, found = proj.readNumEntry(title, 'delin/threshold', 0)
        if found and threshold > 0:
            try:
                self._dlg.numCells.setText(str(threshold))
            except Exception:
                pass # leave default setting
        wshedFile, found = proj.readEntry(title, 'delin/wshed', '')
        wshedLayer = None
        ft = FileTypes._EXISTINGSUBBASINS
        if found and wshedFile != '':
            wshedFile = QSLEEPUtils.join(self._gv.projDir, wshedFile)
            wshedLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), wshedFile, ft, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        else:
            treeLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(ft), root.findLayers())
            if treeLayer:
                layer = treeLayer.layer()
                possFile = QSLEEPUtils.layerFileInfo(layer).absoluteFilePath()
                if QSLEEPUtils.question('Use {0} as {1} file?'.format(possFile, FileTypes.legend(ft)), self._dlg, True) == QMessageBox.Yes:
                    wshedLayer = layer
                    wshedFile = possFile
        if wshedLayer:
            self._gv.wshedFile = wshedFile
        else:
            self._gv.wshedFile = ''
        burnFile, found = proj.readEntry(title, 'delin/burn', '')
        burnLayer = None
        if found and burnFile != '':
            burnFile = QSLEEPUtils.join(self._gv.projDir, burnFile)
            burnLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), burnFile, FileTypes._BURN, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        else:
            treeLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(FileTypes._BURN), root.findLayers())
            if treeLayer:
                layer = treeLayer.layer()
                possFile = QSLEEPUtils.layerFileInfo(layer).absoluteFilePath()
                if QSLEEPUtils.question('Use {0} as {1} file?'.format(possFile, FileTypes.legend(FileTypes._BURN)), self._dlg, True) == QMessageBox.Yes:
                    burnLayer = layer
                    burnFile = possFile
        if burnLayer:
            self._gv.burnFile = burnFile
            self._dlg.checkBurn.setChecked(True)
            self._dlg.selectBurn.setText(burnFile)
        else:
            self._gv.burnFile = ''
        streamFile, found = proj.readEntry(title, 'delin/net', '')
        streamLayer = None
        if found and streamFile != '':
            streamFile = QSLEEPUtils.join(self._gv.projDir, streamFile)
            streamLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), streamFile, FileTypes._STREAMS, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        else:
            treeLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(FileTypes._STREAMS), root.findLayers())
            if treeLayer:
                layer = treeLayer.layer()
                possFile = QSLEEPUtils.layerFileInfo(layer).absoluteFilePath()
                if QSLEEPUtils.question('Use {0} as {1} file?'.format(possFile, FileTypes.legend(FileTypes._STREAMS)), self._dlg, True) == QMessageBox.Yes:
                    streamLayer = layer
                    streamFile = possFile
        if streamLayer:
            self._gv.streamFile = streamFile
        else:
            self._gv.streamFile = ''
        useOutlets, found = proj.readBoolEntry(title, 'delin/useOutlets', True)
        if found:
            self._dlg.useOutlets.setChecked(useOutlets)
            self.changeUseOutlets()
        outletFile, found = proj.readEntry(title, 'delin/outlets', '')
        outletLayer = None
        if found and outletFile != '':
            outletFile = QSLEEPUtils.join(self._gv.projDir, outletFile)
            outletLayer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), outletFile, FileTypes._OUTLETS, self._gv, None, QSLEEPUtils._WATERSHED_GROUP_NAME)
        else:
            treeLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(FileTypes._OUTLETS), root.findLayers())
            if treeLayer:
                layer = treeLayer.layer()
                possFile = QSLEEPUtils.layerFileInfo(layer).absoluteFilePath()
                if QSLEEPUtils.question('Use {0} as {1} file?'.format(possFile, FileTypes.legend(FileTypes._OUTLETS)), self._dlg, True) == QMessageBox.Yes:
                    outletLayer = layer
                    outletFile = possFile
        if outletLayer:
            self._gv.outletFile = outletFile
            self._dlg.selectOutlets.setText(self._gv.outletFile)
        else:
            self._gv.outletFile = ''
            
    def saveProj(self):
        """Write delineation data to project file."""
        proj = QgsProject.instance()
        title = proj.title()
        proj.writeEntry(title, 'delin/net', QSLEEPUtils.relativise(self._gv.streamFile, self._gv.projDir))
        proj.writeEntry(title, 'delin/wshed', QSLEEPUtils.relativise(self._gv.wshedFile, self._gv.projDir))
        proj.writeEntry(title, 'delin/DEM', QSLEEPUtils.relativise(self._gv.demFile, self._gv.projDir))
        proj.writeEntry(title, 'delin/useOutlets', self._dlg.useOutlets.isChecked())
        proj.writeEntry(title, 'delin/outlets', QSLEEPUtils.relativise(self._gv.outletFile, self._gv.projDir))
        proj.writeEntry(title, 'delin/burn', QSLEEPUtils.relativise(self._gv.burnFile, self._gv.projDir))
        try:
            numCells = int(self._dlg.numCells.text())
        except Exception:
            numCells = 0
        proj.writeEntry(title, 'delin/verticalUnits', self._gv.verticalUnits)
        proj.writeEntry(title, 'delin/threshold', numCells)
        proj.write()
            
        
