This repository holds the source code for the QGIS plugin QSLEEP3, which is an assistant for 
the Soil-Landscape Estimation and Evaluatio Program.

## Build
The repository holds an Eclipse project.  It was created on Windows and probably only runs asis in a Windows Eclipse.

There is a Makefile that should provide enough information to build QSLEEP3.  It has only been run on Windows.
For Linux ertainly the path to doxygen will need checking, as will QGISDIR.

Note that the only interesting project in the Makefile is QSLEEP3.   

### Environment variables
The following need to be set to run make:

HOME: 				User's home directory 

OSGEO4W\_ROOT:  	C:\\Program Files\\QGIS 3.4

PATH: 				C:\\MinGW\\bin;C:\\MinGW\\msys\\1.0\\bin (assuming this is correct placement of MinGW, needed for mingw32-make, mkdir, etc)

PYTHONHOME: 		%OSGEO4W\_ROOT%\\apps\\Python37

QSWAT\_PROJECT: 	QSLEEP3

### Additional packages

The following packages are required to be installed:

* statsmodels (which will install pandas and patsy)

* sklearn (which will install click and joblib)
