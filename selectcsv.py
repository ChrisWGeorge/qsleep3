# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                             -------------------
        begin                : 2016-05-21
        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# Import the PyQt and QGIS libraries
from PyQt5.QtCore import * # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
from qgis.gui import * # @UnusedWildImport
from qgis.analysis import QgsRasterCalculator, QgsRasterCalculatorEntry
import os.path
import numpy
import math
import sys
sys.path.append(os.path.dirname(__file__))
import statsmodels.api as sm
# from sklearn.model_selection import GridSearchCV
from sklearn import linear_model  # @UnusedImport
import csv
import shutil
import processing  # @UnresolvedImport

# import the dialogs
from .selectcsvdialog import SelectCSVDialog
from .selectvarsdialog import SelectVarsDialog
from .regressiondialog import RegressionDialog
from .QSLEEPUtils import QSLEEPUtils, fileWriter, FileTypes

class SelectCSV(QObject):
    """Make soil rasters for sand, slit, clay ets for each layer."""
    _SOILATTR = 'SoilAtr'
    _INTERCEPT = 'Intercept'
    ## list of lists of predictors likely to be correlated: now replaced by calculated list
#     dependencies = [['savi', 'ndvi'], ['plc', 'aatplc'], ['profc', 'aatprofc'], ['curv', 'aatcurv'], 
#                     ['degslp', 'aatdegslp', 'pctslp', 'aatpctslp'], ['aspect', 'aataspect']]
    ## minimum correlation
    minCorrel = 0.03
    ## minimum number of predictors
    minCount = 2
    ## maximum correlation between predictors
    maxPredictorCorrel = 0.5
    
    def __init__(self, gv, reportsBox):
        """Constructor."""
        QObject.__init__(self)
        self._dlg = SelectCSVDialog()
        self._dlg.setWindowFlags(self._dlg.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self._gv = gv
        ## Reference to map name -> (file, layer) for soil rasters.
        self.rasters = self._gv.rasters
        ## select vars dialogue
        self.selectDlg = SelectVarsDialog()
        self.selectDlg.setWindowFlags(self.selectDlg.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        ## regression dialogue
        self.regressionDialog = RegressionDialog()
        self.regressionDialog.setWindowFlags(self.regressionDialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        ## input csv file with soil points data
        self.CSVFile = ''
        ## headers from input csv file
        self.headers = []
        ## name of facet class column
        self.facetClassName = QSLEEPUtils._FACETCLASS
        ## name of soil attribute to be modeled next
        self.attributeName = ''
        ## path of raster currently being generated automatically
        self.currentRaster = ''
        ## list of candidate predictors
        self.candidates = []
        ## list of selected predictors
        self.predictors = []
        ## reference to reports combo box on main form
        self.reportsBox = reportsBox
        ## dependencies: list of pairs of rasters
        self.dependencies = []
        
    def run(self):
        """Run forms."""
        # in case of rerun
        self.candidates = []
        self.predictors = []
        self._dlg.selectCSV.setText(self._gv.soilDataCSV)
        self._dlg.selectCSVButton.clicked.connect(self.selectCSVFile)
        self._dlg.loadCSVButton.clicked.connect(self.loadCSVFile)
        self.selectDlg.viewCorrelationsButton.clicked.connect(self.startCorrelate)
        self.selectDlg.cancelButton.clicked.connect(self.cancelSelect)
        self.selectDlg.doneButton.clicked.connect(self.doneSelect)
        self.selectDlg.attributeCombo.currentIndexChanged.connect(self.clearRaster)
        if os.path.isfile(self._gv.automateCSV):
            self.selectDlg.csvFile.setText(self._gv.automateCSV)
        self.selectDlg.csvButton.clicked.connect(self.selectAutomateCSV)
        self.selectDlg.runButton.clicked.connect(self.runAutomate)
        self.regressionDialog.suggestButton.clicked.connect(self.makeSuggest)
        self.regressionDialog.calcButton.clicked.connect(self.calculate)
        self.regressionDialog.nextButton.clicked.connect(self.cancelRegress)
        self.regressionDialog.doneButton.clicked.connect(self.doneRegress)
        self.regressionDialog.clearButton.clicked.connect(self.regressionDialog.rasterList.clear)
        self.regressionDialog.chooseRaster.activated.connect(self.chooseRaster)
        self.regressionDialog.selectRaster.clicked.connect(self.selectRaster)
        self.regressionDialog.outputRaster.setText('')
        self.fillChoose()
        self._dlg.show()
        _ = self._dlg.exec_()
        return self.runSelect()
        
    def selectCSVFile(self):
        """Select the input csv file."""
        settings = QSettings()
        if settings.contains('/QSLEEP/LastInputPath'):
            path = str(settings.value('/QSLEEP/LastInputPath'))
        else:
            path = ''
        title = QSLEEPUtils.trans('Select soil data CSV file')
        filtr = 'Comma Separated Value (*.csv *.CSV);;All files (*)'
        self.CSVFile, _ = QFileDialog.getOpenFileName(None, title, path, filtr)
        if self.CSVFile != '':
            settings.setValue('/QSLEEP/LastInputPath', os.path.dirname(str(self.CSVFile)))
            self._dlg.selectCSV.setText(self.CSVFile)
            
    def selectAutomateCSV(self):
        """Select the automate CSV file."""
        settings = QSettings()
        if settings.contains('/QSLEEP/LastInputPath'):
            path = str(settings.value('/QSLEEP/LastInputPath'))
        else:
            path = ''
        title = QSLEEPUtils.trans('Select CSV file for automatic raster generation')
        filtr = 'Comma Separated Value (*.csv *.CSV);;All files (*)'
        autoCSVFile, _ = QFileDialog.getOpenFileName(None, title, path, filtr)
        if autoCSVFile != '':
            settings.setValue('/QSLEEP/LastInputPath', os.path.dirname(str(autoCSVFile)))
            # copy to sleepDir if necessary
            inInfo = QFileInfo(autoCSVFile)
            inDir = inInfo.absoluteDir()
            outDir = QDir(self._gv.sleepDir)
            if inDir != outDir:
                shutil.copy(autoCSVFile, self._gv.sleepDir)
                autoCSVFile = QSLEEPUtils.join(self._gv.sleepDir, os.path.split(autoCSVFile)[1])
            self.selectDlg.csvFile.setText(autoCSVFile)
            self._gv.automateCSV = autoCSVFile
            proj = QgsProject.instance()
            title = proj.title()
            proj.writeEntry(title, 'soil/automateCSV', QSLEEPUtils.relativise(self._gv.automateCSV, self._gv.projDir))
            proj.write()
        
    def runAutomate(self):
        """Generate rasters automatically."""
        if self.selectDlg.facetClassCombo.currentIndex() < 0:
            QSLEEPUtils.error('Please select the facet class column', self._gv.isBatch)
            return
        if self.selectDlg.csvFile.text() == '' or not os.path.exists(self.selectDlg.csvFile.text()):
            QSLEEPUtils.information('Please select a csv file for automatic raster creation', self._gv.isBatch)
            return
        self._gv.automateCSV = self.selectDlg.csvFile.text()
        self.selectDlg.setCursor(Qt.WaitCursor)
        self.candidates = self.selectDlg.predictorList.selectedItems()
        if self.selectDlg.ignoreCorrelations.isChecked():
            self.dependencies = []
        else:
            self.collectDependencies()
        rasters = []
        try:
            self.facetClassName = self.selectDlg.facetClassCombo.currentText()
            with open(self._gv.automateCSV) as autoData:
                readr = csv.reader(autoData)
                _ = next(readr)  # skip headers
                for row in readr:
                    if len(row) > 1:
                        field = row[0]
                        name = row[1]
                        raster = QSLEEPUtils.join(self._gv.sleepDir, '{0}.tif'.format(name))
                        QSLEEPUtils.progress('{0} ...'.format(name), self.selectDlg.rasterLabel)
                        self.generateRaster(field, raster)
                        rasters.append(raster)
        finally:
            QSLEEPUtils.progress('', self.selectDlg.rasterLabel)
            self.selectDlg.setCursor(Qt.ArrowCursor)                
        if len(rasters) > 0:
            msg = 'Rasters generated in SLEEP folder:\n'
            for raster in rasters:
                msg += '  {0}\n'.format(os.path.split(raster)[1])
            msg += 'plus their smooth and median filtered versions.'
            QSLEEPUtils.information(msg, self._gv.isBatch)
            # write results to csv file
            soilDataLayer = QgsVectorLayer(self._gv.soilDataFile, 'Soil data', 'ogr')
            soilDataCSV = QSLEEPUtils.join(self._gv.sleepDir, self._gv.soilDataCSV)
            if os.path.exists(soilDataCSV):
                os.remove(soilDataCSV)
            # deprecated
            #error = QgsVectorFileWriter.writeAsVectorFormat(soilDataLayer, soilDataCSV, "UTF-8", soilDataLayer.crs(), driverName="CSV")
            ## options for creating csv
            csvOptions = QgsVectorFileWriter.SaveVectorOptions()
            csvOptions.ActionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
            csvOptions.driverName = "CSV"
            csvOptions.fileEncoding = "UTF-8"
            error = QgsVectorFileWriter.writeAsVectorFormatV2(soilDataLayer, soilDataCSV, QgsCoordinateTransformContext(), csvOptions)
            if error[0] != QgsVectorFileWriter.NoError:
                QSLEEPUtils.error('Failed to write final soil data csv file: error {0}'.format(error), self._gv.isBatch)
            
    def generateRaster(self, field, raster):
        """Generate raster for field automatically."""
        self.attributeName = field
        self.currentRaster = raster
        correlations = self.collectCorrelations()
        if correlations is None:
            return
        keepAll = self.selectDlg.keepSelected.isChecked()
        selection = []
        for (predictor, r) in correlations:
            if r < self.minCorrel and len(selection) >= self.minCount and not keepAll:
                break
            if keepAll or self.independent(predictor, selection):
                selection.append(predictor)
        self.predictors = selection
        self.calculateResults(isAuto=True)    
            
    def loadCSVFile(self):
        """Load the input csv file and run form."""
        self.CSVFile = self._dlg.selectCSV.text()
        if self.CSVFile == '' or not os.path.exists(self.CSVFile):
            QSLEEPUtils.information('Please select a CSV file', self._gv.isBatch)
            return
        self._dlg.close()
        
    def runSelect(self):
        """Run the sekection dialogue."""
        
        facetsFile = self._gv.rasters[QSLEEPUtils._FACETCLASS][0]
        if facetsFile is not None:
            facetsLayer = QSLEEPUtils.getLayerByLegend(FileTypes.legend(FileTypes._FACETS), QgsProject.instance().layerTreeRoot().findLayers())
            if facetsLayer is None:
                self.loadRaster(facetsFile)
        self.selectDlg.facetClassCombo.clear()
        self.selectDlg.attributeCombo.clear()
        self.selectDlg.predictorList.clear()
        if not os.path.isfile(self.CSVFile):
            return 0
        with open(self.CSVFile) as csvData:
            readr = csv.reader(csvData)
            row = next(readr)
            self.headers = row
            self.selectDlg.facetClassCombo.addItems(row)
            fcIndex = self.selectDlg.facetClassCombo.findText(self.facetClassName)
            if fcIndex >= 0:
                self.selectDlg.facetClassCombo.setCurrentIndex(fcIndex)
                self.selectDlg.attributeCombo.addItems(row[0:fcIndex])
                self.selectDlg.predictorList.addItems(row[fcIndex+1:])
                self.selectDlg.predictorList.selectAll()
            else:
                self.selectDlg.attributeCombo.addItems(row)
                self.selectDlg.predictorList.addItems(row)
        if self.selectDlg.attributeCombo.count() == 0:
            QSLEEPUtils.error('No soil attribute columns found in CSV file {0}'.format(self.CSVFile), self._gv.isBatch)
            return
        if self.selectDlg.predictorList.count() == 0:
            QSLEEPUtils.error('No predictor columns found in CSV file {0}'.format(self.CSVFile), self._gv.isBatch)
            return
        self.selectDlg.show()
        result = self.selectDlg.exec_()
        QSLEEPUtils.loginfo('Select csv result is {0}'.format(result))
        return result
            
    def cancelSelect(self):
        """Cancel."""
        self.cancelRegress()
        self._dlg.done(0)
        self.selectDlg.done(0)
        
    def doneSelect(self):
        """Finish with whatever written so far."""
        self.doneRegress()
        self._dlg.done(1)
        self.selectDlg.done(1)
        
    def cancelRegress(self):
        """Cancel regression form and return to selection form."""
        self.regressionDialog.done(0)
        
    def doneRegress(self):
        """Finish regression form (and selection form) with whatever written so far."""
        self.regressionDialog.done(1)
        
    def startCorrelate(self):
        """Start correlation form."""
        if self.selectDlg.facetClassCombo.currentIndex() < 0:
            QSLEEPUtils.error('Please select the facet class column', self._gv.isBatch)
            return
        if self.selectDlg.attributeCombo.currentIndex() < 0:
            QSLEEPUtils.error('Please select the soil attribute column', self._gv.isBatch)
            return
        self.candidates = self.selectDlg.predictorList.selectedItems()
        if not self.candidates or len(self.candidates) == 0:
            QSLEEPUtils.error('Please select the candidate predictors to be used', self._gv.isBatch)
            return
        self.facetClassName = self.selectDlg.facetClassCombo.currentText()
        self.attributeName = self.selectDlg.attributeCombo.currentText()
        self.showCorrelations()
        self.regressionDialog.show()
        result = self.regressionDialog.exec_()
        if result == 1:
            self.selectDlg.done(1)
            self._dlg.done(1)
            
    def showCorrelations(self):
        """Fill table with predictors and correlations, sorted high correlations to low."""
        table = self.regressionDialog.tableWidget
        table.setRowCount(len(self.candidates))
        table.setHorizontalHeaderLabels(['Predictor', self.attributeName])
        table.setSortingEnabled(False)
        table.clearSelection()
        self.collectDependencies()
        correlations = self.collectCorrelations()
        if correlations is None:
            return
        i = 0
        for (name, r) in correlations:
            table.setItem(i, 0, QTableWidgetItem(name))
            table.setItem(i, 1, QTableWidgetItem("{0:.3f}".format(abs(r))))
            i += 1
        
    def collectCorrelations(self):
        """Return a list of (predictor, correlation coefficient), sorted high to low."""
        correlations = []
        attVals = []
        predVals = dict()
        for item in self.candidates:
            name = item.text()
            predVals[name] = []
        errorCount = 0
        with open(self.CSVFile, 'r') as csvData:
            readr = csv.DictReader(csvData)
            for row in readr:
                try:
                    attVal = float(row[self.attributeName])
                except ValueError:
                    # null or non-numeric value
                    errorCount += 1
                    continue
                except KeyError:
                    QSLEEPUtils.error('Cannot find field {0}'.format(self.attributeName), self._gv.isBatch)
                    return None
                attVals.append(attVal)
                for item in self.candidates:
                    name = item.text()
                    predVals[name].append(float(row[name]))
        if errorCount > 0:
            QSLEEPUtils.information('{0} data points ignored for null or non-numeric values in {1} field'.format(errorCount, self.attributeName), self._gv.isBatch)
        if len(attVals) == 0:
            QSLEEPUtils.error('No numeric data found for field {0}'.format(self.attributeName), self._gv.isBatch)
            return None
        for (name, vals) in predVals.items():
            r = numpy.corrcoef(attVals, vals)[0, 1]
            correlations.append((name, abs(r)))
        correlations.sort(key=lambda x: x[1], reverse=True)
        return correlations
    
    def collectDependencies(self):
        """Fill in dependencies list with pairs of items with correlation above maxPredictorCorrel."""
        predVals = dict()
        QSLEEPUtils.loginfo('Condidates: {0}'.format(str([item.text() for item in self.candidates])))
        for item in self.candidates:
            name = item.text()
            predVals[name] = []
        with open(self.CSVFile, 'r') as csvData:
            readr = csv.DictReader(csvData)
            for row in readr:
                for item in self.candidates:
                    name = item.text()
                    predVals[name].append(float(row[name])) 
        # calculate correlations between predictors
        # and add pairs to dependencies if r > self.maxPredictorCorrel
        self.dependencies = []
        predictors = list(predVals.keys())
        numPredictors = len(predictors)
        for i in range(numPredictors):
            namei = predictors[i]
            for j in range(i+1, numPredictors):
                namej = predictors[j]
                r = numpy.corrcoef(predVals[namei], predVals[namej])[0, 1]
                # QSLEEPUtils.loginfo('{0}-{1}: {2:.2f}'.format(namei, namej, float(r)))
                if abs(float(r)) > self.maxPredictorCorrel:
                    self.dependencies.append([namei, namej])
        QSLEEPUtils.loginfo('Dependencies: {0}'.format(str(self.dependencies))) 
        
    def makeSuggest(self):
        """Select at most minimum number of independent correlated predictors, if have at least minimum correlation."""
        table = self.regressionDialog.tableWidget
        table.setFocus()
        table.sortItems(1, Qt.DescendingOrder)
        table.clearSelection()
        selection = []
        for i in range(table.rowCount()):
            predictor = table.item(i, 0).text()
            r = float(table.item(i, 1).text())
            if r < self.minCorrel and len(selection) >= self.minCount:
                break
            if self.independent(predictor, selection):
                selection.append(predictor)
                item = table.item(i, 0)
                item.setSelected(True)
        
    def independent(self, predictor, selection):
        """Return true of predictors not expected to be correlated."""
        for predicted in selection:
            for deps in self.dependencies:
                if predictor in deps and predicted in deps:
                    return False
        return True  
    
    def calculate(self):
        """Create output raster, using least squares best fit for each facet class."""
        # check output raster selected
        if self.regressionDialog.outputRaster.text() == '':
            QSLEEPUtils.error('Please select an output raster file', self._gv.isBatch)
            return
        # make list of selected predictor variable names
        self.predictors = []
        table = self.regressionDialog.tableWidget
        self.regressionDialog.setCursor(Qt.WaitCursor)
        for i in range(table.rowCount()):
            item = table.item(i, 0)
            if item.isSelected():
                name = item.text()
                self.predictors.append(name)
        if len(self.predictors) == 0:
            QSLEEPUtils.error('You need to select some predictors before generating a raster', self._gv.isBatch)
            self.regressionDialog.setCursor(Qt.ArrowCursor)
            return
        QSLEEPUtils.loginfo('Selection is {0}'.format(self.predictors))
        self.calculateResults(isAuto=False)
        self.regressionDialog.setCursor(Qt.ArrowCursor)
        
    def calculateResults(self, isAuto=False):
        """Write output raster and summaries from selected predictors using least squares best fit for each facet class."""
        
        allCandidates = self.predictors # [item.text() for item in self.candidates]
        # collect predictor data in a dictionary facetclass -> predictor -> value-list
        xData = dict()
        # collect soil attribute data in dictionary facetclass -> value-list
        yData = dict()
        with open(self.CSVFile, 'r') as csvData:
            readr = csv.DictReader(csvData)
            for row in readr:
                facetClass = int(row[self.facetClassName])
                if facetClass not in xData:
                    xData[facetClass] = dict()
                try:
                    yVal = float(row[self.attributeName])
                except:
                    # null or non-numeric value: reported earlier
                    continue
                if facetClass in yData:
                    yData[facetClass].append(yVal)
                else:
                    yData[facetClass] = [yVal]
                for name in allCandidates: # self.predictors for just OLS
                    val = float(row[name])
                    if name in xData[facetClass]:
                        xData[facetClass][name].append(val)
                    else:
                        xData[facetClass][name] = [val]
        results = []
        summaries = dict()
        minmax = dict() if self.selectDlg.preserveRange.isChecked() else None
        for facetClass, predVals in xData.items():
#             cols = []
#             for pred in self.predictors:
#                 vals = predVals[pred]
#                 #QSLEEPUtils.loginfo(u'{0} for facet class {1} has {2} values'.format(pred, facetClass, len(vals)))
#                 cols.append(numpy.array(vals))
#             x = numpy.column_stack(cols)
#             # x = sm.add_constant(x, prepend=True)
#             y = numpy.array(yData[facetClass])
#             #QSLEEPUtils.loginfo(u'y shape: {0}; x shape: {1}'.format(y.shape, x.shape))
#             # res = sm.OLS(y, x).fit()
#             #QSLEEPUtils.loginfo(u'Parameters: {0}'.format(res.params))
#             #QSLEEPUtils.loginfo(u'BSE: {0}'.format(res.bse))
#             #QSLEEPUtils.loginfo(u'Summary: {0}'.format(res.summary()))
#             rego = linear_model.LinearRegression()
#             rego.fit(x, y)
#             result = dict()
#             result['Type'] = 'OLS'
#             result[self._SOILATTR] = self.attributeName
#             result[self._INTERCEPT] = rego.intercept_
#             result[self.facetClassName] = facetClass
#             for candidate in allCandidates:
#                 try:
#                     i = self.predictors.index(candidate)
#                     result[candidate] = rego.coef_[i]
#                 except:
#                     result[candidate] = '-'
#             result['alpha'] = '-'
#             result['mse'] = numpy.mean((y - rego.predict(x)) ** 2)
#             result['R2'] = rego.score(x, y)
#             results.append(result)
#             # for Ridge, Lasso and ElasticNet use all candidates
#             cols = []
#             for pred in allCandidates:
#                 vals = predVals[pred]
#                 cols.append(numpy.array(vals))
#             x = numpy.column_stack(cols)
#             regr = linear_model.RidgeCV(alphas=numpy.logspace(-6, 6, 13))
#             regr.fit(x, y)
#             result = dict()
#             result['Type'] = 'Ridge'
#             result[self._SOILATTR] = self.attributeName
#             result[self._INTERCEPT] = regr.intercept_
#             result[self.facetClassName] = facetClass
#             for i in xrange(len(allCandidates)):
#                 result[allCandidates[i]] = regr.coef_[i]
#             result['alpha'] = regr.alpha_
#             result['mse'] = numpy.mean((y - regr.predict(x)) ** 2)
#             result['R2'] = regr.score(x, y)
#             results.append(result)
#             cols = []
#             for pred in allCandidates:
#                 vals = predVals[pred]
#                 cols.append(numpy.array(vals))
#             x = numpy.column_stack(cols)
#             regl = linear_model.LassoCV()
#             regl.fit(x, y)
#             result = dict()
#             result['Type'] = 'Lasso'
#             result[self._SOILATTR] = self.attributeName
#             result[self._INTERCEPT] = regl.intercept_
#             result[self.facetClassName] = facetClass
#             for i in xrange(len(allCandidates)):
#                 result[allCandidates[i]] = regl.coef_[i]
#             result['alpha'] = regl.alpha_
#             result['mse'] = numpy.mean((y - regl.predict(x)) ** 2)
#             result['R2'] = regl.score(x, y)
#             results.append(result)
#             cols = []
#             for pred in allCandidates:
#                 vals = predVals[pred]
#                 cols.append(numpy.array(vals))
#             x = numpy.column_stack(cols)
#             rege = linear_model.ElasticNetCV(l1_ratio=[.1, .5, .7, .9, .95, .99, 1])
#             rege.fit(x, y)
#             result = dict()
#             result['Type'] = 'ElasticNet'
#             result[self._SOILATTR] = self.attributeName
#             result[self._INTERCEPT] = rege.intercept_
#             result[self.facetClassName] = facetClass
#             for i in xrange(len(allCandidates)):
#                 result[allCandidates[i]] = rege.coef_[i]
#             result['alpha'] = rege.alpha_
#             result['mse'] = numpy.mean((y - rege.predict(x)) ** 2)
#             result['R2'] = rege.score(x, y)
#             results.append(result)
            yVals = yData[facetClass]
            if minmax is not None:
                minmax[facetClass] = (min(yVals), max(yVals))
            cols = []
            for pred in self.predictors:
                vals = predVals[pred]
                cols.append(numpy.array(vals))
            x = numpy.column_stack(cols)
            x = sm.add_constant(x, prepend=True)
            y = numpy.array(yVals)
            res = sm.OLS(y, x).fit()
            result = dict()
            result['Type'] = 'Statsmodels OLS'
            result[self._SOILATTR] = self.attributeName
            result[self._INTERCEPT] = res.params[0]
            result[self.facetClassName] = facetClass
            for i in range(len(self.predictors)):
                result[self.predictors[i]] = res.params[i+1]
            result['alpha'] = '-'
            result['mse'] = numpy.mean((y - res.predict(x)) ** 2)
            result['R2'] = res.rsquared
            results.append(result)
            summaries[facetClass] = res.summary(self.attributeName, ['const'] + self.predictors)
        if isAuto:
            self.doWriteRaster(results, self.currentRaster, minmax)
        else:
            self.writeRaster(results)
        self.writeResults(results, isAuto=isAuto)
        self.writeSummary(summaries, isAuto=isAuto)
        
    def writeResults(self, results, isAuto=False):
        """Write the predictors and coefficients to a csv file."""
        resultHeaders = list(results[0].keys())
        if isAuto:
            raster = self.currentRaster
        else:
            raster = self.regressionDialog.outputRaster.text()
        resultsCSV = os.path.splitext(raster)[0] + '.csv'
        with open(resultsCSV, 'w') as CSVFile:
            w = csv.DictWriter(CSVFile, fieldnames=resultHeaders, restval=0)
            w.writeheader()
            w.writerows(results)
            
    def writeSummary(self, summaries, isAuto=False):
        """Copy the results summary to a text file, and add it to the reports box."""
        if isAuto:
            raster = self.currentRaster
        else:
            raster = self.regressionDialog.outputRaster.text()
        resultsTxt = os.path.splitext(raster)[0] + '.txt'
        with fileWriter(resultsTxt) as w:
            for facetClass, summary in summaries.items():
                w.writeLine('Facet class {0}:'.format(facetClass))
                w.writeLine(summary.as_text().replace('\n', fileWriter._END_LINE))
                w.writeLine('')
        self.reportsBox.setVisible(True)
        txt = os.path.split(resultsTxt)[1]
        if self.reportsBox.findText(txt) < 0:
            self.reportsBox.addItem(txt)
            
    def fillChoose(self):
        """Fill the combo box of rasters to be written."""
        for num in range(1, len(self._gv.layers)+1):
            for name in QSLEEPUtils._RASTERNAMES:
                self.regressionDialog.chooseRaster.addItem(name + str(num))
                
    def chooseRaster(self):
        """Create a file name for the selected raster name."""
        dlg = self.regressionDialog
        dlg.outputRaster.setText(QSLEEPUtils.join(self._gv.sleepDir,
                                                  dlg.chooseRaster.currentText() + '.tif'))

    def selectRaster(self):
        """Select a raster file to be written."""
        settings = QSettings()
        if settings.contains('/QSLEEP/LastOutputPath'):
            path = str(settings.value('/QSLEEP/LastOutputPath'))
        else:
            path = self._gv.sleepDir
        title = QSLEEPUtils.trans('Select a raster file')
        filtr = 'GeoTiff (*.tif *.TIF)'
        raster, _ = QFileDialog.getSaveFileName(None, title, path, filtr)
        if raster != '':
            settings.setValue('/QSLEEP/LastOutputPath', os.path.dirname(str(raster)))
            self.regressionDialog.outputRaster.setText(raster)
           
    def writeRaster(self, results):
        """Write a raster file.  Also write a 3x3 smoothed version.  Add name to list of created rasters."""
        raster = self.regressionDialog.outputRaster.text()
        rasterList = self.regressionDialog.rasterList
        # remove raster name from rasters generated (so can see when finished)
        for i in range(rasterList.count()):
            item = rasterList.item(i)
            if item.text() == raster:
                rasterList.takeItem(i)
        self.doWriteRaster(results, raster)
        rasterList.addItem(raster) 
        rasterList.scrollToBottom() 
        
    def doWriteRaster(self, results, raster, minmax):
        """Write ouput raster plus 3x3 smoothed version.""" 
        root = QgsProject.instance().layerTreeRoot()
        QSLEEPUtils.removeLayerAndFiles(raster, root)
        # map from FacetClass to formula
        classFormulae = dict()
        # list of file identifiers
        idents = set() if self.rasters[self.facetClassName][0] is None else set([self.facetClassName])
        for result in results:
            facetClass = result[self.facetClassName]
            rng = None if minmax is None else minmax[facetClass]
            classFormulae[facetClass] = self.generateFormula(result, idents, rng)
            QSLEEPUtils.loginfo(u'Formula for attribute {0} class {1}: {2}'.format(self.facetClassName, facetClass, classFormulae[facetClass]))
        formula = classFormulae[1] if len(classFormulae) == 1 else self.combineFormulae(classFormulae) 
        self.runQGISCalc(formula, idents, raster)
        smoothRaster = raster.replace('.tif', 'smooth.tif')
        # processing.runalg("saga:simplefilter","sand1.tif",0,0,1,"sand1smooth.tif")
        square = 0
        smooth = 0
        radius = 1
        QSLEEPUtils.removeLayerAndFiles(smoothRaster, root)
        if self._gv.qgisIs2_6:
            result = processing.runalg('saga:simplefilter', raster, square, smooth, radius, smoothRaster)
        else:
            params =  {'INPUT': raster,
                       'MODE': square,
                       'METHOD': smooth,
                       'RADIUS':1,
                       'RESULT': QSLEEPUtils.tempFile('.sdat')}
            result = processing.run('saga:simplefilter', params)
        if not result:
            QSLEEPUtils.error('Smoothing filter failed', self._gv.isBatch)
            return
        # convert .sdat files to .tif 
        command = 'gdal_translate -of GTiff "{0}" "{1}"'
        os.system(command.format(result['RESULT'], smoothRaster))
        assert os.path.exists(smoothRaster), 'failed to create smooth raster {0}'.format(smoothRaster) 
        # add median filter (rank filter with rank set to 50)
        # processing.runalg("saga:rankfilter","bd1.tif",0,1,50,3,"bd1median.tif") 
        medianRaster = raster.replace('.tif', 'median.tif')
        rank = 50 
        QSLEEPUtils.removeLayerAndFiles(medianRaster, root)
        if self._gv.qgisIs2_6:
            result = processing.runalg('saga:rankfilter', raster, square, radius, rank, medianRaster)
        else:
            params = {'INPUT': raster,
                       'MODE': square,
                       'RADIUS': 1,
                       'RANK': rank,
                       'RESULT': QSLEEPUtils.tempFile('.sdat')}
            result = processing.run('saga:rankfilter', params)
        if not result:
            QSLEEPUtils.error('Median filter failed', self._gv.isBatch)
            return
        os.system(command.format(result['RESULT'], medianRaster))
        assert os.path.exists(medianRaster), 'failed to create median raster {0}'.format(medianRaster)
        # calculate R2 values for each facet class
        # dictionary is facetClass -> [values]
        raw = 0
        smooth = 1
        median = 2
        observed = 3
        # writing newxt two lines as 
        # rawProvider = QgsRasterLayer(raster, 'raw').dataProvider()
        # causes a minidump
        rawLayer = QgsRasterLayer(raster, 'raw')
        rawProvider = rawLayer.dataProvider()
        smoothLayer = QgsRasterLayer(smoothRaster, 'smooth')
        smoothProvider = smoothLayer.dataProvider()
        medianLayer = QgsRasterLayer(medianRaster, 'median')
        medianProvider = medianLayer.dataProvider()
        data = {raw: dict(), smooth: dict(), median: dict(), observed: dict()}
        # collector for data regardless of facet
        allData = {raw: [], smooth: [], median: [], observed: []}
        soilDataLayer = QgsVectorLayer(self._gv.soilDataFile, 'Soil data', 'ogr')
        soilProvider = soilDataLayer.dataProvider()
        # allow attribute fields to exist already (as may be a rerun)
        base = os.path.splitext(os.path.split(raster)[1])[0]
        attRaw = base + '_r'
        attSmooth = base + '_s'
        attMedian = base + '_m'
        attributes = [attRaw, attSmooth, attMedian]
        toDelete = []
        for i in range(3):  # step through raw, smooth, median
            index = soilProvider.fieldNameIndex(attributes[i])
            if index >= 0:
                toDelete.append(index)
        soilProvider.deleteAttributes(toDelete)
        soilDataLayer.updateFields()
        for i in range(3):  # step through raw, smooth, median
            field = QgsField(attributes[i], QVariant.Double, len=6, prec=3)
            soilProvider.addAttributes([field])
        soilDataLayer.updateFields()
        indexes = [-1, -1, -1]
        for i in range(3):  # step through raw, smooth, median
            indexes[i] = soilProvider.fieldNameIndex(attributes[i])
        fileType0 = soilProvider.wkbType()
        fileType = QgsWkbTypes.flatType(fileType0)  # ignore any Z or M values
        attValues = dict()
        for f in soilProvider.getFeatures():
            geom = f.geometry()
            point = geom.asPoint()
            if fileType == QgsWkbTypes.Point:
                point = geom.asPoint()
            elif fileType == QgsWkbTypes.MultiPoint:
                point = geom.asMultiPoint()[0]
            else:
                QSLEEPUtils.error('Strange type {0} for soil data points'.format(fileType0))
                return
            facetClass = f[self.facetClassName]
            if facetClass not in data[observed]:
                for d in list(data.values()):
                    d[facetClass] = []
            try:
                observedVal = float(f[self.attributeName])
            except:
                # null or non-numeric - reported earlier
                continue
#             rawVal = rawProvider.identify(point, QgsRaster.IdentifyFormatValue).results()[1]
#             smoothVal = smoothProvider.identify(point, QgsRaster.IdentifyFormatValue).results()[1]
#             medianVal = medianProvider.identify(point, QgsRaster.IdentifyFormatValue).results()[1]
            rawVal, ok = rawProvider.sample(point, 1)
            if not ok:
                continue
            smoothVal, ok = smoothProvider.sample(point, 1)
            if not ok:
                continue
            medianVal, ok = medianProvider.sample(point, 1)
            if not ok:
                continue
            # try to filter out nodata values
            if math.isnan(rawVal) or math.isnan(smoothVal) or math.isnan(medianVal):
                continue
            values = dict()
            data[observed][facetClass].append(observedVal)
            data[raw][facetClass].append(rawVal)
            values[indexes[0]] = rawVal
            data[smooth][facetClass].append(smoothVal)
            values[indexes[1]] = smoothVal
            data[median][facetClass].append(medianVal)
            values[indexes[2]] = medianVal
            attValues[f.id()] = values
            allData[observed].append(observedVal)
            allData[raw].append(rawVal)
            allData[smooth].append(smoothVal)
            allData[median].append(medianVal)
        soilProvider.changeAttributeValues(attValues)
        resultFile = raster.replace('.tif', 'R2.txt')
        with open(resultFile, 'w') as f:
            for facetClass, observedList in data[observed].items():
                f.write('Facet class {0}:\n'.format(facetClass))
                #f.write(str(['{0:.2F}'.format(v) for v in observedList][:10]))
                #f.write(str(['{0:.2F}'.format(v) for v in data[raw][facetClass]][:10]))
                f.write('Coefficients of determination (R2):\n')
                f.write('  Raw: {0:.2F}\n'.format(SelectCSV.calcR2(observedList, data[raw][facetClass])))
                #f.write(str(['{0:.2F}'.format(v) for v in data[smooth][facetClass]][:10]))
                f.write('  Smooth: {0:.2F}\n'.format(SelectCSV.calcR2(observedList, data[smooth][facetClass])))
                #f.write(str(['{0:.2F}'.format(v) for v in data[median][facetClass]][:10]))
                f.write('  Median: {0:.2F}\n'.format(SelectCSV.calcR2(observedList, data[median][facetClass])))
                f.write('Nash-Sutcliffe Efficiency:\n')
                nse = SelectCSV.calcNash(observedList, data[raw][facetClass])
                if nse is None:
                    f.write('  Not available: input data is missing or constant')
                else:
                    f.write('  Raw: {0:.2F}\n'.format(nse))
                    f.write('  Smooth: {0:.2F}\n'.format(SelectCSV.calcNash(observedList, data[smooth][facetClass])))
                    f.write('  Median: {0:.2F}\n'.format(SelectCSV.calcNash(observedList, data[median][facetClass])))
            f.write('Overall:\n')
            f.write('Coefficients of determination (R2):\n')
            f.write('  Raw: {0:.2F}\n'.format(SelectCSV.calcR2(allData[observed], allData[raw])))
            f.write('  Smooth: {0:.2F}\n'.format(SelectCSV.calcR2(allData[observed], allData[smooth])))
            f.write('  Median: {0:.2F}\n'.format(SelectCSV.calcR2(allData[observed], allData[median])))
            f.write('Nash-Sutcliffe Efficiency:\n')
            nse = SelectCSV.calcNash(allData[observed], allData[raw])
            if nse is None:
                f.write('  Not available: input data is missing or constant')
            else:
                f.write('  Raw: {0:.2F}\n'.format(nse))
                f.write('  Smooth: {0:.2F}\n'.format(SelectCSV.calcNash(allData[observed], allData[smooth])))
                f.write('  Median: {0:.2F}\n'.format(SelectCSV.calcNash(allData[observed], allData[median])))
        self.reportsBox.setVisible(True)
        txt = os.path.split(resultFile)[1]
        if self.reportsBox.findText(txt) < 0:
            self.reportsBox.addItem(txt)
        self.loadRaster(smoothRaster)
        
    def loadRaster(self, raster):
        rasterName = os.path.split(raster)[1]
        if rasterName == 'organic1smooth.tif':
            _, _ = QSLEEPUtils.getLayerByFilename([], raster, FileTypes._ORGANIC, 
                                                  self._gv, None, QSLEEPUtils._RESULTS_GROUP_NAME)
        elif rasterName == 'clay1smooth.tif':
            _, _ = QSLEEPUtils.getLayerByFilename([], raster, FileTypes._CLAY, 
                                                  self._gv, None, QSLEEPUtils._RESULTS_GROUP_NAME)
        elif rasterName == 'silt1smooth.tif':
            _, _ = QSLEEPUtils.getLayerByFilename([], raster, FileTypes._SILT, 
                                                  self._gv, None, QSLEEPUtils._RESULTS_GROUP_NAME)
        elif rasterName == 'sand1smooth.tif':
            _, _ = QSLEEPUtils.getLayerByFilename([], raster, FileTypes._SAND, 
                                                  self._gv, None, QSLEEPUtils._RESULTS_GROUP_NAME)
        elif rasterName == 'rock1smooth.tif':
            _, _ = QSLEEPUtils.getLayerByFilename([], raster, FileTypes._ROCK, 
                                                  self._gv, None, QSLEEPUtils._RESULTS_GROUP_NAME)
        elif rasterName == 'facetclass.tif':
            _, _ = QSLEEPUtils.getLayerByFilename([], raster, FileTypes._FACETS, 
                                                  self._gv, None, QSLEEPUtils._RESULTS_GROUP_NAME)
    
    @staticmethod            
    def calcR2(l1, l2):
        """Calculate R2 for lists l1, l2"""
        r = numpy.corrcoef(l1, l2)[0, 1]
        return r ** 2
    
    @staticmethod
    def calcNash(o, s):
        """Return Nash-Sutcliffe efficiency for observed sequence o and simulated s."""
        n = len(o)
        if n == 0:
            return None
        mean_o = sum(o) / n
        num = 0
        den = 0
        for oi, si in zip(o, s):
            di = oi - si
            do = oi - mean_o
            num += di * di
            den += do * do
        return None if den == 0 else 1 - (num / den)
    
    def generateFormula(self, result, idents, rng):
        """Create a formula for use with the QGIS raster calculator."""
        formula = ''
        first = True
        for key, val in result.items():
            if key not in {self._SOILATTR, self.facetClassName, 'Type', 'alpha', 'mse', 'R2'}:
                if first:
                    first = False
                else:
                    formula += ' + '
                if key == self._INTERCEPT:
                    formula += str(val)
                else:
                    idents.add(key)
                    # we will get raw values from the raster, so assume that mean and standard deviation
                    # calculated from the sample values are OK for all values, and use these to standardize
                    mean = self._gv.means[key]
                    std = self._gv.stds[key]
                    factor = val/std
                    formula += '({0}@1 - {1}) * {2}'.format(key, mean, factor)
                    # old code for GRASS 
                    #letter = self.fileToLetter(key, idents)
                    # zero values from files seem to be interpreted as nulls, so we include a check for null
                    #formula += '(isnull({0})?0:{0}*{1})'.format(letter, val)
        if rng is None:
            return formula
        else:
            return 'min({0}, max({1}, {2}))'.format(rng[1], rng[0], formula) 
         
    
    def combineFormulae(self, classFormulae):
        """Combine formulae for different facet class values.
        
        Generates a formula of the form (where fc is facet class name)
        (fc = 0) * A + (fc = 1) * B + (fc = 2) * C + ...
        which is effectively
        if fc = 0 then A else if fc = 1 then B else if fc = 2 then C ... else 0
        since (fc = n) is 1 if fc's value is n and 0 otherwise.
        """
        formula = ''
        first = True
        for key, val in classFormulae.items():
            if first:
                first = False
            else:
                formula += ' + '
            formula += '({0}@1 = {1}) * ({2})'.format(self.facetClassName, key, val)
        return formula   
    
    def runQGISCalc(self, formula, idents, outFile):
        """Run QGIS raster calculator."""
        entries = [self.generateEntry(ident) for ident in idents]
        assert len(entries) > 0, 'No entries for QGIS calculator'
        layer = self.rasters['fil'][1]
        calc = QgsRasterCalculator(formula, outFile, 'GTiff', layer.extent(), layer.width(), layer.height(), entries, QgsCoordinateTransformContext())
        result = calc.processCalculation(feedback=None)
        if result == 0:
            assert os.path.exists(outFile), 'QGIS calculator formula {0} failed to write output'.format(formula)
            QSLEEPUtils.copyPrj(self.rasters['fil'][0], outFile)
            return True
        QSLEEPUtils.error('QGIS calculator formula {0} failed: returned {1}'.format(formula, result), self._gv.isBatch)
        return False        
    
    def generateEntry(self, ident):
        """Generate a file entry in a QGIS raster calculator formula."""
        entry = QgsRasterCalculatorEntry()
        entry.bandNumber = 1
        entry.raster = self.rasters[ident][1]
        entry.ref = ident + '@1'
        return entry
    
    def clearRaster(self):
        """Clear soil raster box to avoid danger of overwriting previous soil raster."""
        self.regressionDialog.outputRaster.setText('')
        
