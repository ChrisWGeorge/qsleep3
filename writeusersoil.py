# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                             -------------------
        begin                : 2016-05-21
        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# Import the PyQt and QGIS libraries
from PyQt5.QtCore import * # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
from qgis.gui import * # @UnusedWildImport
import os
from osgeo import gdal
import csv
import math
import numpy

from .QSLEEPUtils import QSLEEPUtils #, fileWriter, FileTypes
from .QSLEEPTopology import QSLEEPTopology

class UserSoil:
    """Create user_soil.csv."""
    
    _CSVNAME = 'user_soil.csv'
    _CSVHEADERS = ['Soil ID', 'SNAME', 'NLAYERS', 'HYDGRP']
    _LAYERHEADERS = ['Z', 'OC', 'Clay', 'Silt', 'Sand', 'Rock']
    _MAXLAYERSPLUS1 = 11
    
    def __init__(self, gv):
        """Constructor."""
        self._gv = gv
        ## layer number of first raster, which is used as reference for others
        self.refNum = -1
        ## name of reference raster
        self.refName = ''
        ## geo transform of reference raster
        self.refTransform = None
        ## dictionary layer number -> name -> RasterData
        self.rastersData = dict()
        ## name of output cvs file
        self.CSVFileName = ''
        
    def run(self):
        """Run form."""
        gdal.AllRegister()
        try:
            if not self.checkFiles():
                return False
            writr, CSVFile = self.makeCSVFile(self._CSVNAME)
            self.writeData(writr)
            CSVFile.close()
            return True
        except Exception as e:
            QSLEEPUtils.error('Failed to write usersoil table: {0}'.format(str(e)))
            return False
        
    def checkFiles(self):
        """Check files exist for each layer: warn if not."""
        for num in range(1, len(self._gv.layers)+1):
            self.rastersData[num] = dict()
            for name in QSLEEPUtils._RASTERNAMES:
                rasterFile = QSLEEPUtils.join(self._gv.sleepDir, name + str(num) + 'smooth.tif')
                if not os.path.exists(rasterFile):
                    QSLEEPUtils.information('Warning: cannot find soil attribute raster {0}.  Values will be zero.'.format(rasterFile))
                else:
                    rasterData = RasterData(rasterFile)
                    if self.refNum < 0:
                        # first file = use as reference
                        self.refNum = num
                        self.refName = name
                        self.refTransform = rasterData.transform
                        rasterData.hasRefTransform = True
                    else:
                        rasterData.hasRefTransform = (rasterData.transform == self.refTransform)
                    self.rastersData[num][name] = rasterData
        return True
    
    def makeCSVFile(self, name):
        """Create csv file and write headers."""
        self.CSVFileName = QSLEEPUtils.join(self._gv.sleepDir, name)
        headers = self._CSVHEADERS
        for num in range(1, self._MAXLAYERSPLUS1):
            headers.extend([name + str(num) for name in self._LAYERHEADERS])
        CSVFile = open(self.CSVFileName, 'w')
        csv.register_dialect('sleep', lineterminator='\n')
        w = csv.DictWriter(CSVFile, fieldnames=headers, restval='', dialect='sleep')
        w.writeheader()
        return w, CSVFile
    
    def writeData(self, writr):
        """Write data to csv file."""
        numLayers = len(self._gv.layers)
        rowData = dict()
        rowData[self._CSVHEADERS[2]] = numLayers
        soilId = 1
        refData = self.rastersData[self.refNum][self.refName]
        numRows = refData.numRows
        numCols = refData.numCols
        for row in range(numRows):
            refY = QSLEEPTopology.rowToY(row, self.refTransform)
            # read row data into buffers
            for num in range(1, numLayers+1):
                for name in QSLEEPUtils._RASTERNAMES:
                    if name in self.rastersData[num]:
                        data = self.rastersData[num][name]
                        nextRow = row if data.hasRefTransform else QSLEEPTopology.yToRow(refY, data.transform)
                        if nextRow != data.currentRow:
                            if 0 <= nextRow < data.numRows:
                                band = data.ds.GetRasterBand(1)
                                data.buffer = band.ReadAsArray(0, nextRow, data.numCols, 1)
                                data.currentRow = nextRow
                            else:
                                data.currentRow = -1
            for col in range(numCols):
                # build an output row and write it
                rowData[self._CSVHEADERS[0]] = soilId
                rowData[self._CSVHEADERS[1]] = 'S' + str(soilId)
                refX = QSLEEPTopology.colToX(col, self.refTransform)
                rowHasData = False
                for num in range(1, self._MAXLAYERSPLUS1):
                    zHead = self._LAYERHEADERS[0] + str(num)
                    zVal = 0 if num > numLayers else self._gv.layers[num]
                    rowData[zHead] = zVal
                    vals = {name : 0 for name in QSLEEPUtils._RASTERNAMES}
                    for name in QSLEEPUtils._RASTERNAMES:
                        if num in self.rastersData and name in self.rastersData[num]:
                            data = self.rastersData[num][name]
                            if data.currentRow >= 0:
                                nextCol = col if data.hasRefTransform else QSLEEPTopology.xToCol(refX, data.transform)
                                if 0 <= nextCol < data.numCols:
                                    val = data.buffer[0, nextCol]
                                    vals[name] = 0 if math.isnan(val) or val == data.noData  or val < 0 else val
                    if self.normalise(vals):
                        rowHasData = True
                    for nm, vl in vals.items():
                        rowData[self.rasterToHeader(nm) + str(num)] = vl
                if rowHasData:
                    writr.writerow(rowData)
                    soilId += 1
                    
    @staticmethod
    def rasterToHeader(rasterName):
        """Convert base name used for raster to base name of corresponding column header."""
        if rasterName == 'organic':
            return 'OC'
        if rasterName == 'clay':
            return 'Clay'
        if rasterName == 'silt':
            return 'Silt'
        if rasterName == 'sand':
            return 'Sand'
        if rasterName == 'rock':
            return 'Rock'
        raise NameError('Unknown raster name {0}'.format(rasterName))
        
    def normalise(self, vals):
        """
        Make values sum to 100 or all zero, and return true if not all zero, esle false.
        
        Assumes none are negative or nan.
        """
        namesToNormalise = ['clay', 'silt', 'sand']  # QSLEEPUtils._RASTERNAMES minus organic and rock
        summ = 0
        for name, val in vals.items():
            if name in namesToNormalise:
                summ += val
        if summ == 0:
            return False
        factor = 100 / float(summ)
        for name in namesToNormalise:
            vals[name] *= factor
        return True
        
class RasterData:
    """Data held about each input raster."""
    def __init__(self, rasterFile):
        """Constructor."""
        
        ## gdal data set
        self.ds = gdal.Open(rasterFile, gdal.GA_ReadOnly)
        ## number of rows 
        self.numRows = self.ds.RasterYSize
        ## number of columns
        self.numCols = self.ds.RasterXSize
        ## geo transform
        self.transform = self.ds.GetGeoTransform()
        ## true if same transform as reference raster
        self.hasRefTransform = False
        ## no data value
        self.noData = self.ds.GetRasterBand(1).GetNoDataValue()
        ## currentRow >= 0 iff buffer contains data for that row
        self.currentRow = -1
        ## buffer for one row
        self.buffer = numpy.empty([1, self.numCols], dtype=float)
                    
