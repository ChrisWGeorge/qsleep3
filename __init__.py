# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                             -------------------
        begin                : 2016-05-21
        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

__version__ = '1.11'

def classFactory(iface):
    # load QSleep class from file QSleep
    from .qsleep import QSleep
    return QSleep(iface, __version__)
