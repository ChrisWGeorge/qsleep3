# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSLEEP
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                              -------------------
        begin                : 2014-07-18
        copyright            : (C) 2014 by Chris George
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt5.QtCore import *  # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from PyQt5.QtXml import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
import os.path
import ntpath
import glob
import shutil
import random
import datetime
import time
import sys
from osgeo import gdal, ogr
import traceback

class QSLEEPUtils:
    """Various utilities."""
    
    _QSLEEPNAME = 'QSLEEP'
    _WATERSHED_GROUP_NAME = 'Watershed'
    _RESULTS_GROUP_NAME = 'Results'
    
    _FACETCLASS = 'facetclass'
    _RASTERNAMES = ['organic', 'clay', 'silt', 'sand', 'rock']
    
    _SNAPPEDLEGEND = 'Snapped inlets/outlets'
    _SELECTEDLEGEND = 'Selected inlets/outlets'
    _DRAWNLEGEND = 'Drawn inlets/outlets'
    _EXTRALEGEND = 'Extra inlets/outlets'
    _FULLHRUSLEGEND = 'Full HRUs'
    _ACTHRUSLEGEND = 'Actual HRUs'
    _HILLSHADELEGEND = 'Hillshade'
    _WATERSHEDLEGEND = 'Watershed'
    _GRIDLEGEND = 'Watershed grid'
    _GRIDSTREAMSLEGEND = 'Grid streams'
    ## x-offsets for TauDEM D8 flow directions, which run 1-8, so we use dir - 1 as index
    _dX = [1, 1, 0, -1, -1, -1, 0, 1]
    ## y-offsets for TauDEM D8 flow directions, which run 1-8, so we use dir - 1 as index
    _dY = [0, -1, -1, -1, 0, 1, 1, 1]
    
    @staticmethod
    def error(msg, isBatch=False):
        """Report msg as an error."""
        QSLEEPUtils.logerror(msg)
        if isBatch:
            # in batch mode we generally only look at stdout 
            # (to avoid distracting messages from gdal about .shp files not being supported)
            # so report to stdout
            sys.stdout.write('ERROR: {0}\n'.format(msg))
        else:
            msgbox = QMessageBox()
            msgbox.setWindowTitle(QSLEEPUtils._QSLEEPNAME)
            msgbox.setIcon(QMessageBox.Critical)
            msgbox.setText(QSLEEPUtils.trans(msg))
            msgbox.exec_()
        return
    
    @staticmethod
    def exceptionError(msg, isBatch=False):
        """Report msg plus exception details as an en error."""
        QSLEEPUtils.error('{0}: {1}'.format(msg, traceback.format_exc()), isBatch)
        return
    
    @staticmethod
    def question(msg, parent, affirm):
        """Ask msg as a question, returning Yes or No."""
        # only ask question if interactive (not eg testing)
        if parent and parent.isVisible():
            questionBox = QMessageBox()
            questionBox.setWindowTitle(QSLEEPUtils._QSLEEPNAME)
            questionBox.setIcon(QMessageBox.Question)
            questionBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            questionBox.setText(QSLEEPUtils.trans(msg))
            result = questionBox.exec_()
        else: # not interactive: use affirm parameter
            if affirm:
                result = QMessageBox.Yes
            else:
                result = QMessageBox.No
        if result == QMessageBox.Yes:
            res = ' Yes'
        else:
            res = ' No'
        QSLEEPUtils.loginfo(msg + res)
        if not (parent and parent.isVisible()):
            print('{0}\n'.format(msg + res))
        return result
    
    @staticmethod
    def information(msg, isBatch=False):
        """Report msg as information."""
        QSLEEPUtils.loginfo(msg)
        if isBatch:
            sys.stdout.write('{0}\n'.format(msg))
        else:
            msgbox = QMessageBox()
            msgbox.setWindowTitle(QSLEEPUtils._QSLEEPNAME)
            msgbox.setIcon(QMessageBox.Information)
            msgbox.setText(QSLEEPUtils.trans(msg))
            msgbox.exec_()
        return
    
    @staticmethod
    def loginfo(msg):
        """Log message as information."""
        app = QgsApplication.instance()
        # allow to fail if no application
        if app is not None:
            log = QgsApplication.messageLog()
            log.logMessage(msg, QSLEEPUtils._QSLEEPNAME, Qgis.Info)
        
    @staticmethod
    def logerror(msg):
        """Log message as error."""
        app = QgsApplication.instance()
        # allow to fail if no application
        if app is not None:
            log = QgsApplication.messageLog()
            log.logMessage(msg, QSLEEPUtils._QSLEEPNAME, Qgis.Critical)
        
    @staticmethod
    def trans(msg):
        """Translate msg according to current locale."""
        return QApplication.translate(QSLEEPUtils._QSLEEPNAME, msg, None)
    
    @staticmethod
    def join(path, fileName):
        """Use appropriate path separator."""
        if os.name == 'nt':
            return ntpath.join(path, fileName)
        else:
            return posixpath.join(path, fileName)
        
    @staticmethod
    def relativise(filePath, path):
        """If filePath exists, return filePath relative to path, else return empty string."""
        if os.path.exists(filePath):
            return os.path.relpath(filePath, path)
        else:
            return ''
        
    @staticmethod
    def samePath(p1, p2):
        """Return true if absolute paths of the two paths are the same."""
        return os.path.abspath(p1) == os.path.abspath(p2)
    
    @staticmethod
    def translateCoords(transform1, transform2, numRows1, numCols1):
        """
        Return a pair of functions:
        row, latitude -> row and column, longitude -> column
        for transforming positions in raster1 to row and column of raster2.
        
        The functions are:
        identities on the first argument if the rasters have (sufficiently) 
        the same origins and cell sizes;
        a simple shift on the first argument if the rasters have 
        the same cell sizes but different origins;
        otherwise a full transformation on the second argument.
        It is assumed that the first and second arguments are consistent, 
        ie they identify the same cell in raster1.
        """
        # may work, thuough we are comparing real values
        if transform1 == transform2:
            return (lambda row, _: row), (lambda col, _: col)
        xOrigin1, xSize1, _, yOrigin1, _, ySize1 = transform1
        xOrigin2, xSize2, _, yOrigin2, _, ySize2 = transform2
        # accept the origins as the same if they are within a tenth of the cell size
        sameXOrigin = abs(xOrigin1 - xOrigin2) < xSize2 * 0.1
        sameYOrigin = abs(yOrigin1 - yOrigin2) < abs(ySize2) * 0.1
        # accept cell sizes as equivalent if  vertical/horizontal difference 
        # in cell size times the number of rows/columns
        # in the first is less than half the depth/width of a cell in the second
        sameXSize = abs(xSize1 - xSize2) * numCols1 < xSize2 * 0.5
        sameYSize = abs(ySize1 - ySize2) * numRows1 < abs(ySize2) * 0.5
        if sameXSize:
            if sameXOrigin:
                xFun = (lambda col, _: col)
            else:
                # just needs origin shift
                # note that int truncates, i.e. rounds towards zero
                if xOrigin1 > xOrigin2:
                    colShift = int((xOrigin1 - xOrigin2) / xSize1 + 0.5)
                    xFun = lambda col, _: col + colShift
                else:
                    colShift = int((xOrigin2 - xOrigin1) / xSize1 + 0.5)
                    xFun = lambda col, _: col - colShift
        else:
            # full transformation
            xFun = lambda _, x: int((x - xOrigin2) / xSize2)
        if sameYSize:
            if sameYOrigin:
                yFun = (lambda row, _: row)
            else:
                # just needs origin shift
                # note that int truncates, i.e. rounds towards zero, and y size will be negative
                if yOrigin1 > yOrigin2:
                    rowShift = int((yOrigin2 - yOrigin1) / ySize1 + 0.5)
                    yFun = lambda row, _: row - rowShift
                else:
                    rowShift = int((yOrigin1 - yOrigin2) / ySize1 + 0.5)
                    yFun = lambda row, _: row + rowShift
        else:
            # full transformation
            yFun = lambda _, y: int((y - yOrigin2) / ySize2)
        # note row, column order of return (same as order of reading rasters)
        return yFun, xFun
    
    @staticmethod
    def colToX(col, transform):
        """Convert column number to X-coordinate."""
        return (col + 0.5) * transform[1] + transform[0]
    
    @staticmethod
    def rowToY(row, transform):
        """Convert row number to Y-coordinate."""
        return (row + 0.5) * transform[5] + transform[3]
    
    @staticmethod
    def copyPrj(inFile, outFile):
        """
        Copy .prj file, if it exists, from inFile to .prj file of outFile,
        unless outFile is .dat.
        """
        inBase = os.path.splitext(inFile)[0]
        (outBase, outSuffix) = os.path.splitext(outFile)
        if not outSuffix == '.dat':
            inPrj = inBase + '.prj'
            outPrj = outBase + '.prj'
            if os.path.exists(inPrj):
                shutil.copy(inPrj, outPrj)
            
    @staticmethod
    def isUpToDate(inFile, outFile):
        """Return true if inFile exists, outFile exists and is no older than inFile."""
        if not os.path.exists(inFile):
            return False
        if os.path.exists(outFile):
            if os.path.getmtime(outFile) >= os.path.getmtime(inFile):
                return True
        return False
    
    @staticmethod
    def progress(text, label):
        """Set label text and repaint."""
        if text == '':
            label.clear()
            label.update()
        else:
            label.setText(text)
            # shows on console if visible; more useful in testing when appears on standard output
            print(text)
            # calling processEvents after label.clear can cause QGIS to hang
            label.update()
            QCoreApplication.processEvents(QEventLoop.ExcludeUserInputEvents)
        
    @staticmethod
    def layerFileInfo(layer):
        """Return QFileInfo of raster or vector layer."""
        provider = layer.dataProvider()
        if isinstance(layer, QgsRasterLayer):
            return QFileInfo(provider.dataSourceUri())
        elif isinstance(layer, QgsVectorLayer):
            path  = provider.dataSourceUri()
            # vector data sources have additional "|layerid=0"
            pos = path .find('|')
            if pos >= 0:
                path  = path [:pos]
            return QFileInfo(path)
        return None
      
    @staticmethod
    def removeLayerAndFiles(fileName, root):
        """Remove any layers for fileName; delete files with same basename 
        regardless of suffix.
        """
        # sometimes fails to remove files so allow to fail: seems to do no harm as tools overwrite
        QSLEEPUtils.removeLayer(fileName, root)
        try:
            QSLEEPUtils.removeFiles(fileName)
        except:
            pass
          
    @staticmethod  
    def removeLayer(fileName, root):
        """Remove any layers for fileName."""
        fileInfo = QFileInfo(fileName)
        lIds = []
        layers = []
        for layer in root.findLayers():
            info = QSLEEPUtils.layerFileInfo(layer.layer())
            if info == fileInfo:
                lIds.append(layer.layerId())
                layers.append(layer)
        QgsProject.instance().removeMapLayers(lIds)
        for layer in layers:
            del layer
     
    @staticmethod
    def removeLayerByLegend(legend, treeLayers):
        """Remove any tree layers whose legend name starts with the legend."""
        lIds = []
        for treeLayer in treeLayers:
            name = treeLayer.name()
            if name.startswith(legend):
                lIds.append(treeLayer.layerId())
        QgsProject.instance().removeMapLayers(lIds)
        
    @staticmethod
    def getLayerByLegend(legend, treeLayers):
        """Find a layer if any whose legend name starts with the legend."""
        for layer in treeLayers:
            if layer.name().startswith(legend):
                return layer
        return None
        
    @staticmethod
    def removeAllFeatures(layer):
        """Remove all features from layer."""
        provider = layer.dataProvider()
        request = QgsFeatureRequest().setFlags(QgsFeatureRequest.NoGeometry)
        ids = [feature.id() for feature in provider.getFeatures(request)]
        return provider.deleteFeatures(ids)
        
    @staticmethod
    def setLayerVisibility(layer, visibility, root):
        """Set map layer visible or not according to visibility."""
        try:
            treeLayer = root.findLayer(layer.id())
            treeLayer.setItemVisibilityChecked(visibility)
        except Exception:
            # layer probably removed - just exit
            return
                
    @staticmethod
    def removeFiles(fileName):
        """
        Delete all files with same root as fileName, 
        i.e. regardless of suffix.
        """
        pattern = os.path.splitext(fileName)[0] + '.*'
        for f in glob.iglob(pattern):
            os.remove(f)
            
    @staticmethod
    def copyFiles(inInfo, saveDir):
        """
        Copy files with same basename as file with info  inInfo to saveDir, 
        i.e. regardless of suffix.
        """
        inFile = inInfo.fileName()
        inPath = inInfo.path()
        if inFile == 'sta.adf' or inFile == 'hdr.adf':
            # ESRI grid: need to copy top directory of inInfo to saveDir
            inDirName = inInfo.dir().dirName()
            savePath = QSLEEPUtils.join(saveDir, inDirName)
            # guard against trying to copy to itself
            inPath = os.path.normcase(os.path.abspath(inPath))
            savePath = os.path.normcase(os.path.abspath(savePath))
            if inPath != savePath:
                if os.path.exists(savePath):
                    shutil.rmtree(savePath)
                shutil.copytree(inPath, savePath, True, None)
        else:
            pattern = QSLEEPUtils.join(inPath, inInfo.baseName()) + '.*'
            for f in glob.iglob(pattern):
                shutil.copy(f, saveDir)
            
    @staticmethod
    def shapefileExists(infile) -> bool:
        """Assuming infile is.shp, check existence of .shp. .shx and .dbf."""
        if not os.path.isfile(infile):
            return False
        shxFile = infile.replace('.shp', '.shx')
        if not os.path.isfile(shxFile):
            return False
        dbfFile = infile.replace('.shp', '.dbf')
        if not os.path.isfile(dbfFile):
            return False
        return True
                
    @staticmethod
    def copyShapefile(infile, outbase, outdir):
        """Copy files with same basename as infile to outdir, setting basename to outbase."""
        pattern = os.path.splitext(infile)[0] + '.*'
        for f in glob.iglob(pattern):
            suffix = os.path.splitext(f)[1]
            outfile = QSLEEPUtils.join(outdir, outbase + suffix)
            shutil.copy(f, outfile)
                
    @staticmethod
    def getLayerByFilenameOrLegend(treeLayers, fileName, ft, legend, parent):
        """Look for file that should have a layer.  
        If not found by filename, try legend, either as given or by file type ft."""
        layer, _ = QSLEEPUtils.getLayerByFilename(treeLayers, fileName, ft, None, None, None)
        if layer:
            return layer
        lgnd = FileTypes.legend(ft) if legend == '' else legend
        treeLayer = QSLEEPUtils.getLayerByLegend(lgnd, treeLayers)
        if treeLayer:
            layer = treeLayer.layer()
            possFile = QSLEEPUtils.layerFileInfo(layer).absoluteFilePath()
            if QSLEEPUtils.question('Use {0} as {1} file?'.format(possFile, lgnd), parent, True) == QMessageBox.Yes:
                return layer
        return None
            
    @staticmethod        
    def getLayerByFilename(treeLayers, fileName, ft, gv, subLayer, groupName):
        """
        Return map layer for this fileName and flag to indicate if new layer, 
        loading it if necessary if groupName is not None.
        
        If groupName is not none, inserts the new layer in group, above sublayer if not none.
        """
        fileInfo = QFileInfo(fileName)
        for treeLayer in treeLayers:
            mapLayer = treeLayer.layer()
            if QSLEEPUtils.layerFileInfo(mapLayer) == fileInfo:
                return (mapLayer, False)
        # not found: load layer if requested
        if groupName is not None:
            legend = FileTypes.legend(ft)
            styleFile = FileTypes.styleFile(ft)
            baseName = fileInfo.baseName()
            if fileInfo.suffix() == 'adf':
                # ESRI grid: use directory name as baseName
                baseName = fileInfo.dir().dirName()
            if ft == FileTypes._OUTLETS:
                if baseName.endswith('_snap'):
                    legend = QSLEEPUtils._SNAPPEDLEGEND
                elif baseName.endswith('_sel'):
                    legend = QSLEEPUtils._SELECTEDLEGEND
                elif baseName == 'extra':
                    legend = QSLEEPUtils._EXTRALEGEND
                elif baseName == ('drawoutlets'):
                    legend = QSLEEPUtils._DRAWNLEGEND
            proj = QgsProject.instance()
            root = proj.layerTreeRoot()
            group = root.findGroup(groupName)
            if group is None:
                root.insertGroup(0, groupName)
            if subLayer is None:
                index: int = 0
            else:
                index = QSLEEPUtils.groupIndex(group, subLayer)
            if FileTypes.isRaster(ft):
                layer: QgRasterLayer = QgsRasterLayer(fileName, '{0} ({1})'.format(legend, baseName))
            else:
                ogr.RegisterAll()
                layer = QgsVectorLayer(fileName, '{0} ({1})'.format(legend, baseName), 'ogr')
            layer = proj.addMapLayer(layer, False)
            if layer is not None and group is not None:
                group.insertLayer(index, layer)
            if layer and layer.isValid():
                fun = FileTypes.colourFun(ft)
                if fun: 
                    fun(layer, gv.facetClassCount)
                elif not (styleFile is None or styleFile == ''):
                    layer.loadNamedStyle(QSLEEPUtils.join(gv.plugin_dir, styleFile))
                    layer.triggerRepaint()
                # save qml form of DEM style file if batch (no support for sld form for rasters)
                if gv.isBatch and ft == FileTypes._DEM:
                    qmlFile = QSLEEPUtils.join(gv.projDir, 'dem.qml')
                    (msg, OK) = layer.saveNamedStyle(qmlFile)
                    if not OK:
                        QSLEEPUtils.error('Failed to create dem.qml: {0}'.format(msg), gv.isBatch)
                return (layer, True)
            else:
                if layer is not None:
                    err: QgsError = layer.error()
                    msg: str = err.summary()
                else:
                    msg = 'layer is None'
                QSLEEPUtils.error('Failed to load {0}: {1}'.format(fileName, msg), gv.isBatch)
        return (None, False)
    
    @staticmethod
    def groupIndex(group, layer) -> int:
        """Find index of tree layer in group's children, defaulting to 0."""
        index = 0
        if group is None:
            return index
        for child in group.children():
            if QgsLayerTree.isLayer(child) and child == layer:
                return index
            index += 1
        return 0
    
    @staticmethod 
    def moveLayerToGroup(layer, groupName, 
                         root, gv):
        """Move map layer to start of group unless already in it, and return it."""
        QSLEEPUtils.loginfo('Moving {0} to group {1}'.format(layer.name(), groupName))
        group: QgsLayerTreeGroup = root.findGroup(groupName)
        if group is None:
            QSLEEPUtils.information('Internal error: cannot find group {0].'.format(groupName), gv.isBatch)
            return layer
        layerId = layer.id()
        # redundant code, but here to make a fast exit from usual situation of already in right group
        if group.findLayer(layerId) is not None:
            QSLEEPUtils.loginfo('Found layer in group {}'.format(groupName))
            return layer
        # find the group the layer is in
        currentGroup = root
        currentLayer = root.findLayer(layerId)
        if currentLayer is None:
            # not at the top level: check top level groups
            currentGroup = None
            for node in root.children():
                if QgsLayerTree.isGroup(node):
                    currentLayer = node.findLayer(layerId)
                    if currentLayer is not None:
                        if node == group: # already in required group
                            return layer
                        else:
                            currentGroup = node
                            break
        if currentGroup is None:
            # failed to find layer
            QSLEEPUtils.information(f'Trying to move layer {layer.name()} to group {groupName} but failed to find layer',
                                   gv.isBatch)
            return layer
        QSLEEPUtils.loginfo('Found layer in group {0}'.format(currentGroup.name()))
        # need to move from currentGroup to group
        QSLEEPUtils.loginfo('Layer to be cloned is {0}'.format(repr(layer)))
        cloneLayer: QgsLayerTreeLayer = layer.clone()
        QSLEEPUtils.loginfo('Cloned map layer is {0}'.format(repr(cloneLayer)))
        movedLayer: QgsLayerTreeLayer = group.insertLayer(0, cloneLayer)
        currentGroup.removeLayer(layer)
        QSLEEPUtils.loginfo('Moved tree layer is {0}'.format(repr(movedLayer)))
        QSLEEPUtils.loginfo('Moved map layer is {0}'.format(repr(movedLayer.layer())))
        return movedLayer.layer()
    
    @staticmethod  
    def printLayers(root, n):
        """Debug function for displaying tre and map laers."""
        layers = [(layer.name(), layer) for layer in root.findLayers()]
        mapLayers = \
            [(layer.layer().name(), layer.layer()) for layer in root.findLayers()]
        QSLEEPUtils.loginfo('{0}: layers: {1}'.format(n, repr(layers)))
        QSLEEPUtils.loginfo('{0}: map layers: {1}'.format(n, repr(mapLayers)))
        
    @staticmethod    
    def openAndLoadFile(root, ft, box, saveDir, gv, subLayer, groupName):
        """
        Use dialog to open file of FileType ft chosen by user, 
        add a layer for it if necessary, 
        copy files to saveDir, write path to box,
        and return file path and layer.
        """
        settings = QSettings()
        if settings.contains('/QSLEEP/LastInputPath'):
            path = str(settings.value('/QSLEEP/LastInputPath'))
        else:
            path = ''
        title = QSLEEPUtils.trans(FileTypes.title(ft))
        inFileName, _ = QFileDialog.getOpenFileName(None, title, path, FileTypes.filter(ft))
        if inFileName:
            settings.setValue('/QSLEEP/LastInputPath', os.path.dirname(str(inFileName)))
            # copy to saveDir if necessary
            inInfo = QFileInfo(inFileName)
            inDir = inInfo.absoluteDir()
            outDir = QDir(saveDir)
            if inDir != outDir:
                inFile = inInfo.fileName()
                if inFile == 'sta.adf' or inFile == 'hdr.adf':
                    # ESRI grid - whole directory will be copied to saveDir
                    inDirName = inInfo.dir().dirName()
                    if ft == FileTypes._DEM:
                        # will be converted to .tif, so make a .tif name
                        outFileName = QSLEEPUtils.join(saveDir, inDirName) + '.tif'
                    else:
                        outFileName = QSLEEPUtils.join(QSLEEPUtils.join(saveDir, inDirName), inFile)
                else:
                    outFileName = QSLEEPUtils.join(saveDir, inFile)
                    if ft == FileTypes._DEM:
                        # will be converted to .tif, so convert to .tif name
                        outFileName = os.path.splitext(outFileName)[0] + '.tif'
                # remove any existing layer for this file, else cannot copy to it
                QSLEEPUtils.removeLayer(outFileName, root)
                if ft == FileTypes._DEM:
                    # use GDAL CreateCopy to ensure result is a GeoTiff
                    inDs = gdal.Open(inFileName)
                    driver = gdal.GetDriverByName('GTiff')
                    # QSLEEPUtils.information(u'Creating {0}'.format(outFileName), gv.isBatch)
                    outDs = driver.CreateCopy(outFileName, inDs, 0)
                    if outDs is None:
                        QSLEEPUtils.error('Failed to create dem in geoTiff format', gv.isBatch)
                        return (None, None)
                    # may not have got projection information, so if any copy it
                    QSLEEPUtils.copyPrj(inFileName, outFileName)
                    del inDs
                    del outDs
                else:
                    QSLEEPUtils.copyFiles(inInfo, saveDir)
            else:
                outFileName = inFileName
            # this function will add layer if necessary
            layer, _ = QSLEEPUtils.getLayerByFilename(root.findLayers(), outFileName, ft, 
                                                   gv, subLayer, groupName)
            if not layer:
                return (None, None)
            # if no .prj file, try to create one
            # this is needed, for example, when DEM is created from ESRI grid
            # or if DEM is made by clipping
            QSLEEPUtils.writePrj(outFileName, layer)
            if box is not None:
                box.setText(outFileName)
            return (outFileName, layer)
        else:
            return (None, None)
        
    @staticmethod
    def getFeatureByValue(layer, indx, val):
        """Return feature in features whose attribute with index indx has value val."""
        for f in layer.getFeatures():
            v = f.attributes()[indx]
            if v == val:
                # QSLEEPUtils.loginfo(u'Looking for {0!s} found {1!s}: finished'.format(val, v))
                return f
            # QSLEEPUtils.loginfo(u'Looking for {0!s} found {1!s}: still looking'.format(val, v))
        return None
        
    @staticmethod
    def writePrj(fileName, layer):
        """If no .prj file exists for fileName, try to create one from the layer's crs."""
        prjFile = os.path.splitext(fileName)[0] + '.prj'
        if os.path.exists(prjFile):
            return
        try:
            srs = layer.crs()
            wkt = srs.toWkt()
            if not wkt:
                raise ValueError('Could not make WKT from CRS.')
            with fileWriter(prjFile) as fw:
                fw.writeLine(wkt)
        except Exception:
            QSLEEPUtils.information("Unable to make .prj file for {0}.  You may need to set this map's projection manually".format(fileName), False)
        
    @staticmethod
    def makeCurrent(string, combo):
        """Add string to combo box if not already present, and make it current."""
        index = combo.findText(string)
        if index < 0:
            combo.addItem(string)
            combo.setCurrentIndex(combo.count() - 1)
        else:
            combo.setCurrentIndex(index) 
        
    @staticmethod
    def parseSlopes(string):
        """
        Parse a slope limits string to list of intermediate limits.
        For example '[min, a, b, max]' would be returned as [a, b].
        """
        slopeLimits = []
        nums = string.split(',')
        # ignore first and last
        for i in range(1, len(nums) - 1):
            slopeLimits.append(float(nums[i]))
        return slopeLimits
        
    @staticmethod
    def slopesToString(slopeLimits):
        """
        Return a slope limits string made from a string of intermediate limits.
        For example [a, b] would be returned as '[0, a, b, 9999]'.
        """
        str1 = '[0, '
        for i in slopeLimits:
            # lose the decimal point if possible
            d = int(i)
            if i == d:
                str1 += ('{0!s}, '.format(d))
            else:
                str1 += ('{0!s}, '.format(i))
        return str1 + '9999]'
    
    @staticmethod
    def insertIntoSortedList(val, vals, unique):
        """
        Insert val into assumed sorted list vals.  
        If unique is true and val already in vals do nothing.
        Return true if insertion made.
        
        Note function is polymorphic: used for lists of integers and lists of strings
        """
        for index in range(len(vals)):
            nxt = vals[index]
            if nxt == val:
                if unique:
                    return False
                else:
                    vals.insert(index, val)
                    return True
            if nxt > val:
                vals.insert(index, val)
                return True
        vals.append(val)
        return True
            
    @staticmethod
    def date():
        """Retun today's date as day month year."""
        return datetime.date.today().strftime('%d %B %Y')
    
    @staticmethod
    def time():
        """Return the time now as hours.minutes."""
        return datetime.datetime.now().strftime('%H.%M')
    
    @staticmethod
    def fileBase(SWATBasin, relhru):
        """
        Return the string used to name SWAT input files 
        from basin and relative HRU number.
        """
        return '{0:05d}{1:04d}'.format(SWATBasin, relhru)
    
    @staticmethod
    def getSlsubbsn(meanSlope):
        """Estimate the average slope length in metres from the mean slope."""     
        if meanSlope < 0.01: return 120
        elif meanSlope < 0.02: return 100
        elif meanSlope < 0.03: return 90
        elif meanSlope < 0.05: return 60
        else: return 30
        
    @staticmethod
    def tempFolder():
        """Make temporary QSLEEP folder and return its absolute path."""
        tempDir = QSLEEPUtils.join(str(QDir.tempPath()), QSLEEPUtils._QSLEEPNAME)
        if not QDir(tempDir).exists():
            QDir().mkpath(tempDir)
        return tempDir
    
    @staticmethod
    def tempFile(suffix):
        """Make a new temporary file in tempFolder with suffix."""
        base = 'tmp' + str(time.process_time()).replace('.','')
        folder = QSLEEPUtils.tempFolder()
        fil = QSLEEPUtils.join(folder, base + suffix)
        if os.path.exists(fil):
            time.sleep(1)
            return QSLEEPUtils.tempFile(suffix)
        return fil
        
    @staticmethod
    def deleteTempFolder():
        """Delete the temporary folder and its contents."""
        folder = QSLEEPUtils.tempFolder()
        if QDir(folder).exists():
            shutil.rmtree(folder, True)
        
    @staticmethod
    def quote(strng):
        """Quote a string."""
        return '"' + strng + '"'
    
    @staticmethod
    def unixifyPath(path):
        """Replace back slashes with forward."""
        return path.replace('\\', '/')
    
    @staticmethod
    def rasterToPolygonShapefile(rasterFile, polyShapefile, attId, crs, root, isBatch):
        """Create shapefile polyShapefile from rasterFile, using attId as the identifier."""
        QSLEEPUtils.removeLayer(polyShapefile, root)
        driver = ogr.GetDriverByName('ESRI Shapefile')
        if driver is None:
            QSLEEPUtils.error('ESRI Shapefile driver is not available - cannot write watershed shapefile', isBatch)
            return
        if os.path.exists(polyShapefile):
            driver.DeleteDataSource(polyShapefile)
        ds = driver.CreateDataSource(polyShapefile)
        if ds is None:
            QSLEEPUtils.error('Cannot create watershed shapefile {0}'.format(polyShapefile), isBatch)
            return
        fileInfo = QFileInfo(polyShapefile)
        polyLayer = ds.CreateLayer(str(fileInfo.baseName()), geom_type=ogr.wkbPolygon)
        if polyLayer is None:
            QSLEEPUtils.error('Cannot create layer for shapefile {0}'.format(polyShapefile), isBatch)
            return
        idFieldDef = ogr.FieldDefn(attId, ogr.OFTInteger)
        if idFieldDef is None:
            QSLEEPUtils.error('Cannot create field {0}'.format(attId), isBatch)
            return
        index = polyLayer.CreateField(idFieldDef)
        if index != 0:
            QSLEEPUtils.error('Cannot create field {0} in {1}'.format(attId, polyShapefile), isBatch)
            return
        sourceRaster = gdal.Open(rasterFile)
        if sourceRaster is None:
            QSLEEPUtils.error('Cannot open raster {0}'.format(rasterFile), isBatch)
            return
        band = sourceRaster.GetRasterBand(1)
        nodata = band.GetNoDataValue()
        featuresToDelete = []
        # We could use band as a mask, but that removes polygons with id 0
        # so we run with no mask, which produces an unwanted polygon with attId
        # set to the rasterFile's nodata value.  This we will remove later.
        gdal.Polygonize(band, None, polyLayer, 0, [], callback=None)
        ds = None  # closes data source
        QSLEEPUtils.copyPrj(rasterFile, polyShapefile)
        polyLayer = QgsVectorLayer(polyShapefile, 'Polygon shapefile', 'ogr')
        polyLayer.setCrs(crs)
        polyLayer.startEditing()
        idIndex = polyLayer.fields().lookupField(attId)
        for feature in polyLayer.getFeatures():
            basin = feature.attributes()[idIndex]
            if basin == nodata:
                featuresToDelete.append(feature.id())
        # get rid of any basin corresponding to nodata in rasterFile
        if len(featuresToDelete) > 0:
            polyLayer.dataProvider().deleteFeatures(featuresToDelete)
        polyLayer.commitChanges()
        
    @staticmethod
    def cellToProj(col, row, transform):
        """Convert column and row numbers to (X,Y)-coordinates."""
        x = (col + 0.5) * transform[1] + transform[0]
        y = (row + 0.5) * transform[5] + transform[3]
        return (x,y)
        
    @staticmethod
    def projToCell(x, y, transform):
        """Convert (X,Y)-coordinates to column and row numbers."""
        col = int((x - transform[0]) / transform[1])
        row = int((y - transform[3]) / transform[5])
        return (col, row)
    
    @staticmethod
    def sameTransform(transform1, transform2, numRows1, numCols1):
        """Return true if transforms are sufficiently close to be regarded as the same,
        i.e. row and column numbers for the first can be used without transformation to read the second.  
        Avoids relying on equality between real numbers."""
        # may work, thuough we are comparing real values
        if transform1 == transform2:
            return True
        xOrigin1, xSize1, _, yOrigin1, _, ySize1 = transform1
        xOrigin2, xSize2, _, yOrigin2, _, ySize2 = transform2
        # accept the origins as the same if they are within a tenth of the cell size
        sameXOrigin = abs(xOrigin1 - xOrigin2) < xSize2 * 0.1
        if sameXOrigin:
            sameYOrigin = abs(yOrigin1 - yOrigin2) < abs(ySize2) * 0.1
            if sameYOrigin:
                # accept cell sizes as equivalent if  vertical/horizontal difference 
                # in cell size times the number of rows/columns
                # in the first is less than half the depth/width of a cell in the second
                sameXSize = abs(xSize1 - xSize2) * numCols1 < xSize2 * 0.5
                if sameXSize:
                    sameYSize = abs(ySize1 - ySize2) * numRows1 < abs(ySize2) * 0.5
                    return sameYSize
        return False

class fileWriter:
    
    """
    Class effectively extending writer with a writeLine method
    """
    
    # should be automatically changed for Windows, but isn't
    _END_LINE = os.linesep # '\r\n' for Windows
    
    def __init__(self, path):
        """Initialise class variables."""
        ## writer
        self.writer = open(path, 'w')
        ## write method
        self.write = self.writer.write
        ## close
        self.close = self.writer.close
    
    def writeLine(self, string):
        """Write string plus end-of-line."""
        self.writer.write(string + '\n')
        
    def __enter__(self):
        """Return self."""
        return self
        
    def __exit__(self, typ, value, traceback):  # @UnusedVariable
        """Close."""
        self.writer.close()
        
class FileTypes:
    
    """File types for various kinds of file that will be loaded, 
    and utility functions.
    """
    
    _DEM = 0
    _MASK = 1
    _BURN = 2
    _OUTLETS = 3
    _STREAMS = 4
    _SUBBASINS = 5
    _LANDUSES = 6
    _SOILS = 7
    _SLOPEBANDS = 8
    _REACHES = 9
    _WATERSHED = 10
    _EXISTINGSUBBASINS = 11
    _EXISTINGWATERSHED = 12
    _HILLSHADE = 13
    _GRID = 14
    _GRIDSTREAMS = 15
    _STREAMRASTER = 16
    _ORGANIC = 17
    _CLAY = 18
    _SILT = 19
    _SAND = 20
    _ROCK = 21
    _FACETS = 22
    
    @staticmethod
    def filter(ft):
        """Return filter for open file dialog according to file type."""
        if FileTypes.isRaster(ft):
            return QgsProviderRegistry.instance().fileRasterFilters()
        elif ft == FileTypes._MASK:
            return 'All files (*)' # TODO: use dataprovider.fileRasterFilters + fileVectorFilters
        else:
            return QgsProviderRegistry.instance().fileVectorFilters()
    
    @staticmethod
    def isRaster(ft) -> bool:
        if ft == FileTypes._DEM or ft == FileTypes._LANDUSES or ft == FileTypes._SOILS or \
                ft == FileTypes._SLOPEBANDS or ft == FileTypes._HILLSHADE or ft == FileTypes._STREAMRASTER or \
                ft == FileTypes._ORGANIC or ft == FileTypes._CLAY or ft == FileTypes._SILT or \
                ft == FileTypes._SAND or ft == FileTypes._ROCK or ft == FileTypes._FACETS:
            return True
        else:
            return False
        
    @staticmethod
    def legend(ft):
        """Legend entry string for file type ft."""
        if ft == FileTypes._DEM:
            return 'DEM'
        elif ft == FileTypes._MASK:
            return 'Mask'
        elif ft == FileTypes._BURN:
            return 'Stream burn-in'
        elif ft == FileTypes._OUTLETS:
            return 'Inlets/outlets'
        elif ft == FileTypes._STREAMS or ft == FileTypes._STREAMRASTER:
            return 'Streams'
        elif ft == FileTypes._SUBBASINS or ft == FileTypes._EXISTINGSUBBASINS:
            return 'Subbasins'
        elif ft == FileTypes._LANDUSES:
            return 'Landuses'
        elif ft == FileTypes._SOILS:
            return 'Soils'
        elif ft == FileTypes._SLOPEBANDS:
            return 'Slope bands'
        elif ft == FileTypes._WATERSHED or ft == FileTypes._EXISTINGWATERSHED:
            return QSLEEPUtils._WATERSHEDLEGEND
        elif ft == FileTypes._REACHES:
            return 'Reaches'
        elif ft == FileTypes._HILLSHADE:
            return QSLEEPUtils._HILLSHADELEGEND
        elif ft == FileTypes._GRID:
            return QSLEEPUtils._GRIDLEGEND
        elif ft == FileTypes._GRIDSTREAMS:
            return QSLEEPUtils._GRIDSTREAMSLEGEND
        elif ft == FileTypes._ORGANIC:
            return 'Organic'
        elif ft == FileTypes._CLAY:
            return 'Clay'
        elif ft == FileTypes._SILT:
            return 'Silt'
        elif ft == FileTypes._SAND:
            return 'Sand'
        elif ft == FileTypes._ROCK:
            return 'Rock'
        elif ft == FileTypes._FACETS:
            return 'Facets'
        
    @staticmethod
    def styleFile(ft):
        """.qml file, if any, for file type ft."""
        if ft == FileTypes._DEM:
            return None
        elif ft == FileTypes._MASK:
            return None
        elif ft == FileTypes._BURN:
            return None
        elif ft == FileTypes._OUTLETS:
            return 'outlets.qml'
        elif ft == FileTypes._STREAMS or ft == FileTypes._REACHES:
            return 'stream.qml'
        elif ft == FileTypes._SUBBASINS or ft == FileTypes._WATERSHED:
            return 'wshed.qml'
        elif ft == FileTypes._EXISTINGSUBBASINS or ft == FileTypes._EXISTINGWATERSHED:
            return 'existingwshed.qml'
        elif ft == FileTypes._GRID:
            return 'grid.qml'
        elif ft == FileTypes._GRIDSTREAMS:
            return 'gridstreams.qml'
        elif ft == FileTypes._LANDUSES:
            return None
        elif ft == FileTypes._SOILS:
            return None
        elif ft == FileTypes._SLOPEBANDS:
            return None
        elif ft == FileTypes._HILLSHADE:
            return None
        elif ft == FileTypes._STREAMRASTER:
            return None
        elif ft == FileTypes._ORGANIC:
            return None
        elif ft == FileTypes._CLAY:
            return None
        elif ft == FileTypes._SILT:
            return None
        elif ft == FileTypes._SAND:
            return None
        elif ft == FileTypes._ROCK:
            return None
        elif ft == FileTypes._FACETS:
            return None

    @staticmethod
    def title(ft):
        """Title for open file dialog for file type ft."""
        if ft == FileTypes._DEM:
            return 'Select DEM'
        elif ft == FileTypes._MASK:
            return 'Select mask'
        elif ft == FileTypes._BURN:
            return 'Select stream reaches shapefile to burn-in'
        elif ft == FileTypes._OUTLETS:
            return 'Select inlets/outlets shapefile'
        elif ft == FileTypes._STREAMS:
            return 'Select stream reaches shapefile'
        elif ft == FileTypes._SUBBASINS or ft == FileTypes._EXISTINGSUBBASINS:
            return 'Select watershed shapefile'
        elif ft == FileTypes._LANDUSES:
            return 'Select landuses file'
        elif ft == FileTypes._SOILS:
            return 'Select soils file'
        elif ft == FileTypes._SLOPEBANDS or ft == FileTypes._REACHES or ft == FileTypes._WATERSHED or \
                ft == FileTypes._EXISTINGWATERSHED or ft == FileTypes._HILLSHADE or ft == FileTypes._GRID or \
                ft == FileTypes._STREAMRASTER or \
                ft == FileTypes._ORGANIC or ft == FileTypes._CLAY or ft == FileTypes._SILT or \
                ft == FileTypes._SAND or ft == FileTypes._ROCK or ft == FileTypes._FACETS:
            return None
        
    @staticmethod
    def colourFun(ft):
        """Layer colouring function for raster layer of file type ft."""
        if ft == FileTypes._DEM:
            return FileTypes.colourDEM
        elif ft in {FileTypes._ORGANIC, FileTypes._CLAY, FileTypes._SILT, FileTypes._SAND, FileTypes._ROCK}:
            return FileTypes.colourSoil
        elif ft == FileTypes._FACETS:
            return FileTypes.colourFacets
        else:
            return None

    @staticmethod
    def colourDEM(layer, _):
        """Layer colouring function for DEM."""
        shader = QgsRasterShader()
        stats = layer.dataProvider().bandStatistics(1, QgsRasterBandStats.Min | QgsRasterBandStats.Max)
        minVal = int(stats.minimumValue + 0.5)
        maxVal = int(stats.maximumValue + 0.5)
        mean = (minVal + maxVal) // 2
        s1 = str((minVal * 2 + maxVal) // 3)
        s2 = str((minVal + maxVal * 2) // 3)
        item0 = \
            QgsColorRampShader.ColorRampItem(minVal, QColor(10, 100, 10), str(minVal) + ' - ' + s1)
        item1 = \
            QgsColorRampShader.ColorRampItem(mean, QColor(153, 125, 25), s1 + ' - ' + s2)
        item2 = \
            QgsColorRampShader.ColorRampItem(maxVal, QColor(255, 255, 255), s2 + ' - ' + str(maxVal))
        fcn = QgsColorRampShader(minVal, maxVal)
        fcn.setColorRampType(QgsColorRampShader.Interpolated)
        fcn.setColorRampItemList([item0, item1, item2])
        shader.setRasterShaderFunction(fcn)
        renderer = QgsSingleBandPseudoColorRenderer(layer.dataProvider(), 1, shader)
        layer.setRenderer(renderer)
        layer.triggerRepaint()
        
    @staticmethod
    def colourSoil(layer, _):
        """Layer colouring function for clay, rock, sand, silt and organic soil component rasters."""
        shader = QgsRasterShader()
        stats = layer.dataProvider().bandStatistics(1, QgsRasterBandStats.Min | QgsRasterBandStats.Max)
        minVal = stats.minimumValue
        maxVal = stats.maximumValue
        # make spectral colour ramp
        colour1 = QColor(215,25,28,255)
        colour2 = QColor(43,131,186,255)
        stop1 = QgsGradientStop(0.25, QColor(253,174,97,255))
        stop2 = QgsGradientStop(0.5, QColor(255,255,191,255))
        stop3 = QgsGradientStop(0.75, QColor(171,221,164,255))
        ramp = QgsGradientColorRamp(colour1, colour2, True, [stop1, stop2, stop3])
        fcn = QgsColorRampShader(minVal, maxVal)
        fcn.setClassificationMode(QgsColorRampShader.Quantile)
        fcn.setSourceColorRamp(ramp)
        fcn.setColorRampType("DISCRETE")
        fcn.classifyColorRamp(5, 1, layer.extent(), layer.dataProvider())
        # reset values in labels to 2 decimal places
        items = fcn.colorRampItemList()
        value0 = items[0].value
        value1 = items[1].value
        value2 = items[2].value
        value3 = items[3].value
        value4 = items[4].value
        item0 = QgsColorRampShader.ColorRampItem(value0, items[0].color, "<= {0:.2f}".format(value0))
        item1 = QgsColorRampShader.ColorRampItem(value1, items[1].color, "{0:.2f} - {1:.2f}".format(value0, value1))
        item2 = QgsColorRampShader.ColorRampItem(value2, items[2].color, "{0:.2f} - {1:.2f}".format(value1, value2))
        item3 = QgsColorRampShader.ColorRampItem(value3, items[3].color, "{0:.2f} - {1:.2f}".format(value2, value3))
        item4 = QgsColorRampShader.ColorRampItem(value4, items[4].color, "> {0:.2f}".format(value3))
        fcn.setColorRampItemList([item0, item1, item2, item3, item4])
        shader.setRasterShaderFunction(fcn)
        renderer = QgsSingleBandPseudoColorRenderer(layer.dataProvider(), 1, shader)
        layer.setRenderer(renderer)
        layer.triggerRepaint()
    
    @staticmethod
    def colourFacets(layer, numFacets) -> None:
        """Layer colouring function for facets raster."""
        items = []
        colours = QgsLimitedRandomColorRamp.randomColors(numFacets)
        for i in range(1, numFacets+1):
            item = QgsPalettedRasterRenderer.Class(i, colours[i-1], str(i))
            items.append(item)
        renderer = QgsPalettedRasterRenderer(layer.dataProvider(), 1, items)
        layer.setRenderer(renderer)
        layer.triggerRepaint()
    
    @staticmethod
    def randColor():
        """Return random QColor."""
        return QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    
