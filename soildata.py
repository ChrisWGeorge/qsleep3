# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                             -------------------
        begin                : 2016-05-21
        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# This version uses raw data for curvature and slopes rather than converting to int: see soildata_int for that

# Import the PyQt and QGIS libraries
from PyQt5.QtCore import * # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
from qgis.gui import * # @UnusedWildImport
from qgis.analysis import QgsRasterCalculator, QgsRasterCalculatorEntry  # @UnresolvedImport
import os
import math
import processing  # @UnresolvedImport
import sys
from osgeo import gdal, ogr
sys.path.append(os.path.dirname(__file__))
from sklearn import preprocessing
import numpy as np

from .soildatadialog import SoilDataDialog
from .QSLEEPUtils import QSLEEPUtils
from .TauDEMUtils import TauDEMUtils
from .raster import Raster
# from statsmodels.stats.multitest import fdrcorrection

class SoilData(QObject):
    """Create rasters to be used as predictors."""
    
    def __init__(self, gv):
        """Constructor."""
        QObject.__init__(self)
        self._dlg = SoilDataDialog()
        self._dlg.setWindowFlags(self._dlg.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self._gv = gv
        self._root = QgsProject.instance().layerTreeRoot()
        ## near infra-red band raster
        self.NIRFile = ''
        ## red band rastere
        self.RedFile = ''
        ## SAVI L factor selected
        self.LFactor = 0.5
        ## number of processes (for TauDEM)
        self.numProcesses = 0
        ## flag showing if form has been run
        self.hasRun = False
        ## initial chunk count for reading/writing rasters
        self.chunkCount = 1
        
    def run(self):
        """Run form."""
        self._dlg.layersTable.setHorizontalHeaderLabels(['Depth (mm)'])
        self._dlg.selectSoilDataButton.clicked.connect(self.selectSoilData)
        self._dlg.selectNIRButton.clicked.connect(self.selectNIR)
        self._dlg.selectRedButton.clicked.connect(self.selectRed)
        self._dlg.addLayerButton.clicked.connect(self.addLayer)
        self._dlg.deleteLayerButton.clicked.connect(self.deleteLayer)
        self._dlg.createButton.clicked.connect(self.save)
        self._dlg.buttonBox.accepted.connect(self.finish)
        self._dlg.buttonBox.rejected.connect(self.cancel)
        self.readProj()
        self.hasRun = False
        gdal.AllRegister()
        ogr.RegisterAll()
        self._dlg.show()
        return self._dlg.exec_()
        
    def selectSoilData(self):
        """Select soil points shapefile."""
        settings = QSettings()
        if settings.contains('/QSLEEP/LastInputPath'):
            path = str(settings.value('/QSLEEP/LastInputPath'))
        else:
            path = ''
        title = QSLEEPUtils.trans('Select soil data shapefile')
        filtr = QgsProviderRegistry.instance().fileVectorFilters()
        soilDataFile, _ = QFileDialog.getOpenFileName(None, title, path, filtr)
        if soilDataFile != '':
            direc = os.path.dirname(soilDataFile)
            settings.setValue('/QSLEEP/LastInputPath', str(direc))
            if os.path.samefile(direc, self._gv.soilDir):
                self._gv.soilDataFile = soilDataFile
            else:
                QSLEEPUtils.copyShapefile(soilDataFile, 'soildata', self._gv.soilDir)
                self._gv.soilDataFile = QSLEEPUtils.join(self._gv.soilDir, 'soildata.shp')
            self._dlg.selectSoilData.setText(self._gv.soilDataFile)
            soilLayer = QgsVectorLayer(self._gv.soilDataFile, 'Soil', 'ogr')
            if not soilLayer.isValid():
                QSLEEPUtils.error('Soil data file {0} cannot be loaded'.format(self._gv.soilDataFile))
                self._gv.soilDataFile = ''
                return
            crs = soilLayer.crs()
            if crs is None or crs.isGeographic():
                QSLEEPUtils.error('Soil data file {0} uses lat-long coordinates.  It must be projected'.format(self._gv.soilDataFile))
                self._gv.soilDataFile = ''
        
    def selectNIR(self):
        """Select near infra-red band raster."""
        settings = QSettings()
        if settings.contains('/QSLEEP/LastInputPath'):
            path = str(settings.value('/QSLEEP/LastInputPath'))
        else:
            path = ''
        title = QSLEEPUtils.trans('Select near infra red band raster file')
        filtr = QgsProviderRegistry.instance().fileRasterFilters()
        NIRFile, _ = QFileDialog.getOpenFileName(None, title, path, filtr)
        if NIRFile != '':
            settings.setValue('/QSLEEP/LastInputPath', os.path.dirname(str(NIRFile)))
            # copy to NIRDir if necessary
            inInfo = QFileInfo(NIRFile)
            inDir = inInfo.absoluteDir()
            outDir = QDir(self._gv.NIRDir)
            if inDir != outDir:
                inFile = inInfo.fileName()
                if inFile == 'sta.adf' or inFile == 'hdr.adf':
                    # ESRI grid - whole directory will be copied to sleepDir
                    inDirName = inInfo.dir().dirName()
                    outFileName = QSLEEPUtils.join(QSLEEPUtils.join(self._gv.NIRDir, inDirName), inFile)
                else:
                    outFileName = QSLEEPUtils.join(self._gv.NIRDir, inFile)
                QSLEEPUtils.copyFiles(inInfo, self._gv.NIRDir)
            else:
                outFileName = NIRFile
            self.NIRFile = outFileName
            self._dlg.selectNIR.setText(self.NIRFile)
            NIRLayer = QgsRasterLayer(self.NIRFile, 'NIR')
            if not NIRLayer.isValid():
                QSLEEPUtils.error('Near infra red raster {0} cannot be loaded'.format(self.NIRFile))
                self.NIRFile = ''
                return
            crs = NIRLayer.crs()
            if crs is None or crs.isGeographic():
                QSLEEPUtils.error('Near infra red raster {0} uses lat-long coordinates.  It must be projected'.format(self.NIRFile))
                self.NIRFile = ''
        
    def selectRed(self):
        """Select red band raster."""
        settings = QSettings()
        if settings.contains('/QSLEEP/LastInputPath'):
            path = str(settings.value('/QSLEEP/LastInputPath'))
        else:
            path = ''
        title = QSLEEPUtils.trans('Select red band raster file')
        filtr = QgsProviderRegistry.instance().fileRasterFilters()
        RedFile, _ = QFileDialog.getOpenFileName(None, title, path, filtr)
        if RedFile != '':
            settings.setValue('/QSLEEP/LastInputPath', os.path.dirname(str(RedFile)))
            # copy to RedDir if necessary
            inInfo = QFileInfo(RedFile)
            inDir = inInfo.absoluteDir()
            outDir = QDir(self._gv.RedDir)
            if inDir != outDir:
                inFile = inInfo.fileName()
                if inFile == 'sta.adf' or inFile == 'hdr.adf':
                    # ESRI grid - whole directory will be copied to sleepDir
                    inDirName = inInfo.dir().dirName()
                    outFileName = QSLEEPUtils.join(QSLEEPUtils.join(self._gv.RedDir, inDirName), inFile)
                else:
                    outFileName = QSLEEPUtils.join(self._gv.RedDir, inFile)
                QSLEEPUtils.copyFiles(inInfo, self._gv.RedDir)
            else:
                outFileName = RedFile
            self.RedFile = outFileName
            self._dlg.selectRed.setText(self.RedFile)
            RedLayer = QgsRasterLayer(self.RedFile, 'Red')
            if not RedLayer.isValid():
                QSLEEPUtils.error('Red raster {0} cannot be loaded'.format(self.RedFile))
                self.RedFile = ''
                return
            crs = RedLayer.crs()
            if crs is None or crs.isGeographic():
                QSLEEPUtils.error('Red raster {0} uses lat-long coordinates.  It must be projected'.format(self.RedFile))
                self.RedFile = ''
        
    def save(self):
        """Save files."""
        if self._dlg.selectSoilData.text() == '':
            QSLEEPUtils.error('Please select a soil data shapefile', self._gv.isBatch)
            return
#         if self._dlg.numClusters.value() <= 1:
#             QSLEEPUtils.error(u'There must be at least 2 facet classes', self._gv.isBatch)
#             return
        if self._dlg.selectNIR.text() == '' and self._dlg.selectRed.text() != '':
            QSLEEPUtils.error('There is no point selecting a Red band raster without an NIR band raster', self._gv.isBatch)
            return 
        elif self._dlg.selectNIR.text() != '' and self._dlg.selectRed.text() == '':
            QSLEEPUtils.error('There is no point selecting an NIR band raster without a Red band raster', self._gv.isBatch)
            return
        numLayers = self._dlg.layersTable.rowCount()
        if numLayers == 0:
            QSLEEPUtils.error('You need at least one soil layer to be defined', self._gv.isBatch)
            return
        if numLayers > 10:
            QSLEEPUtils.error('At most ten soil layers may be defined', self._gv.isBatch)
            return
        if not self.writeLayers():
            return
        self._gv.facetClassCount = self._dlg.numClusters.value()
        self.makeSleepRasters()
    
    def makeSleepRasters(self):
        """Make rasters from DEM needed by SLEEP."""
        root = self._root
        fil, filLayer = self._gv.rasters['fil']
        
#         # subtract streams from fil
#         self.progress(u'Subtracting streams')
#         filnofl = QSLEEPUtils.join(self._gv.sleepDir, 'filnofl.tif')
#         strFile = self._gv.rasters['str'][0]
#         if not self.runCalc2Trans(filFile, strFile, SoilData.subStream, filnofl, filFile, root):
#             QSLEEPUtils.error(u'Failed to create filnofl', self._gv.isBatch)
#             return
#         filnofl_layer = QgsRasterLayer(filnofl, 'filnofl')
#         self._gv.rasters['filnofl'] = filnofl, filnofl_layer

        # run SAGA slope aspect curvature
        sloperadraw = QSLEEPUtils.join(self._gv.sleepDir, 'sloperadraw.tif')
        aspectradraw0 = QSLEEPUtils.join(self._gv.sleepDir, 'aspectraw0.tif')
        curvraw = QSLEEPUtils.join(self._gv.sleepDir, 'curvraw.tif')
        profcraw = QSLEEPUtils.join(self._gv.sleepDir, 'profcraw.tif')
        plcraw = QSLEEPUtils.join(self._gv.sleepDir, 'plcraw.tif')
        if not QSLEEPUtils.isUpToDate(self._gv.demFile, fil) or \
            not QSLEEPUtils.isUpToDate(fil, sloperadraw) or \
            not QSLEEPUtils.isUpToDate(fil, aspectradraw0) or \
            not QSLEEPUtils.isUpToDate(fil, curvraw) or \
            not QSLEEPUtils.isUpToDate(fil, profcraw) or \
            not QSLEEPUtils.isUpToDate(fil, plcraw):
            QSLEEPUtils.removeLayerAndFiles(aspectradraw0, root)
            QSLEEPUtils.removeLayerAndFiles(sloperadraw, root)
            QSLEEPUtils.removeLayerAndFiles(curvraw, root)
            QSLEEPUtils.removeLayerAndFiles(profcraw, root)
            QSLEEPUtils.removeLayerAndFiles(plcraw, root)
            self.progress('Aspect and curvature ...')
            self._dlg.setCursor(Qt.WaitCursor)
            # in QGIS 2.6 SAGA produced .tif files; now .sdat
            if self._gv.qgisIs2_6:
                method = 5  # 2 degree polynomialZevenburgen & Thorne 1987
                command = "'saga:slopeaspectcurvature', {0}, {1}, {2}, {3}, {4}. {5}, {6}". \
                format(fil, method, sloperadraw, aspectradraw0, curvraw, plcraw, profcraw)
                result = processing.runalg('saga:slopeaspectcurvature',
                                           fil, method, sloperadraw, aspectradraw0, 
                                           curvraw, plcraw, profcraw)
                QSLEEPUtils.loginfo(command)
                if not result:
                    QSLEEPUtils.error('Processing command failed: {0}'.format(command), self._gv.isBatch)
                    return
                if not 'SLOPE' in result:
                    QSLEEPUtils.error('Processing command produced dictionary with keys: {0}'.format(list(result.keys())), self._gv.isBatch)
            else:    
                params = {'ELEVATION': fil,
                           'METHOD': 6,    # 2 degree polynomial Zevenburgen & Thorne 1987
                           'UNIT_SLOPE': 0,  # use radians 
                           'UNIT_ASPECT': 0,  # use radians 
                           'SLOPE': QSLEEPUtils.tempFile('.sdat'),
                           'ASPECT': QSLEEPUtils.tempFile('.sdat'),
                           'C_GENE': QSLEEPUtils.tempFile('.sdat'),
                           'C_PLAN': QSLEEPUtils.tempFile('.sdat'),
                           'C_PROF': QSLEEPUtils.tempFile('.sdat'),
                           'C_TANG': 'TEMPORARY_OUTPUT',
                           'C_LONG': 'TEMPORARY_OUTPUT',
                           'C_CROS': 'TEMPORARY_OUTPUT',
                           'C_MAXI': 'TEMPORARY_OUTPUT',
                           'C_MINI': 'TEMPORARY_OUTPUT',
                           'C_ROTO': 'TEMPORARY_OUTPUT',
                           'C_TOTA': 'TEMPORARY_OUTPUT'
                           }
                result = processing.run('saga:slopeaspectcurvature', params)
                if not result:
                    QSLEEPUtils.error('slopeaspectcurvature failed: {0}', self._gv.isBatch)
                    return
                if not 'SLOPE' in result:
                    QSLEEPUtils.error('Processing command produced dictionary with keys: {0}'.format(list(result.keys())), self._gv.isBatch)
                # convert .sdat files to .tif 
                command = 'gdal_translate -of GTiff "{0}" "{1}"'
                os.system(command.format(result['SLOPE'], sloperadraw))
                os.system(command.format(result['ASPECT'], aspectradraw0))
                os.system(command.format(result['C_GENE'], curvraw))
                os.system(command.format(result['C_PROF'], profcraw))
                os.system(command.format(result['C_PLAN'], plcraw))
            assert os.path.exists(sloperadraw), 'slope file {0} not created'.format(sloperadraw)
            assert os.path.exists(aspectradraw0), 'aspect file {0} not created'.format(aspectradraw0)
            assert os.path.exists(curvraw), 'curvature file {0} not created'.format(curvraw)
            assert os.path.exists(profcraw), 'profile file {0} not created'.format(profcraw)
            assert os.path.exists(plcraw), 'plan file {0} not created'.format(plcraw)
        # aspect will bo noData in areas of zero curvature; change to zero
        aspectradraw = QSLEEPUtils.join(self._gv.sleepDir, 'aspectraw.tif')
        if not QSLEEPUtils.isUpToDate(aspectradraw0, aspectradraw):
            self.noDataToVal(aspectradraw0, aspectradraw, 0, root, False)
            self._dlg.setCursor(Qt.ArrowCursor)
        
        if self._gv.facetClassCount > 1:
            # create shapefile from facets
            self.progress('Facets shapefile ...')
            self._dlg.setCursor(Qt.WaitCursor)
            # TODO facets1.shp does not disappear when deletion is attempted, and then if the 
            # form is rerun there is a crash.  Need to use a temporary file.
            facetShape1 = QSLEEPUtils.tempFile('_1.shp')
            QSLEEPUtils.rasterToPolygonShapefile(self._gv.facets, facetShape1, 'FacetId', filLayer.crs(), root, self._gv.isBatch)
            QSLEEPUtils.loginfo('Facets 1 is {0}'.format(facetShape1))
            # try fixing geometry first to make dissolve more likely to work
            facetShape2 = QSLEEPUtils.tempFile('_2.shp')
            params = {'INPUT': facetShape1, 'OUTPUT': facetShape2}
            processing.run('native:fixgeometries', params)
            QSLEEPUtils.copyPrj(facetShape1, facetShape2)
            facetShape3 = QSLEEPUtils.tempFile('_3.shp')
            # dissolve to ensure only one shape per facet
            params = {'INPUT': facetShape2,'FIELD': ['FacetId'],'OUTPUT': facetShape3}
            processing.run('native:dissolve', params)
            QSLEEPUtils.copyPrj(facetShape2, facetShape3)
            # add facets area etc to facets shapefile
            # processing.runalg("qgis:zonalstatistics","facets.tif",1,"facets1.shp","_stat",True,"facets2.shp")
            command = "'qgis:zonalstatistics', {0}, 1, {1}, '-f' , True, {2}". \
            format(self._gv.facets, facetShape3, facetShape2)
            params =  {'INPUT_RASTER': self._gv.facets,
                       'RASTER_BAND': 1,
                       'INPUT_VECTOR': facetShape3,
                       'COLUMN_PREFIX': '_f',
                       'STATS':[0]}
            result = processing.run('qgis:zonalstatistics', params)
            if not result:
                QSLEEPUtils.error('Processing command failed: {0}'.format(command), self._gv.isBatch)
                return
            # add slope statistics to facets shapefile
            params =  {'INPUT_RASTER': sloperadraw,
                       'RASTER_BAND': 1,
                       'INPUT_VECTOR': facetShape3,
                       'COLUMN_PREFIX': '_slp',
                       'STATS': [2,5,6]}
            processing.run('qgis:zonalstatistics', params)
            # add normalised count for facets to facet shapefile
            facetLayer = QgsVectorLayer(facetShape3, 'Facets', 'ogr')
            facetCountIndex = facetLayer.fields().lookupField('_fcount')
            # QSLEEPUtils.loginfo(u'_fcount index is {0}'.format(facetCountIndex))
            minCount = sys.maxsize
            maxCount = -1
            for f in facetLayer.getFeatures():
                facetCount = int(f.attributes()[facetCountIndex])
                if facetCount < minCount:
                    minCount = facetCount
                if facetCount > maxCount:
                    maxCount = facetCount
            #QSLEEPUtils.loginfo(u'min and max counts are {0} and {1}'.format(minCount, maxCount))
            facetNormField = QgsField('facetnorm', QVariant.Double)
            slopeNormField = QgsField('slopenorm', QVariant.Double)
            facetLayer.dataProvider().addAttributes([facetNormField, slopeNormField])
            facetNormIndex = facetLayer.dataProvider().fields().lookupField('facetnorm')
            if facetNormIndex < 0:
                QSLEEPUtils.error('Failed to add facetnorm field to facets shapefile', self._gv.isBatch)
                return
            #QSLEEPUtils.loginfo(u'facetnorm index is {0}'.format(facetNormIndex))
            slopeNormIndex = facetLayer.dataProvider().fields().lookupField('slopenorm')
            slopeMinIndex = facetLayer.fields().lookupField('_slpmin')
            slopeMaxIndex = facetLayer.fields().lookupField('_slpmax')
            slopeIndex = facetLayer.fields().lookupField('_slpmean')
            #QSLEEPUtils.loginfo(u'_slpmean index is {0}'.format(slopeIndex))
            facetCountRange = maxCount - minCount
            facetLayer.startEditing()
            for f in facetLayer.getFeatures():
                attrs = f.attributes()
                fid = f.id()
                facetCount = int(attrs[facetCountIndex])
                facetNorm = (facetCount - minCount) / facetCountRange
                OK = facetLayer.changeAttributeValue(fid, facetNormIndex, facetNorm)
                if not OK:
                    QSLEEPUtils.error('Failed to change facetnorm field {0} to {1}'.format(facetNormIndex, facetNorm), self._gv.isBatch)
                    return
                slopeMin = float(attrs[slopeMinIndex])
                slopeMax = float(attrs[slopeMaxIndex])
                slope = float(attrs[slopeIndex])
                slopeNorm = 0.5 if slopeMax == slopeMin else (slope - slopeMin) / (slopeMax - slopeMin)
                OK = facetLayer.changeAttributeValue(fid, slopeNormIndex, slopeNorm)
                if not OK:
                    QSLEEPUtils.error('Failed to change slopenorm field {0} to {1}'.format(slopeNormIndex, slopeNorm), self._gv.isBatch)
                    return
            facetLayer.commitChanges()
            facetArea = QSLEEPUtils.tempFile('_a.tif')
            facet3Layer = os.path.splitext(os.path.split(facetShape3)[1])[0]
            command = 'gdal_rasterize -a {0} -tr {1!s} {2!s} -a_nodata -9999 -l {3} "{4}" "{5}"'.format('facetnorm', self._gv.sizeX, self._gv.sizeY, facet3Layer, facetShape3, facetArea)
            QSLEEPUtils.loginfo(command)
            os.system(command)
            assert os.path.exists(facetArea)
            QSLEEPUtils.copyPrj(self._gv.demFile, facetArea)
            facetSlope = QSLEEPUtils.tempFile('_s.tif')
            command = 'gdal_rasterize -a {0} -tr {1!s} {2!s} -a_nodata -9999 -l {3} "{4}" "{5}"'.format('slopenorm', self._gv.sizeX, self._gv.sizeY, facet3Layer, facetShape3, facetSlope)
            os.system(command)
            assert os.path.exists(facetSlope)
            QSLEEPUtils.copyPrj(self._gv.demFile, facetSlope)
            # do cluster analysis
            clusterMethod = 0
            normalise = True
            oldVersion = False
            if not self._gv.qgisIs2_6:
                maxIter = 0  # default
                updateView = False
                resampling = 0  # nearest neighbour
            facetClass = QSLEEPUtils.join(self._gv.sleepDir, 'facetclass.tif')
            QSLEEPUtils.removeLayerAndFiles(facetClass, root)
            facetClassStats = QSLEEPUtils.join(self._gv.sleepDir, 'facetclassStats.txt')
            QSLEEPUtils.removeLayerAndFiles(facetClassStats, root)
            if self._gv.qgisIs2_6:
                #processing.runalg("saga:clusteranalysisforgrids","facetSlope.tif;facetArea.tif",0,6,True,True,"clusters.tif","clusterstats.csv")
                command = "'saga:clusteranalysisforgrids', '{0};{1}', {2}, {3}, {4}, {5}, {6}, {7}". \
                format(facetArea, facetSlope, clusterMethod, self._gv.facetClassCount, normalise, oldVersion, facetClass, facetClassStats)
                result = processing.runalg('saga:clusteranalysisforgrids', '{0};{1}'.format(facetArea, facetSlope), 
                                           clusterMethod, self._gv.facetClassCount, normalise, oldVersion, facetClass, facetClassStats)
            else:
                params = {'GRIDS': [facetArea, facetSlope],
                          'METHOD': clusterMethod,
                          'NCLUSTER': self._gv.facetClassCount,
                          'MAXITER': 0,
                          'NORMALISE': normalise,
                          'OLDVERSION': False,
                          'UPDATEVIEW':False,
                          'CLUSTER': QSLEEPUtils.tempFile('.sdat'),
                          'STATISTICS': facetClassStats}
                result = processing.run('saga:kmeansclusteringforgrids', params)
            if not result:
                QSLEEPUtils.error('Processing command failed: {0}'.format(command), self._gv.isBatch)
                return
            if not 'CLUSTER' in result:
                QSLEEPUtils.error('Processing cluster command produced dictionary with keys: {0}'.format(list(result.keys())), self._gv.isBatch)
            # convert .sdat file to .tif 
            command = 'gdal_translate -of GTiff "{0}" "{1}"'
            os.system(command.format(result['CLUSTER'], facetClass))
            QSLEEPUtils.copyPrj(facetArea, facetClass)
            
    # tried isodata clustering in QGIS 2.18 but not successful - very few classes (2 for flood version) , one with lots of zeroes in stats
    # also parameter control file 
    # C:\Program Files\QGIS 2.18\apps\qgis-ltr\python\plugins\processing\algs\saga\description\ISODATAClusteringforGrids.txt 
    # has two errors:
    # imagery_classification -> imagery_isocluster
    # ParameterTable|STATISTICS|Statistics|False -> OutputTable|STATISTICS|Statistics|False 
    # to run this code you need to add result directory to SLEEP directory     
    #         facetClass2 = QSLEEPUtils.join(self._gv.sleepDir, 'result/facetclass.tif')
    #         QSLEEPUtils.removeLayerAndFiles(facetClass2, root)
    #         facetClassStats2 = QSLEEPUtils.join(self._gv.sleepDir, 'result/facetclassStats.csv')
    #         QSLEEPUtils.removeLayerAndFiles(facetClassStats2, root)
    #         command = "'saga:isodataclusteringforgrids', '{0};{1}', False, 20, 5, 16, 5, 3, {2}, {3}". \
    #         format(facetArea, facetSlope, facetClass2, facetClassStats2)
    #         result2 = processing.runalg('saga:isodataclusteringforgrids', '{0};{1}'.format(facetArea, facetSlope), 
    #                                     False, 20, 5, 16, 5, 3, facetClass2, facetClassStats2)
    #         if not result2:
    #             QSLEEPUtils.error(u'Processing command failed: {0}'.format(command), self._gv.isBatch)
    #             return
    #         QSLEEPUtils.loginfo('isodata clustering: {0}'.format(repr(result2)))
    #         if not 'CLUSTER' in result2:
    #             QSLEEPUtils.error(u'Processing cluster command produced dictionary with keys: {0}'.format(result.keys()), self._gv.isBatch)
    #         QSLEEPUtils.copyPrj(facetArea, facetClass2)
            
            facetClassLayer = QgsRasterLayer(facetClass, QSLEEPUtils._FACETCLASS)
            self._gv.rasters[QSLEEPUtils._FACETCLASS] = facetClass, facetClassLayer
            self._dlg.setCursor(Qt.ArrowCursor)
        else: # only one facetclass used
            self._gv.rasters[QSLEEPUtils._FACETCLASS] = None, None
        soilDataLayer = QgsVectorLayer(self._gv.soilDataFile, 'Soil data', 'ogr')
        provider = soilDataLayer.dataProvider()
        QSLEEPUtils.loginfo('Initial count {0}'.format(provider.featureCount()))
        if self._gv.facetClassCount == 1:
            self.addConstantAttribute(provider, QSLEEPUtils._FACETCLASS, QVariant.Int, 1)
        else:
            if not self.addRawAttributeFromRaster(provider, QSLEEPUtils._FACETCLASS, QVariant.Int):
                return
            # get minimum count of soil points in each facet class
            counts = dict()
            for i in range(self._gv.facetClassCount):
                if self._gv.qgisIs2_6:
                    counts[i] = 0
                else:
                    # kmeans labels from 1
                    counts[i+1] = 0
            FCIndex = provider.fields().lookupField(QSLEEPUtils._FACETCLASS)
            for f in provider.getFeatures():
                facetClass = f.attributes()[FCIndex]
                counts[facetClass] += 1
            QSLEEPUtils.loginfo('Facet class numbers: {0!s}'.format(counts))
            minCount = sys.maxsize
            minClass = -1
            emptyClasses = 0
            for fc, val in counts.items():
                if 0 < val < minCount:
                    minCount = val
                    minClass = fc
                elif val == 0:
                    emptyClasses += 1
            msg1 = '' if emptyClasses == 0 else 'There is 1 empty facet class.\n'  if emptyClasses == 1 else 'There are {0} empty facet classes.\n'.format(emptyClasses)
            msg2 = '' if minCount >= 10 else 'Facet class {0} has only {1} items.\n'.format(minClass, minCount)
            if msg1 != '' or msg2 != '':
                res = QSLEEPUtils.question(msg1 + msg2 + 'Do you want to change the intended number of facet classes?',
                                            self._dlg, False)
                if res == QMessageBox.Yes:
                    return
        
#         fdrCat = self.clipToBasin(self._gv.rasters['fdr'][0], root)
#         if not fdrCat:
#             return
#         fdrCatU = self.uncompressTiff(fdrCat)
#         if not fdrCatU:
#             return
#         # need to use TauDEM flow accumulation map as may be offset from fac produced by GRASS
#         self.progress(u'Flow accumulation ...')
# #         tauFacCat = QSLEEPUtils.join(self._gv.sleepDir, 'tauFacCat.tif')
#         if self._gv.outletFile == '':
#             outlet = None
#         else:
#             outlet = self._gv.outletFile
#         ok = TauDEMUtils.runAreaD8(fdrCatU, tauFacCat, outlet, None, self.numProcesses, None)
#         if not ok:
#             return
#         QSLEEPUtils.copyPrj(fdrCatU, tauFacCat)
        
#         # calculate D8 contributing areas
#         tauD8 = QSLEEPUtils.join(self._gv.sleepDir, 'tauD8.tif')
#         tauPD8 = QSLEEPUtils.join(self._gv.sleepDir, 'tauPD8.tif')
#         tauAD8 = QSLEEPUtils.join(self._gv.sleepDir, 'tauAD8.tif')
#         fil = self._gv.rasters['fil'][0]
#         ok = TauDEMUtils.runD8FlowDir(fil, tauPD8, tauD8, self.numProcesses, None)
#         if not ok:
#             QSLEEPUtils.error(u'Failed to run D8FlowDir', self._gv.isBatch)
#             return
#         ok = TauDEMUtils.runAreaD8(tauD8, tauAD8, outlet, None, self.numProcesses, None)
#         if not ok:
#             QSLEEPUtils.error(u'Failed to run AreaD8', self._gv.isBatch)
#             return
#         QSLEEPUtils.copyPrj(fil, tauD8)
#         QSLEEPUtils.copyPrj(fil, tauPD8)
#         QSLEEPUtils.copyPrj(fil, tauAD8)

        # don't subtract streams
        fdr = self._gv.rasters['fdr'][0]
        fac = self._gv.rasters['fac'][0]
        
#         # subtract streams
#         tauD8nofl = QSLEEPUtils.join(self._gv.sleepDir, 'tauD8nofl.tif')
#         tauAD8nofl = QSLEEPUtils.join(self._gv.sleepDir, 'tauAD8nofl.tif')
#         if not self.runCalc2Trans(tauD8, strFile, SoilData.subStream, tauD8nofl, filFile, root):
#             QSLEEPUtils.error(u'Failed to create tauD8nofl', self._gv.isBatch)
#             return
#         if not self.runCalc2Trans(tauAD8, strFile, SoilData.subStream, tauAD8nofl, filFile, root):
#             QSLEEPUtils.error(u'Failed to create tauAD8nofl', self._gv.isBatch)
#             return
#         tauD8nofl_layer = QgsRasterLayer(tauD8nofl, 'D8nofl')
#         self._gv.rasters['tauD8nofl'] = tauD8nofl, tauD8nofl_layer
#         tauAD8nofl_layer = QgsRasterLayer(tauAD8nofl, 'AD8nofl')
#         self._gv.rasters['tauAD8nofl'] = tauAD8nofl, tauAD8nofl_layer
        

#         # calculate Dinf contributing areas
#         tauAng = QSLEEPUtils.join(self._gv.sleepDir, 'tauAng.tif')
#         tauSlp = QSLEEPUtils.join(self._gv.sleepDir, 'tauSlp.tif')
#         tauSca = QSLEEPUtils.join(self._gv.sleepDir, 'tauSca.tif')
#         fil = self._gv.rasters['fil'][0]
#         ok = TauDEMUtils.runDinfFlowDir(fil, tauSlp, tauAng, self.numProcesses, None)
#         if not ok:
#             self.cleanUp(3)
#             return
#         ok = TauDEMUtils.runAreaDinf(tauAng, tauSca, outlet, None, self.numProcesses, None)
#         if not ok:
#             self.cleanUp(3)
#             return
#         QSLEEPUtils.copyPrj(fil, tauAng)
#         QSLEEPUtils.copyPrj(fil, tauSlp)
#         QSLEEPUtils.copyPrj(fil, tauSca)
        # categorise average aspect angle with values 1 to 8
        radiansToDegrees = lambda x: x * 180 / math.pi
        self.progress('Average aspect ... ')
        aatAspect = self.averageByFlowFunc(fdr, fac, aspectradraw, 'aataspect.tif', SoilData.mean, root, isInt=False, fun2=radiansToDegrees)
        if not aatAspect:
            QSLEEPUtils.error('Failed to create average aspect', self._gv.isBatch)
            self._dlg.setCursor(Qt.ArrowCursor)
            return
        aataspectLayer = QgsRasterLayer(aatAspect, 'aataspect')
        self._gv.rasters['aataspect'] = aatAspect, aataspectLayer
        self.addAttributeFromRaster(provider, 'aataspect', QVariant.Double, length=6, precision=3)
        self.progress('Average degree slope ...')
        aatDegSlp = self.averageByFlowFunc(fdr, fac, sloperadraw, 'aatdegslp.tif', SoilData.mean, root, isInt=False, fun2=radiansToDegrees)
        if not aatDegSlp:
            QSLEEPUtils.error('Failed to create average degree slope', self._gv.isBatch)
            self._dlg.setCursor(Qt.ArrowCursor)
            return
        aatDegSlpLayer = QgsRasterLayer(aatDegSlp, 'aatdegslp')
        self._gv.rasters['aatdegslp'] = aatDegSlp, aatDegSlpLayer
        self.addAttributeFromRaster(provider, 'aatdegslp', QVariant.Double, length=6, precision=3)
        self.progress('Average percent slope ...')
        aatPctSlp = self.averageByFlowFunc(fdr, fac, sloperadraw, 'aatpctslp.tif', SoilData.meanTanPercent, root, isInt=False)
        if not aatPctSlp:
            QSLEEPUtils.error('Failed to create average percent slope', self._gv.isBatch)
            self._dlg.setCursor(Qt.ArrowCursor)
            return
        aatPctSlpLayer = QgsRasterLayer(aatPctSlp, 'aatpctslp')
        self._gv.rasters['aatpctslp'] = aatPctSlp, aatPctSlpLayer
        self.addAttributeFromRaster(provider, 'aatpctslp', QVariant.Double, length=6, precision=3)
        self.progress('Average curvature ...')
        aatCurv = self.averageByFlowFunc(fdr, fac, curvraw, 'aatcurv.tif', SoilData.mean, root, isInt=False)
        if not aatCurv:
            QSLEEPUtils.error('Failed to create average curvature', self._gv.isBatch)
            self._dlg.setCursor(Qt.ArrowCursor)
            return
        aatCurvLayer = QgsRasterLayer(aatCurv, 'aatcurv')
        self._gv.rasters['aatcurv'] = aatCurv, aatCurvLayer
        self.addAttributeFromRaster(provider, 'aatcurv', QVariant.Double, length=6, precision=3)
        self.progress('Average plan curvature ...')
        aatPlan = self.averageByFlowFunc(fdr, fac, plcraw, 'aatplc.tif', SoilData.mean, root, isInt=False)
        if not aatPlan:
            QSLEEPUtils.error('Failed to create average plan curvature', self._gv.isBatch)
            self._dlg.setCursor(Qt.ArrowCursor)
            return
        aatPlanLayer = QgsRasterLayer(aatPlan, 'aatplc')
        self._gv.rasters['aatplc'] = aatPlan, aatPlanLayer
        self.addAttributeFromRaster(provider, 'aatplc', QVariant.Double, length=6, precision=3)
        self.progress('Average profile curvature ...')
        aatProfile = self.averageByFlowFunc(fdr, fac, profcraw, 'aatprofc.tif', SoilData.mean, root, isInt=False)
        if not aatProfile:
            QSLEEPUtils.error('Failed to create average profile curvature', self._gv.isBatch)
            self._dlg.setCursor(Qt.ArrowCursor)
            return
        aatProfileLayer = QgsRasterLayer(aatProfile, 'aatprofc')
        self._gv.rasters['aatprofc'] = aatProfile, aatProfileLayer
        self.addAttributeFromRaster(provider, 'aatprofc', QVariant.Double, length=6, precision=3)
#         self.progress(u'Curvature reclassified ...')
#         curv = QSLEEPUtils.join(self._gv.sleepDir, 'curv.tif')
#         self.runCalc1(curvraw, SoilData.reclassCurvature, curv, root, isInt=False)
        curvLayer = QgsRasterLayer(curvraw, 'curv')
        self._gv.rasters['curv'] = curvraw, curvLayer
        self.addAttributeFromRaster(provider, 'curv', QVariant.Double, length=6, precision=3)
#         self.progress(u'Plan curvature reclassified ...')
#         plc = QSLEEPUtils.join(self._gv.sleepDir, 'plc.tif')
#         self.runCalc1(plcraw, SoilData.reclassCurvature, plc, root, isInt=False)
        plcLayer = QgsRasterLayer(plcraw, 'plc')
        self._gv.rasters['plc'] = plcraw, plcLayer
        self.addAttributeFromRaster(provider, 'plc', QVariant.Double, length=6, precision=3)
#         self.progress(u'Profile curvature reclassified ...')
#         profc = QSLEEPUtils.join(self._gv.sleepDir, 'profc.tif')
# #         self.runCalc(formula, [profcraw], profc, root, isInt=True)
#         self.runCalc1(profcraw, SoilData.reclassCurvature, profc, root, isInt=False)
        profcLayer = QgsRasterLayer(profcraw, 'profc')
        self._gv.rasters['profc'] = profcraw, profcLayer
        self.addAttributeFromRaster(provider, 'profc', QVariant.Double, length=6, precision=3)
#         self.progress(u'Aspect reclassified ...')
#         aspect = QSLEEPUtils.join(self._gv.sleepDir, 'aspect.tif')
#         self.runCalc1(aspectradraw, SoilData.degToD8, aspect, root, isInt=True, fun1=radiansToDegrees)
        aspectLayer = QgsRasterLayer(aspectradraw, 'aspect')
        self._gv.rasters['aspect'] = aspectradraw, aspectLayer
        self.addAttributeFromRaster(provider, 'aspect', QVariant.Double, length=6, precision=3)
        self.progress('Slope to degrees ...')
        degslp = QSLEEPUtils.join(self._gv.sleepDir, 'degslp.tif')
        self.runCalc1(sloperadraw, SoilData.identity, degslp, root, isInt=False, fun1=radiansToDegrees)
        degslpLayer = QgsRasterLayer(degslp, 'degslp')
        self._gv.rasters['degslp'] = degslp, degslpLayer
        self.addAttributeFromRaster(provider, 'degslp', QVariant.Double, length=6, precision=3)
        self.progress('Percent slope ...')
        pctslp = QSLEEPUtils.join(self._gv.sleepDir, 'pctslp.tif')
        self.runCalc1(sloperadraw, SoilData.tanPercent, pctslp, root, isInt=False)
        pctslpLayer = QgsRasterLayer(pctslp, 'pctslp')
        self._gv.rasters['pctslp'] = pctslp, pctslpLayer
        self.addAttributeFromRaster(provider, 'pctslp', QVariant.Double, length=6, precision=3)
        # calculate CTI
        self.progress('CTI raster ...')
        cti = QSLEEPUtils.join(self._gv.sleepDir, 'cti.tif')
        QSLEEPUtils.removeLayerAndFiles(cti, root)

        self.runCalc2(fac, sloperadraw, SoilData.ctiFun, cti, root, fun1=lambda x: x * self._gv.cellArea)
        assert os.path.exists(cti), 'failed to create cti raster {0}'.format(cti)
        ctiLayer = QgsRasterLayer(cti, 'cti')
        self._gv.rasters['cti'] = cti, ctiLayer
        self.addAttributeFromRaster(provider, 'cti', QVariant.Double, length=6, precision=3)
        # generate SAGA wetness index for comparison
#         ctiSAGA = QSLEEPUtils.join(self._gv.sleepDir, 'ctiSAGA.tif')
#         QSLEEPUtils.removeLayerAndFiles(ctiSAGA, root)
#         processing.runalg("saga:sagawetnessindex",fil,self._gv.sizeX,None,None,None,ctiSAGA)
        # calculate NDVI
        if self.NIRFile != '':
            self.progress('NDVI raster ...')
            ndvi = QSLEEPUtils.join(self._gv.sleepDir, 'ndvi.tif')
            QSLEEPUtils.removeLayerAndFiles(ndvi, root)
            if self.runCalc2Trans(self.RedFile, self.NIRFile, SoilData.ndviFun, ndvi, fil, root):
                ndviLayer = QgsRasterLayer(ndvi, 'ndvi')
                self._gv.rasters['ndvi'] = ndvi, ndviLayer
                self.addAttributeFromRaster(provider, 'ndvi', QVariant.Double, length=6, precision=3)
                # calculate SAVI
                self.progress('SAVI raster ...')
                savi = QSLEEPUtils.join(self._gv.sleepDir, 'savi.tif')
                QSLEEPUtils.removeLayerAndFiles(savi, root)
                if self.runCalc2Trans(self.RedFile, self.NIRFile, self.saviFun, savi, fil, root):
                    saviLayer = QgsRasterLayer(savi, 'savi')
                    self._gv.rasters['savi'] = savi, saviLayer
                    self.addAttributeFromRaster(provider, 'savi', QVariant.Double, length=6, precision=3)
        # add values to soilfacetjoin
        self.addAttributeFromRaster(provider, 'fdr', QVariant.Double, length=6, precision=3)
        self.addAttributeFromRaster(provider, 'fac', QVariant.Double, length=6, precision=3)
        self.addAttributeFromRaster(provider, 'fil', QVariant.Double, length=6, precision=3)
        # bring layer up to date
        soilDataLayer = QgsVectorLayer(self._gv.soilDataFile, 'Soil data', 'ogr')
        soilDataCSV = QSLEEPUtils.join(self._gv.sleepDir, 'soildata.csv')
        if os.path.exists(soilDataCSV):
            os.remove(soilDataCSV)
        # deprecated
        #error = QgsVectorFileWriter.writeAsVectorFormat(soilDataLayer, soilDataCSV, "UTF-8", soilDataLayer.crs(), driverName="CSV")
        ## options for creating csv
        csvOptions = QgsVectorFileWriter.SaveVectorOptions()
        csvOptions.ActionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
        csvOptions.driverName = "CSV"
        csvOptions.fileEncoding = "UTF-8"
        error = QgsVectorFileWriter.writeAsVectorFormatV2(soilDataLayer, soilDataCSV, QgsCoordinateTransformContext(), csvOptions)
        if error[0] != QgsVectorFileWriter.NoError:
            QSLEEPUtils.error('Failed to write soil data csv file: error {0}'.format(error), self._gv.isBatch)
        self._gv.soilDataCSV = soilDataCSV
        self.progress('Rasters done')   
        self.saveProj() 
        self.hasRun = True        
     
    def addAttributeFromRaster(self, provider, attribute, typ, length=0, precision=0):
        """Add standardisedvalues from raster named attribute (stored in self.rasters) for points in shapefile with provider."""
        
        # allow attribute fields to exist already (as may be a rerun)
        fieldIndex = provider.fields().lookupField(attribute)
        if fieldIndex >= 0:
            provider.deleteAttributes([fieldIndex])
            fieldIndex = -1
        if fieldIndex < 0:
            if typ == QVariant.Int:
                field = QgsField(attribute, QVariant.Int)
            else:
                field = QgsField(attribute, QVariant.Double, len=length, prec=precision)
            provider.addAttributes([field])
            fieldIndex = provider.fields().lookupField(attribute)
        rasterLayer = self._gv.rasters[attribute][1]
        rasterProvider = rasterLayer.dataProvider()
        nodata = rasterProvider.sourceNoDataValue(1)
        fileType = provider.wkbType()
        # collect non-nan values in map from f.id to value
        # and nan fids in toDelete
        toDelete = []
        vals = dict()
        for f in provider.getFeatures():
            geom = f.geometry()
            if fileType == QgsWkbTypes.Point:
                point = geom.asPoint()
            elif fileType == QgsWkbTypes.MultiPoint:
                point = geom.asMultiPoint()[0]
            else:
                QSLEEPUtils.error('Strange type {0} for soil data points'.format(fileType))
                return
            fid = f.id()
            val = rasterProvider.identify(point, QgsRaster.IdentifyFormatValue).results()[1]
            if val is None or val == nodata:
                if attribute == 'aspect' or attribute == 'aataspect':
                    # flat areas get aspect nan
                    vals[fid] = 0
                else:
                    toDelete.append(fid)
            else:
                vals[fid] = val
        # collect vals in numpy array, making corresponding list fids
        rawVals = np.empty((len(vals),))
        fids = []
        index = 0
        for fid,  val in list(vals.items()):
            rawVals[index] = val
            fids.append(fid)
            index += 1
        # standardize rawVals into standardVals
        standardVals = preprocessing.scale(rawVals)
        self._gv.means[attribute] = np.mean(rawVals)
        self._gv.stds[attribute] = np.std(rawVals)
#         for i in range(min(3, len(vals))):
#             QSLEEPUtils.loginfo('Attribute: {2}:  Raw: {0:.2f}  Calc {1:.2f}'.
#                                 format(float((rawVals[i] - self._gv.means[attribute])/self._gv.stds[attribute]), float(standardVals[i]), attribute))
        # update soil map attributes
        attValues = dict()
        for i in range(len(fids)):
            attValues[fids[i]] = {fieldIndex: float(standardVals[i])}
        if not provider.changeAttributeValues(attValues):
            QSLEEPUtils.error('Failed to write {0} values to {1}'.format(attribute, self._gv.soilDataFile), self._gv.isBatch)
        if len(toDelete) > 0:
            provider.deleteFeatures(toDelete)
            QSLEEPUtils.loginfo('After {0} feature count reduced to {1!s}'.format(attribute, provider.featureCount()))       
     
    def addRawAttributeFromRaster(self, provider, attribute, typ, length=0, precision=0):
        """Add values from raster named attribute (stored in self.rasters) for points in shapefile with provider."""
        
        # allow attribute fields to exist already (as may be a rerun)
        index = provider.fields().lookupField(attribute)
        if index >= 0:
            provider.deleteAttributes([index])
            index = -1
        if index < 0:
            if typ == QVariant.Int:
                field = QgsField(attribute, QVariant.Int)
            else:
                field = QgsField(attribute, QVariant.Double, len=length, prec=precision)
            provider.addAttributes([field])
            index = provider.fields().lookupField(attribute)
        rasterLayer = self._gv.rasters[attribute][1]
        rasterProvider = rasterLayer.dataProvider()
        nodata = rasterProvider.sourceNoDataValue(1)
        numPoints = provider.featureCount()
        pointsToDelete = []
        attValues = dict()
        fileType = provider.wkbType()
        for f in provider.getFeatures():
            geom = f.geometry()
            if fileType == QgsWkbTypes.Point:
                point = geom.asPoint()
            elif fileType == QgsWkbTypes.MultiPoint:
                point = geom.asMultiPoint()[0]
            else:
                QSLEEPUtils.error('Strange type {0} for soil data points'.format(fileType))
                return
            values = dict()
            val = rasterProvider.identify(point, QgsRaster.IdentifyFormatValue).results()[1]
            if val is None or val == nodata:
                pointsToDelete.append(f.id())
            values[index] = val
            attValues[f.id()] = values
        provider.changeAttributeValues(attValues)
        if len(pointsToDelete) > 0.5 * numPoints:
            res = QSLEEPUtils.question("""Over half the {0} soil points do not overlap with any facet.
Continuing will delete them.  Do you wish to continue?
""")
            if res != QMessageBox.Yes:
                return False
        provider.deleteFeatures(pointsToDelete)
        QSLEEPUtils.loginfo('Count {0} after adding {1}'.format(provider.featureCount(), attribute))
        return True
        
    def addConstantAttribute(self, provider, attribute, typ, const):
        """Add constant value for attribute for points in shapefile with provider."""
        
        # allow attribute fields to exist already (as may be a rerun)
        index = provider.fields().lookupField(attribute)
        if index >= 0:
            provider.deleteAttributes([index])
            index = -1
        if index < 0:
            if typ == QVariant.Int:
                field = QgsField(attribute, QVariant.Int)
            else:
                field = QgsField(attribute, QVariant.Double, len=6, prec=3)
            provider.addAttributes([field])
            index = provider.fields().lookupField(attribute)
        attValues = dict()
        for f in provider.getFeatures():
            values = dict()
            values[index] = const
            attValues[f.id()] = values
        provider.changeAttributeValues(attValues)
        
#     def clipToBasin(self, inFile, root):
#         """Clip inFile to catchment, and return result."""
#         if self._gv.cat == '':
#             # no mask to clip to - using clipped dem without outlet
#             return inFile
#         base, suffix = os.path.splitext(inFile)
#         outFile = base + '_cat' + suffix
#         QSLEEPUtils.removeLayerAndFiles(outFile, root)
#         formula = 'if(B, A)'
#         ok = self.runCalc(formula, [inFile, self._gv.cat], outFile, root)
#         if ok:
#             return outFile
#         else:
#             return None
#          
    def uncompressTiff(self, inFile):
        """TauDEM will not accept compressed tiffs, as produced by GRASS, so uncompress inFile and return result."""
          
        #This seems very clumsy, but haven't found another way.
        outFile = inFile + 'f'
        inDs = gdal.Open(inFile, gdal.GA_ReadOnly)
        driver = gdal.GetDriverByName('GTiff')
        outDs = driver.CreateCopy(outFile, inDs, 0)
        inDs = None
        if outDs is None:
            QSLEEPUtils.error('Failed to uncompress {0}'.format(inFile), self._gv.isBatch)
            return None
        outDs = None
        #not necessary - will be same file
        #QSLEEPUtils.copyPrj(inFile, outFile)
        return outFile
         
#     def averageByFlow(self, fdr, fac, inFile, outFileName, formula, root, isInt=False):
#         """Generate raster of inFile values averaged over flow accumulation file, and return result."""
#         accum = QSLEEPUtils.tempFile('_ac.tif')
#         QSLEEPUtils.removeLayerAndFiles(accum, root)
#         #store sums of weight inFile along fdr flow paths in accum
#         if self._gv.outletFile == '':
#             outlet = None
#         else:
#             outlet = self._gv.outletFile
#         ok = TauDEMUtils.runAreaD8(fdr, accum, outlet, inFile, self.numProcesses, None)
#         if not ok:
#             return None
#         aatFile = QSLEEPUtils.join(self._gv.sleepDir, outFileName)
#         # use fac as first file since it will have a .prj: accum is temporary
#         if not self.runCalc(formula, [fac, accum], aatFile, root, isInt=isInt):
#             return None
#         return aatFile
         
    def averageByFlowFunc(self, fdr, fac, inFile, outFileName, func, root, isInt=False, fun1=None, fun2=None):
        """Generate raster of inFile values averaged over flow accumulation file, and return result."""
        accum = QSLEEPUtils.tempFile('_ac.tif')
        QSLEEPUtils.removeLayerAndFiles(accum, root)
        #store sums of weight inFile along fdr flow paths in accum
        if self._gv.outletFile == '':
            outlet = None
        else:
            outlet = self._gv.outletFile
        ok = TauDEMUtils.runAreaD8(fdr, accum, outlet, inFile, self.numProcesses, None)
        if not ok:
            return None
        #QSLEEPUtils.loginfo('Weighted accumulation is {0}'.format(accum))
        aatFile = QSLEEPUtils.join(self._gv.sleepDir, outFileName)
        # use fac as first file since it will have a .prj: accum is temporary
        if not self.runCalc2(fac, accum, func, aatFile, root, isInt=isInt, fun1=fun1, fun2=fun2):
            return None
        return aatFile
    
#     def averageByDinfFlow(self, ang, sca, inFile, outFileName, formula, root, isInt=False):
#         """Generate raster of inFile values averaged over Dinf flow accumulation file, and return result."""
#         accum = QSLEEPUtils.tempFile('_ac.tif')
#         QSLEEPUtils.removeLayerAndFiles(accum, root)
#         #store sums of weight inFile along Dinf flow paths in accum
#         if self._gv.outletFile == '':
#             outlet = None
#         else:
#             outlet = self._gv.outletFile
#         ok = TauDEMUtils.runAreaDinf(ang, accum, outlet, inFile, self.numProcesses, None)
#         if not ok:
#             return None
#         aatFile = QSLEEPUtils.join(self._gv.sleepDir, outFileName)
#         # use sca as first file since it will have a .prj: accum is temporary
#         if not self.runCalc(formula, [sca, accum], aatFile, root, isInt=isInt):
#             return None
#         return aatFile
    
#     def runCalc(self, formula, fileList, outFile, root, CELLSize=0, isInt=False):
#         """Run formula on fileList to make outFile.  Return True if successful."""
#         
#         QSLEEPUtils.removeLayerAndFiles(outFile, root)
#         if self._gv.qgisIs2_6:
#             if CELLSize == 0: 
#                 CELLSize = self._gv.sizeX  # real default - 'self' not visible in formal parameters
#             count = len(fileList)
#             if count > 6:
#                 QSLEEPUtils.error(u'{0} is too many files for raster calculator: maximum is 6'.format(count), self._gv.isBatch)
#                 return False
#             f0 = fileList[0]
#             f1 = fileList[1] if count > 1 else None
#             f2 = fileList[2] if count > 2 else None
#             f3 = fileList[3] if count > 3 else None
#             f4 = fileList[4] if count > 4 else None
#             f5 = fileList[5] if count > 5 else None
#             command = "'grass:r.mapcalculator', {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}". \
#             format(f0, f1, f2, f3, f4, f5, formula, self._gv.extentString, CELLSize, outFile)
#             result = processing.runalg('grass:r.mapcalculator', f0, f1, f2, f3, f4, f5, 
#                                        formula, self._gv.extentString, CELLSize, outFile)
#         else:
#             # grass7 r.mapcalculator is broken in QGIS 2.18; use saga
#             f0 = fileList[0]
#             otherFiles = ';'.join(fileList[1:])
#             resampling = 0  # nearest neighbour 
#             typ  = 6 if isInt else 7  # 4 byte int or float
#             useNoData = False
#             command = "'saga:rastercalculator', {0}, {1}, {2}, {3}, {4}, {5}, {6}". \
#             format(f0, otherFiles, formula, resampling, useNoData, typ, outFile)
#             result = processing.runalg('saga:rastercalculator', f0, otherFiles, 
#                                        formula, resampling, useNoData, typ, outFile)
#         if not result:
#             QSLEEPUtils.error(u'Calculator command failed: {0}'.format(command), self._gv.isBatch)
#             return False
#         assert os.path.exists(outFile), u'Calculator command failed: {0}'.format(command)
#         QSLEEPUtils.copyPrj(f0, outFile)
#         return True
    
    def runQgisCalc(self, formula, fileList, outFile, refLayer):
        """Run formula on fileList to make outFile using QGIS map calculator, using refLayer
        to provide extent, width and height.  Return True if successful."""
#         ids = ['P', 'Q', 'R', 'S', 'T', 'U']
#         entries = dict()
#         for i in xrange(min(len(fileList), 6)):
#             entries[i] = QgsRasterCalculatorEntry()
#             entries[i].bandNumber = 1
#             entries[i].raster = QgsRasterLayer(fileList[i], ids[i])
#             entries[i].ref = '{0}@{1!s}'.format(ids[i], i)
#         inLayer = QgsRasterLayer(fileList[0], 'P')
#         calc = QgsRasterCalculator(formula, outFile, 'GTiff', inLayer.extent(), inLayer.width(), inLayer.height(), [entries[i] for i in xrange(len(entries))])
        
        layer1 = QgsRasterLayer(fileList[0], 'P')
        entry1 = QgsRasterCalculatorEntry()
        entry1.bandNumber = 1
        entry1.raster = layer1
        entry1.ref = 'P@1'        
        layer2 = QgsRasterLayer(fileList[1], 'Q')
        entry2 = QgsRasterCalculatorEntry()
        entry2.bandNumber = 1
        entry2.raster = layer2
        entry2.ref = 'Q@1'
        calc = QgsRasterCalculator(formula, outFile, 'GTiff', refLayer.extent(), refLayer.width(), refLayer.height(), [entry1, entry2], QgsCoordinateTransformContext())
        result = calc.processCalculation(p=None)
        if result == 0:
            assert os.path.exists(outFile), 'QGIS calculator formula {0} failed to write output'.format(formula)
            QSLEEPUtils.copyPrj(fileList[0], outFile)
            return True
        else:
            QSLEEPUtils.error('QGIS calculator formula {0} failed: returned {1}'.format(formula, result), self._gv.isBatch)
            return False
        
    def noDataToVal(self, inFile, outFile, val, root, isInt=False):
        """Replace nodata in inFile with val to generate outFile."""
        if os.path.exists(outFile):
            QSLEEPUtils.removeLayerAndFiles(outFile, root)
        r1 = Raster(inFile)
        rout = Raster(outFile, canWrite=True, isInt=isInt)
        completed = False
        while not completed:
            try:
                # safer to mark complete immediately to avoid danger of endless loop
                # only way to loop is then the memory error exception being raised 
                completed = True
                r1.open(self.chunkCount)
                rout.open(self.chunkCount, numRows=r1.numRows, numCols=r1.numCols, 
                                transform=r1.ds.GetGeoTransform(), projection=r1.ds.GetProjection(), noData=r1.noData)
                for row in range(r1.numRows):
                    for col in range(r1.numCols):
                        v1 = r1.read(row, col)
                        vout = val if v1 == r1.noData else v1
                        rout.write(row, col, vout)
                r1.close()
                rout.close()
            except MemoryError:
                QSLEEPUtils.loginfo('noDataToVal out of memory with chunk count {0}'.format(self.chunkCount))
                self.chunkCount += 1
                completed = False
                r1.close()
                rout.close()
        if os.path.exists(outFile):
            QSLEEPUtils.copyPrj(inFile, outFile)
            return True
        else:
            QSLEEPUtils.error('Calculator  failed', self._gv.isBatch)
            return False
        
    def runCalc1(self, file1, func, outFile, root, isInt=False, fun1=None):
        """Use func as a function to calulate outFile from file1.
        
        Valid input data values have fun1 applied if it is not None"""
        if os.path.exists(outFile):
            QSLEEPUtils.removeLayerAndFiles(outFile, root)
        r1 = Raster(file1)
        rout = Raster(outFile, canWrite=True, isInt=isInt)
        completed = False
        while not completed:
            try:
                # safer to mark complete immediately to avoid danger of endless loop
                # only way to loop is then the memory error exception being raised 
                completed = True
                r1.open(self.chunkCount)
                noData = -9999 if isInt else r1.noData
                rout.open(self.chunkCount, numRows=r1.numRows, numCols=r1.numCols, 
                                transform=r1.ds.GetGeoTransform(), projection=r1.ds.GetProjection(), noData=noData)
                for row in range(r1.numRows):
                    for col in range(r1.numCols):
                        v1 = r1.read(row, col)
                        if fun1 is not None and v1 != r1.noData:
                            v1 = fun1(v1)
                        vout = func(v1, r1.noData, noData)
                        rout.write(row, col, vout)
                r1.close()
                rout.close()
            except MemoryError:
                QSLEEPUtils.loginfo('runCalc1 out of memory with chunk count {0}'.format(self.chunkCount))
                self.chunkCount += 1
                completed = False
                r1.close()
                rout.close()
        if os.path.exists(outFile):
            QSLEEPUtils.copyPrj(file1, outFile)
            return True
        else:
            QSLEEPUtils.error('Calculator  failed', self._gv.isBatch)
            return False
        
    def runCalc2(self, file1, file2, func, outFile, root, isInt=False, fun1=None, fun2=None):
        """Use func as a function to calculate outFile from file1 and file2.
        
        Assumes file1 and file2 have same origins and pixel size.
        If file1/2 values are not nodata and fun1/2 are not None, they are applied before func is applied."""
        if os.path.exists(outFile):
            QSLEEPUtils.removeLayerAndFiles(outFile, root)
        r1 = Raster(file1)
        r2 = Raster(file2)
        rout = Raster(outFile, canWrite=True, isInt=isInt)
        completed = False
        while not completed:
            try:
                # safer to mark complete immediately to avoid danger of endless loop
                # only way to loop is then the memory error exception being raised 
                completed = True
                r1.open(self.chunkCount)
                r2.open(self.chunkCount)
                noData = -9999 if isInt else r1.noData
                rout.open(self.chunkCount, numRows=r1.numRows, numCols=r1.numCols, 
                            transform=r1.ds.GetGeoTransform(), projection=r1.ds.GetProjection(), noData=noData)
                for row in range(r1.numRows):
                    for col in range(r1.numCols):
                        v1 = r1.read(row, col)
                        if fun1 is not None and v1 != r1.noData:
                            v1 = fun1(v1)
                        v2 = r2.read(row, col)
                        if fun2 is not None and v2 != r2.noData:
                            v2 = fun2(v2)
                        vout = func(v1, r1.noData, v2, r2.noData, noData)
                        rout.write(row, col, vout)
                r1.close()
                r2.close()
                rout.close()
            except MemoryError:
                QSLEEPUtils.loginfo('runCalc2 out of memory with chunk count {0}'.format(self.chunkCount))
                self.chunkCount += 1
                completed = False
                r1.close()
                r2.close()
                rout.close()
        if os.path.exists(outFile):
            QSLEEPUtils.copyPrj(file1, outFile)
            return True
        else:
            QSLEEPUtils.error('Calculator  failed', self._gv.isBatch)
            return False
        
    def runCalc2Trans(self, file1, file2, func, outFile, baseFile, root, isInt=False, fun1=None, fun2=None):
        """Use func as a function to calulate outFile from file1 and file2, using rows, columns and extent of baseFile.

        If file1/2 values are not nodata and fun1/2 are not None, they are applied before func is applied."""
        if os.path.exists(outFile):
            QSLEEPUtils.removeLayerAndFiles(outFile, root)
        r1 = Raster(file1)
        r2 = Raster(file2)
        rout = Raster(outFile, canWrite=True, isInt=isInt)
        ds = gdal.Open(baseFile, gdal.GA_ReadOnly)
        completed = False
        while not completed:
            try:
                # safer to mark complete immediately to avoid danger of endless loop
                # only way to loop is then the memory error exception being raised 
                completed = True
                r1.open(self.chunkCount)
                r2.open(self.chunkCount)
                transform = ds.GetGeoTransform()
                transform1 = r1.ds.GetGeoTransform()
                transform2 = r2.ds.GetGeoTransform()
                numRows = ds.RasterYSize
                numCols = ds.RasterXSize
                rowFun1, colFun1 = QSLEEPUtils.translateCoords(transform, transform1, numRows, numCols)
                rowFun2, colFun2 = QSLEEPUtils.translateCoords(transform, transform2, numRows, numCols)
                noData = -1 if isInt else r1.noData
                rout.open(self.chunkCount, numRows=numRows, numCols=numCols, 
                            transform=transform, projection=ds.GetProjection(), noData=noData)
                for row in range(numRows):
                    y = QSLEEPUtils.rowToY(row, transform)
                    row1 = rowFun1(row, y)
                    row2 = rowFun2(row, y)
                    for col in range(numCols):
                        x = QSLEEPUtils.colToX(col, transform)
                        col1 = colFun1(col, x)
                        col2 = colFun2(col, x)
                        v1 = r1.read(row1, col1)
                        if fun1 is not None and v1 != r1.noData:
                            v1 =fun1(v1)
                        v2 = r2.read(row2, col2)
                        if fun2 is not None and v2 != r2.noData:
                            v2 = fun2(v2)
                        vout = func(v1, r1.noData, v2, r2.noData, noData)
                        rout.write(row, col, vout)
                r1.close()
                r2.close()
                rout.close()
            except MemoryError:
                QSLEEPUtils.loginfo('runCalc2Trans out of memory with chunk count {0}'.format(self.chunkCount))
                self.chunkCount += 1
                completed = False
                r1.close()
                r2.close()
                rout.close()
        ds = None
        if os.path.exists(outFile):
            QSLEEPUtils.copyPrj(baseFile, outFile)
            return True
        else:
            QSLEEPUtils.error('Calculator  failed', self._gv.isBatch)
            return False
        
    @staticmethod
    def identity(v, noDataIn, noDataOut):
        """Identity function """
        if v == noDataIn:
            return noDataOut
        return v
        
    
    @staticmethod
    def round(v, noDataIn, noDataOut):
        """Round to integer."""
        if v == noDataIn:
            return noDataOut
        return int(v + 0.5)
    
    @staticmethod
    def tanPercent(rad, noDataIn, noDataOut):
        """Convert tan to percent"""
        if rad == noDataIn:
            return noDataOut
        return math.tan(rad) * 100
        
    @staticmethod
    def degToD8(deg, noDataIn, noDataOut):
        """Convert degrees to D8"""
        if deg == noDataIn:
            return noDataOut
        if deg < 22.5: return 3
        if deg < 67.5: return 2
        if deg < 112.5: return 1
        if deg < 157.5: return 8
        if deg < 202.5: return 7
        if deg < 247.5: return 6
        if deg < 292.5: return 5
        if deg < 337.5: return 4
        return 3
    
    @staticmethod
    def subStream(raster, noDataRaster, stream, noDataStream, noDataOut):  # @UnusedVariable noDataStream
        """Subtract stream line from raster."""
        if raster == noDataRaster or stream == 1:
            return noDataOut
        return raster
    
    @staticmethod
    def reclassCurvature(curv, noDataIn, noDataOut):
        """Calculate 'if(curv, 1, 3, 2)'"""
        if curv == noDataIn: return noDataOut
        return curv
    
#     @staticmethod
#     def reclass(curv):
#         """Calculate 'if(curv, 1, 3, 2)'"""
#         if curv > 0: return 1
#         if curv < 0: return 2
#         return 3
    
#     @staticmethod
#     def meanByFlow(fac, noDataFac, accum, noDataAccum, noDataOut):
#         """ Calculate 'accum / fac' """
#         if fac == noDataFac or accum == noDataAccum:
#             return noDataOut
#         return accum / fac  # SoilData.degToD8(accum / fac, noDataFac, noDataOut)
    
    @staticmethod
    def mean(fac, noDataFac, accum, noDataAccum, noDataOut):
        """ Calculate 'accum / fac' """
        if fac == noDataFac or accum == noDataAccum:
            return noDataOut
        return accum / fac
        
    @staticmethod
    def meanTanPercent(fac, noDataFac, accum, noDataAccum, noDataOut):
        """Calculate 'tan(b / a) * 100'"""
        if fac == noDataFac or accum == noDataAccum:
            return noDataOut
        return math.tan(accum/fac) * 100
    
#     @staticmethod
#     def meanReclassify(fac, noDataFac, accum, noDataAccum, noDataOut):
#         """Calculate 'if(b/a, 1, 3, 2)'"""
#         if fac == noDataFac or accum == noDataAccum:
#             return noDataOut
#         return accum / fac
    
    @staticmethod
    def ctiFun(a, noDataA, b, noDataB, noDataOut):
        """Calculate 'ln(a / ifelse(lt(tan(b), 0.001), 0.001, tan(b)))'"""
        if a == noDataA or b == noDataB:
            return noDataOut
        return math.log(a / max(0.001, math.tan(b)))
    
    @staticmethod
    def ndviFun(a, noDataA, b, noDataB, noDataOut):
        """Calculate '(b - a) / (b + a)'"""
        if a == noDataA or b == noDataB:
            return noDataOut
        return 0 if b + a == 0 else (b - a) / (b + a)
    
    def saviFun(self, a, noDataA, b, noDataB, noDataOut):
        """Calculate '((b - a) / (b + a + LScaled)) * LPlus1"""
        if a == noDataA or b == noDataB:
            return noDataOut
        self.LFactor = self._dlg.LFactor.value()
        # red and ir values are in the range 0-255, and they should be in 0-1 for the SAVI formula
        # For simplicity we equivalently multiply L by 256 for its role in the denominator
        LScaled = self.LFactor * 256
        LPlus1 = 1 + self.LFactor
        return ((b - a) / (b + a + LScaled)) * LPlus1
        
    def readProj(self):
        """Read soil and vegetation data from project file."""
        settings = QSettings()
        try:
            self.numProcesses = int(settings.value('/QSWAT/NumProcesses'))
        except:
            self.numProcesses = 0
        proj = QgsProject.instance()
        title = proj.title()
        soilDataFile, found = proj.readEntry(title, 'soil/soildata', '')
        if found and soilDataFile != '':
            soilDataFile = QSLEEPUtils.join(self._gv.projDir, soilDataFile)
            if os.path.exists(soilDataFile):
                self._dlg.selectSoilData.setText(soilDataFile)
                self._gv.soilDataFile = soilDataFile
        NIRFile, found = proj.readEntry(title, 'vegetation/nir', '')
        if found and NIRFile != '':
            NIRFile = QSLEEPUtils.join(self._gv.projDir, NIRFile)
            if os.path.exists(NIRFile):
                self._dlg.selectNIR.setText(NIRFile)
                self.NIRFile = NIRFile
        RedFile, found = proj.readEntry(title, 'vegetation/red', '')
        if found and RedFile != '':
            RedFile = QSLEEPUtils.join(self._gv.projDir, RedFile)
            if os.path.exists(RedFile):
                self._dlg.selectRed.setText(RedFile)
                self.RedFile = RedFile
        self._gv.facetClassCount, found = proj.readNumEntry(title, 'soil/facetclasscount', 1)
        if found:
            self._dlg.numClusters.setValue(self._gv.facetClassCount)
        layerCount, found = proj.readNumEntry(title, 'soil/layercount', 1)
        if found:
            self._gv.layers = dict()
            depth1, found = proj.readNumEntry(title, 'soil/depth1', 0)
            if found:
                self._gv.layers[1] = depth1
            if layerCount > 1:
                depth2, found = proj.readNumEntry(title, 'soil/depth2', 0)
                if found:
                    self._gv.layers[2] = depth2
            if layerCount > 2:
                depth3, found = proj.readNumEntry(title, 'soil/depth3', 0)
                if found:
                    self._gv.layers[3] = depth3
            if layerCount > 3:
                depth4, found = proj.readNumEntry(title, 'soil/depth4', 0)
                if found:
                    self._gv.layers[4] = depth4
            if layerCount > 4:
                depth5, found = proj.readNumEntry(title, 'soil/depth5', 0)
                if found:
                    self._gv.layers[5] = depth5
            if layerCount > 5:
                depth6, found = proj.readNumEntry(title, 'soil/depth6', 0)
                if found:
                    self._gv.layers[6] = depth6
            if layerCount > 6:
                depth7, found = proj.readNumEntry(title, 'soil/depth7', 0)
                if found:
                    self._gv.layers[7] = depth7
            if layerCount > 7:
                depth8, found = proj.readNumEntry(title, 'soil/depth8', 0)
                if found:
                    self._gv.layers[8] = depth8
            if layerCount > 8:
                depth9, found = proj.readNumEntry(title, 'soil/depth9', 0)
                if found:
                    self._gv.layers[9] = depth9
            if layerCount > 9:
                depth10, found = proj.readNumEntry(title, 'soil/depth10', 0)
                if found:
                    self._gv.layers[10] = depth10
            for i in range(layerCount):
                rowCount = self._dlg.layersTable.rowCount()
                if i >= rowCount:
                    self._dlg.layersTable.insertRow(i)
                self._dlg.layersTable.setItem(i, 0, QTableWidgetItem(str(self._gv.layers[i+1])))
        else:
            rowCount = self._dlg.layersTable.rowCount()
            if rowCount == 0:
                self._dlg.layersTable.insertRow(0)
        LFactor, found = proj.readDoubleEntry(title, 'vegetation/lfactor', 0.5)
        if found:
            self._dlg.LFactor.setValue(LFactor)
        automateCSV, found = proj.readEntry(title, 'soil/automateCSV', '')
        if found:
            automateCSV = QSLEEPUtils.join(self._gv.projDir, automateCSV)
            if os.path.exists(automateCSV):
                self._gv.automateCSV = QSLEEPUtils.join(self._gv.projDir, automateCSV)
            
    def addLayer(self):
        """Add another soil layer for depth to be entrered."""
        numLayers = self._dlg.layersTable.rowCount()
        self._dlg.layersTable.insertRow(numLayers)
        
    def deleteLayer(self):
        """Remove bottom soil layer and depth."""
        numLayers = self._dlg.layersTable.rowCount()
        self._dlg.layersTable.removeRow(numLayers-1)
        
    def writeLayers(self):
        """Save soil layer depths."""
        numLayers = self._dlg.layersTable.rowCount()
        self._gv.layers = dict()
        for i in range(numLayers):
            itm = self._dlg.layersTable.item(i, 0)
            val = itm.text() if itm is not None else None
            if val is None or val == '':
                QSLEEPUtils.error('No depth defined for layer {0}'.format(i+1), self._gv.isBatch)
                return False
            try:
                self._gv.layers[i+1] = int(val)
            except Exception as e:
                QSLEEPUtils.error('Cannot read depth for layer {0}: {1}'.format(i+1, str(e)), self._gv.isBatch)
                return False  
        return True
            
    def saveProj(self):
        """Write data to project file."""
        proj = QgsProject.instance()
        title = proj.title()
        proj.writeEntry(title, 'soil/soildata', QSLEEPUtils.relativise(self._gv.soilDataFile, self._gv.projDir))
        proj.writeEntry(title, 'soil/facetclasscount', self._dlg.numClusters.value())
        proj.writeEntry(title, 'vegetation/nir', QSLEEPUtils.relativise(self.NIRFile, self._gv.projDir))
        proj.writeEntry(title, 'vegetation/red', QSLEEPUtils.relativise(self.RedFile, self._gv.projDir))
        if not self.writeLayers():
            return
        layerCount = len(self._gv.layers)
        proj.writeEntry(title, 'soil/layercount', layerCount)
        if layerCount >= 1:
            proj.writeEntry(title, 'soil/depth1', self._gv.layers[1])
        if layerCount >= 2:
            proj.writeEntry(title, 'soil/depth2', self._gv.layers[2])
        if layerCount >= 3:
            proj.writeEntry(title, 'soil/depth3', self._gv.layers[3])
        if layerCount >= 4:
            proj.writeEntry(title, 'soil/depth4', self._gv.layers[4])
        if layerCount >= 5:
            proj.writeEntry(title, 'soil/depth5', self._gv.layers[5])
        if layerCount >= 6:
            proj.writeEntry(title, 'soil/depth6', self._gv.layers[6])
        if layerCount >= 7:
            proj.writeEntry(title, 'soil/depth7', self._gv.layers[7])
        if layerCount >= 8:
            proj.writeEntry(title, 'soil/depth8', self._gv.layers[8])
        if layerCount >= 9:
            proj.writeEntry(title, 'soil/depth9', self._gv.layers[9])
        if layerCount >= 10:
            proj.writeEntry(title, 'soil/depth10', self._gv.layers[10])
        proj.writeEntry(title, 'vegetation/lfactor', str(self._dlg.LFactor.value()))
        proj.write()
        
    def finish(self):
        """Exit OK; warn if nothing done."""
        if not self.hasRun:
            QSLEEPUtils.information('Warning: you did not create input rasters', self._gv.isBatch)
        self._dlg.done(1)
        
    def cancel(self):
        """Cancel form."""
        self._dlg.done(0)
    
    def progress(self, msg):
        """Update progress label with message; emit message for display in testing."""
        QSLEEPUtils.progress(msg, self._dlg.progressLabel)
        if msg != '':
            self.progress_signal.emit(msg) 
              
    ## signal for updating progress label
    progress_signal = pyqtSignal(str)
            
