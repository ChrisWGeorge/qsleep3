# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                              -------------------
        begin                : 2016-05-21
        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt5.QtCore import * # @UnusedWildImport
from PyQt5.QtGui import * # @UnusedWildImport
from PyQt5.QtWidgets import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
import os.path
import time
import subprocess

# Initialize Qt resources from file resources.py
from . import resources_rc
# Import the code for the dialog
from .qsleepdialog import QSleepDialog
from .QSLEEPUtils import QSLEEPUtils  #, FileTypes
from .sleepglobals import GlobalVars
from .sleepdelin import Delineation
# choose one or other
from .soildata import SoilData
from .writeusersoil import UserSoil
from .selectcsv import SelectCSV
from .parameters import Parameters
from .about import AboutQSLEEP


class QSleep:
    """QGIS plugin for preparing SLEEP data."""
    _SLOPE_GROUP_NAME = 'Slope'
    _LANDUSE_GROUP_NAME = 'Landuse'
    _SOIL_GROUP_NAME = 'Soil'
    _WATERSHED_GROUP_NAME = 'Watershed'
    _RESULTS_GROUP_NAME = 'Results'

    def __init__(self, iface, version):
        """Constructor."""
        # Save reference to the QGIS interface
        self._iface = iface
        ## save version
        self._version = version
        # initialize plugin directory
        ## Directory holding QSLEEP plugin
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'qsleep_{}.qm'.format(locale))
        # set default behaviour for loading files with no CRS to prompt - the safest option
        QSettings().setValue('Projections/defaultBehaviour', 'prompt')
        ## translator for current locale
        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
        self._gv = None  # set later

        # Create the dialog (after translation) and keep reference
        self._odlg = QSleepDialog()
        self._odlg.setWindowFlags(self._odlg.windowFlags() & ~Qt.WindowContextHelpButtonHint & Qt.WindowMinimizeButtonHint)
        self._odlg.move(0, 0)
        self._odlg.setWindowTitle('QSLEEP {0}'.format(self._version))
        # flag used in initialising delineation form
        self._demIsProcessed = False
        # report QGIS version
        QSLEEPUtils.loginfo('QGIS version: {0}; QSLEEP version: {1}'.format(Qgis.QGIS_VERSION, self._version))
        # remove QSLEEP temporary folder
        QSLEEPUtils.deleteTempFolder()

    def initGui(self):
        """Create action that will start plugin configuration."""
        ## action to start plugin
        self.action = QAction(
            QIcon(":/QSLEEP/SLEEP32.png"),
            "Soil-Landscape Estimation and Evaluation Program", self._iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self._iface.addToolBarIcon(self.action)
        self._iface.addPluginToMenu("&QSLEEP", self.action)

    def unload(self):
        """Remove the plugin menu item and icon."""
        self._iface.removePluginMenu("&QSLEEP", self.action)
        self._iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        """Run QSLEEP."""
        self._odlg.reportsBox.setVisible(False)
        self._odlg.reportsLabel.setVisible(False)
        self._odlg.reportsBox.clear()
        self._odlg.reportsBox.addItem(QSLEEPUtils.trans('Select report to view'))
        # connect buttons
        self._odlg.aboutButton.clicked.connect(self.about)
        self._odlg.newButton.clicked.connect(self.newProject)
        self._odlg.existingButton.clicked.connect(self.existingProject)
        self._odlg.delinButton.clicked.connect(self.doDelineation)
        self._odlg.soilButton.clicked.connect(self.doSoil)
        self._odlg.rasterButton.clicked.connect(self.predict)
        self._odlg.tableButton.clicked.connect(self.makeTable)
#        self._odlg.paramsButton.clicked.connect(self.runParams)
        self._odlg.reportsBox.activated.connect(self.showReport)
        self._odlg.buttonBox.accepted.connect(self.OK)
        self._odlg.buttonBox.rejected.connect(self.cancel)
        self.initButtons()
        self._odlg.projPath.setText('')
        # show the dialog
        self._odlg.show()
        # initially only new/existing project buttons visible if project not set
        proj = QgsProject.instance()
        if proj.fileName() == '':
            self._odlg.mainBox.setVisible(False)
        else:
            self._iface.mainWindow().setCursor(Qt.WaitCursor)
            self.setupProject(proj, False)
            self._iface.mainWindow().setCursor(Qt.ArrowCursor)
        # Run the dialog event loop
        result = self._odlg.exec_() # @UnusedVariable
        # See if OK was pressed
        if result:
            proj.write()

    def about(self):
        """Show information about QSLEEP."""
        form = AboutQSLEEP()
        form.run(self._version)

    def initButtons(self):
        """Initial button settings."""
        self._odlg.delinLabel.setText('Step 1')
        self._odlg.soilLabel.setText('Step 2')
        self._odlg.rasterLabel.setText('Step 3')
        self._odlg.tableLabel.setText('Step 4')
        self._odlg.soilLabel.setEnabled(False)
        self._odlg.soilButton.setEnabled(False)
        self._odlg.rasterLabel.setEnabled(False)
        self._odlg.rasterButton.setEnabled(False)
        self._odlg.tableLabel.setEnabled(False)
        self._odlg.tableButton.setEnabled(False)
        
    def newProject(self):
        """Call QGIS actions to create and name a new project."""
        self._iface.actionNewProject().trigger()
        # save the project to force user to supply a name and location
        self._iface.actionSaveProjectAs().trigger()
        self.initButtons()
        # allow time for project to be created
        time.sleep(2)
        proj = QgsProject.instance()
        projFile = proj.fileName()
        if not projFile or projFile == '':
            # QSLEEPUtils.error('No project created', False)
            return
        if not QFileInfo(projFile).baseName()[0].isalpha():
            QSLEEPUtils.error('Project name must start with a letter', False)
            if os.path.exists(projFile):
                os.remove(projFile)
            return
        self._odlg.raise_()
        self.setupProject(proj, False)
#        self._gv.writeMasterProgress(0, 0)
        
    def existingProject(self):
        """Open an existing QGIS project."""
        self._iface.actionOpenProject().trigger()
        # allow time for project to be opened
        time.sleep(2)
        proj = QgsProject.instance()
        if proj.fileName() == '':
            QSLEEPUtils.error('No project opened', False)
            return
        self._odlg.raise_()
        self.setupProject(proj, False)
    
    def setupProject(self, proj, isBatch):
        """Set up the project."""
        self._odlg.mainBox.setVisible(True)
        self._odlg.mainBox.setEnabled(False)
        self._odlg.setCursor(Qt.WaitCursor)
        self._odlg.projPath.setText('Restarting project ...')
        proj.setTitle(QFileInfo(proj.fileName()).baseName())
        # now have project so initiate global vars
        # if we do this earlier we cannot for example find the project database
        self._gv = GlobalVars(self._iface, isBatch)
        if self._gv.qgisIs2_6:
            QSLEEPUtils.loginfo('Running QGIS 2.6')
        else:
            QSLEEPUtils.loginfo('Running QGIS {0}'.format(Qgis.QGIS_VERSION))
        self._gv.plugin_dir = self.plugin_dir
        self._odlg.projPath.repaint()
        # create watershed and results groups in layers panel
        root = proj.layerTreeRoot()
        node = root.findGroup(QSLEEPUtils._WATERSHED_GROUP_NAME)
        if node is None:
            root.insertGroup(0, QSLEEPUtils._WATERSHED_GROUP_NAME)
        node = root.findGroup(QSLEEPUtils._RESULTS_GROUP_NAME)
        if node is None:
            root.insertGroup(0, QSLEEPUtils._RESULTS_GROUP_NAME)
        if self.demProcessed():
            self._demIsProcessed = True
            self._odlg.delinLabel.setText('Done')
            self._odlg.soilLabel.setEnabled(True)
            self._odlg.soilButton.setEnabled(True)
        self._odlg.tableButton.setEnabled(False)
        self._odlg.projPath.setText(self._gv.projDir)
        self._odlg.mainBox.setEnabled(True)
        self._odlg.setCursor(Qt.ArrowCursor)
       
    def doDelineation(self):
        """Run the delineation dialog."""
        delin = Delineation(self._iface, self._gv, self._demIsProcessed)
        result = delin.run()
        if result == 1:
            self._demIsProcessed = True
            self._odlg.delinLabel.setText('Done')
            self._odlg.soilLabel.setEnabled(True)
            self._odlg.soilButton.setEnabled(True)
#         elif result == 0:
#             self._demIsProcessed = False
#             self._odlg.delinLabel.setText('Step 1')
#             self._odlg.soilLabel.setText('Step 2')
#             self._odlg.soilLabel.setEnabled(False)
#             self._odlg.soilButton.setEnabled(False)
#             self._odlg.rasterLabel.setEnabled(False)
#             self._odlg.rasterButton.setEnabled(False)
        self._odlg.raise_()
        
    def doSoil(self):
        """Run the soil data dialogue."""
        soilData = SoilData(self._gv)
        result = soilData.run()
        if result:
            self._odlg.soilLabel.setText('Done')
            self._odlg.rasterLabel.setEnabled(True)
            self._odlg.rasterButton.setEnabled(True)
        self._odlg.raise_()
        
    def predict(self):
        """Run the multiple regression dialogue."""
        loadCSV = SelectCSV(self._gv, self._odlg.reportsBox)
        result = loadCSV.run()
        QSLEEPUtils.loginfo('Select csv returned {0}'.format(result))
        if result == 1:
            self._odlg.rasterLabel.setText('Done')
            self._odlg.tableLabel.setEnabled(True)
            self._odlg.tableButton.setEnabled(True)
        self._odlg.raise_()
        
    def makeTable(self):
        """Make the final outout table."""
        self._iface.mainWindow().setCursor(Qt.WaitCursor)
        usersoil = UserSoil(self._gv)
        result = usersoil.run()
        if result == 1:
            self._odlg.tableLabel.setText('Done')
        self._iface.mainWindow().setCursor(Qt.ArrowCursor)
            
    def demProcessed(self):
        """
        Return true if we can proceed with HRU creation.
        
        Return false if any required project setting is not found 
        in the project file
        Return true if:
        Using existing watershed and watershed grid exists and 
        is newer than dem
        or
        Not using existing watershed and filled dem exists and 
        is no older than dem, and
        watershed shapefile exists and is no older than filled dem
        """
        proj = QgsProject.instance()
        if not proj:
            QSLEEPUtils.loginfo('demProcessed failed: no project')
            return False
        title = proj.title()
        root = proj.layerTreeRoot()
        demFile, found = proj.readEntry(title, 'delin/DEM', '')
        if not found or demFile == '':
            QSLEEPUtils.loginfo('demProcessed failed: no DEM')
            return False
        demFile = QSLEEPUtils.join(self._gv.projDir, demFile)
        if not os.path.isfile(demFile):
            QSLEEPUtils.loginfo('demProcessed failed: DEM file does not exist')
            return False
        self._gv.demFile = demFile
        demLayer = QgsRasterLayer(demFile)
        units = demLayer.crs().mapUnits()
        factor = 1 if units == QgsUnitTypes.DistanceMeters else Parameters._FEETTOMETRES if units == QgsUnitTypes.DistanceFeet else 0
        if factor == 0:
            QSLEEPUtils.loginfo('demProcessed failed: units are {0!s}'.format(units))
            return False
        self._gv.sizeX = demLayer.rasterUnitsPerPixelX()
        self._gv.sizeY = demLayer.rasterUnitsPerPixelY()
        self._gv.cellArea = self._gv.sizeX * self._gv.sizeY * factor * factor
        outletFile, found = proj.readEntry(title, 'delin/outlets', '')
        if found and outletFile != '':
            outletFile = QSLEEPUtils.join(self._gv.projDir, outletFile)
            if not os.path.isfile(outletFile):
                QSLEEPUtils.loginfo('demProcessed failed: no outlet file')
                return False
        self._gv.outletFile = outletFile
        demInfo = QFileInfo(demFile)
        if not demInfo.exists():
            QSLEEPUtils.loginfo('demProcessed failed: no DEM info')
            return False
        base = QSLEEPUtils.join(demInfo.absolutePath(), demInfo.baseName())
        self._gv.pFile = base + 'p.tif'
        if not os.path.exists(self._gv.pFile):
            QSLEEPUtils.loginfo('demProcessed failed: no p raster')
            return False
        felFile = base + 'fel.tif'
        if not os.path.isfile(felFile):
            QSLEEPUtils.loginfo('demProcessed failed: no filled DEM')
            return False
        if not QSLEEPUtils.isUpToDate(demFile, felFile):
            QSLEEPUtils.loginfo('demProcessed failed: filled DEM not up to date')
            return False
        self._gv.felFile = felFile
        self._gv.ad8File = base + 'ad8.tif'
        if not os.path.exists(self._gv.ad8File):
            QSLEEPUtils.loginfo('demProcessed failed: no D8 accumulation raster')
            return False
        valleyDepthsFile = base + 'depths.tif'
        if not QSLEEPUtils.isUpToDate(demFile, valleyDepthsFile):
            QSLEEPUtils.loginfo('demProcessed failed: valley depths not up to date')
            return False
        self._gv.valleyDepthsFile = valleyDepthsFile
        clipPFile = base + 'pclip.tif'
        if not QSLEEPUtils.isUpToDate(demFile, clipPFile):
            QSLEEPUtils.loginfo('demProcessed failed: pcclip not up to date')
            return False
        clipAD8File = base + 'ad8clip.tif'
        if not QSLEEPUtils.isUpToDate(demFile, clipAD8File):
            QSLEEPUtils.loginfo('demProcessed failed: clipAD8 not up to date')
            return False
        fac = clipAD8File
        fdr = clipPFile
        facLayer = QgsRasterLayer(fac, 'fac')
        fdrLayer = QgsRasterLayer(fdr, 'fdr')
        self._gv.rasters['fac'] = fac, facLayer
        self._gv.rasters['fdr'] = fdr, fdrLayer
        fil = QSLEEPUtils.join(self._gv.sleepDir, 'fil.tif')
        if not QSLEEPUtils.isUpToDate(demFile, fil):
            QSLEEPUtils.loginfo('demProcessed failed: fil not up to date')
            return False
        filLayer = QgsRasterLayer(fil, 'fil')
        self._gv.rasters['fil'] = fil, filLayer
        srcStreamFile = base + 'srcStream.tif'
        if not QSLEEPUtils.isUpToDate(demFile, srcStreamFile):
            QSLEEPUtils.loginfo('demProcessed failed: streams not up to date')
            return False
        strLayer = QgsRasterLayer(srcStreamFile, 'str')
        self._gv.rasters['str'] = srcStreamFile, strLayer
        facets = QSLEEPUtils.join(self._gv.sleepDir, 'facets.tif')
        if not QSLEEPUtils.isUpToDate(demFile, facets):
            QSLEEPUtils.loginfo('demProcessed failed: facets not up to date')
            return False
        self._gv.facets = facets
        return True
        
    def showReport(self):
        """Display selected report."""
        if not self._odlg.reportsBox.hasFocus():
            return
        item = self._odlg.reportsBox.currentText()
        report = QSLEEPUtils.join(self._gv.sleepDir, item)
        if os.name == 'nt': # Windows
            os.startfile(report)
        elif os.name == 'posix': # Linux
            subprocess.call(('xdg-open', report))
        self._odlg.reportsBox.setCurrentIndex(0)
        
    def OK(self):
        """Finish OK."""
        self._odlg.done(1)
        
    def cancel(self):
        """Cancel."""
        self._odlg.done(0)
        
