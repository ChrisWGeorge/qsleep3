# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSleep
                                 A QGIS plugin
 Soil-Landscape Estimation and Evaluation Program
                             -------------------
        begin                : 2016-05-21
        copyright            : (C) 2016 by Spatial Science Laboratory, Texas A&M University
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import pandas as pd

import numpy as np

from pyod.models.abod import ABOD
from pyod.models.knn import KNN

headers = ['id_d', 'OM_25', 'PH_25', 'SAND_25', 'SILT_25', 'CLAY_25', 'SAND_60', 'SILT_60', 
           'CLAY_60', 'SAND_100', 'SILT_100', 'CLAY_100', 'STONE_25', 'STONE_60', 'STONE_100',
           'total_25', 'total_60', 'total_100']

df = pd.read_csv('C:/QSLEEP_Projects/Tana/soildata.csv', usecols=headers)

# when using ABOD
# this seems to break when the next value to be read is a repeated value
# and is selected as an outlier
# eg for OM_25 when numRows is 53 it is OK, and 0.931 (at id_d 8, 25 and 54) is an outlier
# when numRows is 54, the next value to read is 0.931 and it fails

# for PH_25 OK for numRows 45, outlier is 6.1085 at 2, 10 and 37.  One more row wincludes 6.1085 again
# print df.shape
# print df.columns
numRows = df.shape[0]
print(numRows)

ids = df['id_d'].values

outliers_fraction = 0.02

# clf = ABOD(contamination=outliers_fraction)
clf = KNN(contamination=outliers_fraction)

# organic1 = df['OM_25'].values.reshape(-1, 1)
# 
# total1 = df['total_25'].values.reshape(-1, 1)
# 
# 
# 
# clf.fit(organic1)
# 
# 
# y_pred = clf.predict(organic1)
# 
# preds = y_pred.tolist()


for X in headers[1:]:
    print(X)
    data = df[X].values.reshape(-1, 1)
    clf.fit(data)
    preds = clf.predict(data).tolist()
  
    for i in range(numRows):
        if preds[i] == 1:
            print(ids[i], data[i])
        

# for i in xrange(numRows):
#     if preds[i] == 1:
#         print ids[i], organic1[i]
        
